# TLA2LambdaPi activity report

This repository is intended to record weekly my activity on the PhD.

## W6: 06/02/2023 - 10/02/2023

### Ongoing Tasks

* Verify the generated Cantor proof of veriT with Lambdapi.
* Write the ABZ proposal for the Doctoral Symposium. 

### Tasks for Next Week

* Finish Cantor proof.
* Start to instrument the back-end proof engines of TLAPS for collecting proof from veriT.


## W5: 30/01/2023 - 03/02/2023

### Completed Tasks

* Finish the subscription for the doctoral school on ADUM.
* Prove `extend` lemmas
* Prove the Cantor theorem on paper

### Ongoing Tasks

* Look at how `VeritT` and TLAPS communicate (PR of Rosalie).

### Tasks for Next Week

* Writing the rest of the lemmas in `Function` and let them `admit`.
* Think at how to formalise `proof trace` of SMT-lib in TLAPS.
* Prove the Cantor theorem with lambdaPi (not a priority).


## W4: 23/01/2023 - 27/01/2023

### Completed Tasks

* Implement the first Lemma related to `Function`.
* Read [Reliable Reconstruction of Fine-Grained Proofs in a Proof Assistant](https://matryoshka-project.github.io/pubs/verit_isa_paper.pdf)

### Ongoing Tasks

* Prove the lemma related to `Except` operator in `Function`.
* Look at `VeritT` proof format and play with veriT.

### Tasks for Next Week

* Discussing with Rosalie about the new SMT-lib.
* Continuing the proves in `Function`.
* Prove the Cantor Theorem by hand before having a look on TLAPS.

## W3: 16/01/2023 - 20/01/2023

### Completed Tasks

* Familiarize with LambdaPi:
    - Implementing the exist-uniq operator and add some missing predicate lemma.
    - Defining the definition for `Function`.

### Ongoing Tasks

* Read the [SOME AXIOMS FOR TYPE SYSTEMS](https://arxiv.org/pdf/2111.00543v2.pdf) publication (stuck at Ch4)
* Collecting the last document (assurance de responsabilité civile) to finish the subscription to the PhD school.

### Tasks for Next Week

* Starting to prove the first Lemmas related to `Function`.

## W2: 09/01/2023 - 13/01/2023

### Completed Tasks

* Familiarize with TLA+ and TLAPS:
    - Implementing and proving Euclid
    - Implementing and proving Quicksort (Proving is WIP)
* Read the [TLA+ Proof paper](https://arxiv.org/abs/1208.5933)
* Discuss with Stephan about the paper: [The Specification Language TLA+](https://members.loria.fr/SMerz/papers/tla+logic2008.pdf).

### Ongoing Tasks

* Read the [temporal logic of Action](https://lamport.azurewebsites.net/pubs/lamport-actions.pdf) publication

### Tasks for Next Week

* Starting to implement Function in TLA+ implementation in LambdaPi.
* Review my TLA+ code and proves with Stephan

## W1: 02/01/2023 - 06/01/23

### Completed Tasks

* Read the paper: [The Specification Language TLA+](https://members.loria.fr/SMerz/papers/tla+logic2008.pdf) by Stephan Merz
* Finish the TLA+ Course Lecture by Lamport (refreshing my memory on TLA+) 

### Ongoing Tasks

* Start to follow the tutorial on [TLA+ Proof System](https://tla.msr-inria.inria.fr/tlaps/content/Documentation/Tutorial/The_example.html)
* Having a look at the implementation of Set theory in LambdaPi

### Tasks for Next Week

Refer to Ongoing tasks

* Start to play with LambdaPi to become more familiar with the framework