;; Proof obligation:
;;	ASSUME NEW CONSTANT CONSTANT_N_,
;;	       NEW VARIABLE VARIABLE_num_,
;;	       NEW VARIABLE VARIABLE_flag_,
;;	       NEW VARIABLE VARIABLE_pc_,
;;	       NEW VARIABLE VARIABLE_unread_,
;;	       NEW VARIABLE VARIABLE_max_,
;;	       NEW VARIABLE VARIABLE_nxt_
;;	PROVE  (/\ VARIABLE_num_ = [CONSTANT_i_ \in CONSTANT_P_ |-> 0]
;;	        /\ VARIABLE_flag_ = [CONSTANT_i_ \in CONSTANT_P_ |-> FALSE]
;;	        /\ VARIABLE_unread_ \in [CONSTANT_P_ -> SUBSET CONSTANT_P_]
;;	        /\ VARIABLE_max_ \in [CONSTANT_P_ -> Nat]
;;	        /\ VARIABLE_nxt_ \in [CONSTANT_P_ -> CONSTANT_P_]
;;	        /\ VARIABLE_pc_ = [CONSTANT_self_ \in CONSTANT_P_ |-> "p1"])
;;	       => (/\ VARIABLE_num_ \in [CONSTANT_P_ -> Nat]
;;	           /\ VARIABLE_flag_ \in [CONSTANT_P_ -> BOOLEAN]
;;	           /\ VARIABLE_unread_ \in [CONSTANT_P_ -> SUBSET CONSTANT_P_]
;;	           /\ \A CONSTANT_i_ \in CONSTANT_P_ :
;;	                 VARIABLE_pc_[CONSTANT_i_] \in {"p2", "p5", "p6"}
;;	                 => CONSTANT_i_ \notin VARIABLE_unread_[CONSTANT_i_]
;;	           /\ VARIABLE_max_ \in [CONSTANT_P_ -> Nat]
;;	           /\ VARIABLE_nxt_ \in [CONSTANT_P_ -> CONSTANT_P_]
;;	           /\ \A CONSTANT_i_ \in CONSTANT_P_ :
;;	                 VARIABLE_pc_[CONSTANT_i_] = "p6"
;;	                 => VARIABLE_nxt_[CONSTANT_i_] # CONSTANT_i_
;;	           /\ VARIABLE_pc_
;;	              \in [CONSTANT_P_ ->
;;	                     {"p1", "p2", "p3", "p4", "p5", "p6", "cs", "p7"}])
;;	          /\ (\A CONSTANT_i_ \in CONSTANT_P_ :
;;	                 /\ VARIABLE_num_[CONSTANT_i_] = 0
;;	                    <=> VARIABLE_pc_[CONSTANT_i_] \in {"p1", "p2", "p3"}
;;	                 /\ VARIABLE_flag_[CONSTANT_i_]
;;	                    <=> VARIABLE_pc_[CONSTANT_i_] \in {"p2", "p3", "p4"}
;;	                 /\ VARIABLE_pc_[CONSTANT_i_] \in {"p5", "p6"}
;;	                    => (\A CONSTANT_j_
;;	                           \in (CONSTANT_P_ \ VARIABLE_unread_[CONSTANT_i_])
;;	                               \ {CONSTANT_i_} :
;;	                           STATE_After_(CONSTANT_j_, CONSTANT_i_))
;;	                 /\ (/\ VARIABLE_pc_[CONSTANT_i_] = "p6"
;;	                     /\ \/ VARIABLE_pc_[VARIABLE_nxt_[CONSTANT_i_]] = "p2"
;;	                           /\ CONSTANT_i_
;;	                              \notin VARIABLE_unread_[VARIABLE_nxt_[CONSTANT_i_]]
;;	                        \/ VARIABLE_pc_[VARIABLE_nxt_[CONSTANT_i_]] = "p3")
;;	                    => VARIABLE_max_[VARIABLE_nxt_[CONSTANT_i_]]
;;	                       >= VARIABLE_num_[CONSTANT_i_]
;;	                 /\ VARIABLE_pc_[CONSTANT_i_] \in {"cs", "p7"}
;;	                    => (\A CONSTANT_j_ \in CONSTANT_P_ \ {CONSTANT_i_} :
;;	                           STATE_After_(CONSTANT_j_, CONSTANT_i_)))
;; TLA+ Proof Manager 1.5.0
;; Proof obligation #6
;; Generated from file "./tla_specs/Original/TLAPS_Examples/AtomicBakery.tla", line 244, characters 3-4

(set-logic UFLIA)

;; Sorts

(declare-sort Idv 0)

;; Hypotheses

(declare-fun smt__TLA__BoolSet () Idv)

(declare-fun smt__TLA__Cast_Bool (Bool) Idv)

(declare-fun smt__TLA__Cast_Int (Int) Idv)

(declare-fun smt__TLA__FunApp (Idv Idv) Idv)

(declare-fun smt__TLA__FunDom (Idv) Idv)

; omitted declaration of 'TLA__FunFcn' (second-order)

(declare-fun smt__TLA__FunIsafcn (Idv) Bool)

(declare-fun smt__TLA__FunSet (Idv Idv) Idv)

(declare-fun smt__TLA__IntLteq (Idv Idv) Bool)

(declare-fun smt__TLA__IntSet () Idv)

(declare-fun smt__TLA__Mem (Idv Idv) Bool)

(declare-fun smt__TLA__NatSet () Idv)

(declare-fun smt__TLA__Proj_Int (Idv) Int)

(declare-fun smt__TLA__SetEnum_1 (Idv) Idv)

(declare-fun smt__TLA__SetEnum_2 (Idv Idv) Idv)

(declare-fun smt__TLA__SetEnum_3 (Idv Idv Idv) Idv)

(declare-fun smt__TLA__SetEnum_8 (Idv Idv Idv Idv Idv Idv Idv Idv) Idv)

(declare-fun smt__TLA__SetExtTrigger (Idv Idv) Bool)

(declare-fun smt__TLA__SetMinus (Idv Idv) Idv)

(declare-fun smt__TLA__StrLit_cs () Idv)

(declare-fun smt__TLA__StrLit_p1 () Idv)

(declare-fun smt__TLA__StrLit_p2 () Idv)

(declare-fun smt__TLA__StrLit_p3 () Idv)

(declare-fun smt__TLA__StrLit_p4 () Idv)

(declare-fun smt__TLA__StrLit_p5 () Idv)

(declare-fun smt__TLA__StrLit_p6 () Idv)

(declare-fun smt__TLA__StrLit_p7 () Idv)

(declare-fun smt__TLA__StrSet () Idv)

(declare-fun smt__TLA__Subset (Idv) Idv)

(declare-fun smt__TLA__SubsetEq (Idv Idv) Bool)

(declare-fun smt__TLA__TrigEq_Idv (Idv Idv) Bool)

(declare-fun smt__TLA__Tt_Idv () Idv)

;; Axiom: SetExt
(assert
  (!
    (forall ((smt__x Idv) (smt__y Idv))
      (!
        (=>
          (forall ((smt__z Idv))
            (= (smt__TLA__Mem smt__z smt__x) (smt__TLA__Mem smt__z smt__y)))
          (= smt__x smt__y))
        :pattern ((smt__TLA__SetExtTrigger smt__x smt__y)))) :named |SetExt|))

;; Axiom: SubsetEqIntro
(assert
  (!
    (forall ((smt__x Idv) (smt__y Idv))
      (!
        (=>
          (forall ((smt__z Idv))
            (=> (smt__TLA__Mem smt__z smt__x) (smt__TLA__Mem smt__z smt__y)))
          (smt__TLA__SubsetEq smt__x smt__y))
        :pattern ((smt__TLA__SubsetEq smt__x smt__y))))
    :named |SubsetEqIntro|))

;; Axiom: SubsetEqElim
(assert
  (!
    (forall ((smt__x Idv) (smt__y Idv) (smt__z Idv))
      (!
        (=>
          (and (smt__TLA__SubsetEq smt__x smt__y)
            (smt__TLA__Mem smt__z smt__x)) (smt__TLA__Mem smt__z smt__y))
        :pattern ((smt__TLA__SubsetEq smt__x smt__y)
                   (smt__TLA__Mem smt__z smt__x)))) :named |SubsetEqElim|))

;; Axiom: SubsetDefAlt
(assert
  (!
    (forall ((smt__a Idv) (smt__x Idv))
      (!
        (= (smt__TLA__Mem smt__x (smt__TLA__Subset smt__a))
          (smt__TLA__SubsetEq smt__x smt__a))
        :pattern ((smt__TLA__Mem smt__x (smt__TLA__Subset smt__a)))
        :pattern ((smt__TLA__SubsetEq smt__x smt__a)
                   (smt__TLA__Subset smt__a)))) :named |SubsetDefAlt|))

;; Axiom: SetMinusDef
(assert
  (!
    (forall ((smt__a Idv) (smt__b Idv) (smt__x Idv))
      (!
        (= (smt__TLA__Mem smt__x (smt__TLA__SetMinus smt__a smt__b))
          (and (smt__TLA__Mem smt__x smt__a)
            (not (smt__TLA__Mem smt__x smt__b))))
        :pattern ((smt__TLA__Mem smt__x (smt__TLA__SetMinus smt__a smt__b)))
        :pattern ((smt__TLA__Mem smt__x smt__a)
                   (smt__TLA__SetMinus smt__a smt__b))
        :pattern ((smt__TLA__Mem smt__x smt__b)
                   (smt__TLA__SetMinus smt__a smt__b)))) :named |SetMinusDef|))

;; Axiom: NatSetDef
(assert
  (!
    (forall ((smt__x Idv))
      (!
        (= (smt__TLA__Mem smt__x smt__TLA__NatSet)
          (and (smt__TLA__Mem smt__x smt__TLA__IntSet)
            (smt__TLA__IntLteq (smt__TLA__Cast_Int 0) smt__x)))
        :pattern ((smt__TLA__Mem smt__x smt__TLA__NatSet))))
    :named |NatSetDef|))

;; Axiom: FunExt
(assert
  (!
    (forall ((smt__f Idv) (smt__g Idv))
      (!
        (=>
          (and (smt__TLA__FunIsafcn smt__f) (smt__TLA__FunIsafcn smt__g)
            (= (smt__TLA__FunDom smt__f) (smt__TLA__FunDom smt__g))
            (forall ((smt__x Idv))
              (=> (smt__TLA__Mem smt__x (smt__TLA__FunDom smt__f))
                (= (smt__TLA__FunApp smt__f smt__x)
                  (smt__TLA__FunApp smt__g smt__x))))) (= smt__f smt__g))
        :pattern ((smt__TLA__FunIsafcn smt__f) (smt__TLA__FunIsafcn smt__g))))
    :named |FunExt|))

; omitted fact (second-order)

;; Axiom: FunSetIntro
(assert
  (!
    (forall ((smt__a Idv) (smt__b Idv) (smt__f Idv))
      (!
        (=>
          (and (smt__TLA__FunIsafcn smt__f)
            (= (smt__TLA__FunDom smt__f) smt__a)
            (forall ((smt__x Idv))
              (=> (smt__TLA__Mem smt__x smt__a)
                (smt__TLA__Mem (smt__TLA__FunApp smt__f smt__x) smt__b))))
          (smt__TLA__Mem smt__f (smt__TLA__FunSet smt__a smt__b)))
        :pattern ((smt__TLA__Mem smt__f (smt__TLA__FunSet smt__a smt__b)))))
    :named |FunSetIntro|))

;; Axiom: FunSetElim1
(assert
  (!
    (forall ((smt__a Idv) (smt__b Idv) (smt__f Idv))
      (!
        (=> (smt__TLA__Mem smt__f (smt__TLA__FunSet smt__a smt__b))
          (and (smt__TLA__FunIsafcn smt__f)
            (= (smt__TLA__FunDom smt__f) smt__a)))
        :pattern ((smt__TLA__Mem smt__f (smt__TLA__FunSet smt__a smt__b)))))
    :named |FunSetElim1|))

;; Axiom: FunSetElim2
(assert
  (!
    (forall ((smt__a Idv) (smt__b Idv) (smt__f Idv) (smt__x Idv))
      (!
        (=>
          (and (smt__TLA__Mem smt__f (smt__TLA__FunSet smt__a smt__b))
            (smt__TLA__Mem smt__x smt__a))
          (smt__TLA__Mem (smt__TLA__FunApp smt__f smt__x) smt__b))
        :pattern ((smt__TLA__Mem smt__f (smt__TLA__FunSet smt__a smt__b))
                   (smt__TLA__Mem smt__x smt__a))
        :pattern ((smt__TLA__Mem smt__f (smt__TLA__FunSet smt__a smt__b))
                   (smt__TLA__FunApp smt__f smt__x)))) :named |FunSetElim2|))

; omitted fact (second-order)

; omitted fact (second-order)

;; Axiom: EnumDefIntro 1
(assert
  (!
    (forall ((smt__a1 Idv))
      (! (smt__TLA__Mem smt__a1 (smt__TLA__SetEnum_1 smt__a1))
        :pattern ((smt__TLA__SetEnum_1 smt__a1)))) :named |EnumDefIntro 1|))

;; Axiom: EnumDefIntro 2
(assert
  (!
    (forall ((smt__a1 Idv) (smt__a2 Idv))
      (!
        (and (smt__TLA__Mem smt__a1 (smt__TLA__SetEnum_2 smt__a1 smt__a2))
          (smt__TLA__Mem smt__a2 (smt__TLA__SetEnum_2 smt__a1 smt__a2)))
        :pattern ((smt__TLA__SetEnum_2 smt__a1 smt__a2))))
    :named |EnumDefIntro 2|))

;; Axiom: EnumDefIntro 3
(assert
  (!
    (forall ((smt__a1 Idv) (smt__a2 Idv) (smt__a3 Idv))
      (!
        (and
          (smt__TLA__Mem smt__a1
            (smt__TLA__SetEnum_3 smt__a1 smt__a2 smt__a3))
          (smt__TLA__Mem smt__a2
            (smt__TLA__SetEnum_3 smt__a1 smt__a2 smt__a3))
          (smt__TLA__Mem smt__a3
            (smt__TLA__SetEnum_3 smt__a1 smt__a2 smt__a3)))
        :pattern ((smt__TLA__SetEnum_3 smt__a1 smt__a2 smt__a3))))
    :named |EnumDefIntro 3|))

;; Axiom: EnumDefIntro 8
(assert
  (!
    (forall
      ((smt__a1 Idv) (smt__a2 Idv) (smt__a3 Idv) (smt__a4 Idv) (smt__a5 Idv)
        (smt__a6 Idv) (smt__a7 Idv) (smt__a8 Idv))
      (!
        (and
          (smt__TLA__Mem smt__a1
            (smt__TLA__SetEnum_8 smt__a1 smt__a2 smt__a3 smt__a4 smt__a5
              smt__a6 smt__a7 smt__a8))
          (smt__TLA__Mem smt__a2
            (smt__TLA__SetEnum_8 smt__a1 smt__a2 smt__a3 smt__a4 smt__a5
              smt__a6 smt__a7 smt__a8))
          (smt__TLA__Mem smt__a3
            (smt__TLA__SetEnum_8 smt__a1 smt__a2 smt__a3 smt__a4 smt__a5
              smt__a6 smt__a7 smt__a8))
          (smt__TLA__Mem smt__a4
            (smt__TLA__SetEnum_8 smt__a1 smt__a2 smt__a3 smt__a4 smt__a5
              smt__a6 smt__a7 smt__a8))
          (smt__TLA__Mem smt__a5
            (smt__TLA__SetEnum_8 smt__a1 smt__a2 smt__a3 smt__a4 smt__a5
              smt__a6 smt__a7 smt__a8))
          (smt__TLA__Mem smt__a6
            (smt__TLA__SetEnum_8 smt__a1 smt__a2 smt__a3 smt__a4 smt__a5
              smt__a6 smt__a7 smt__a8))
          (smt__TLA__Mem smt__a7
            (smt__TLA__SetEnum_8 smt__a1 smt__a2 smt__a3 smt__a4 smt__a5
              smt__a6 smt__a7 smt__a8))
          (smt__TLA__Mem smt__a8
            (smt__TLA__SetEnum_8 smt__a1 smt__a2 smt__a3 smt__a4 smt__a5
              smt__a6 smt__a7 smt__a8)))
        :pattern ((smt__TLA__SetEnum_8 smt__a1 smt__a2 smt__a3 smt__a4
                    smt__a5 smt__a6 smt__a7 smt__a8))))
    :named |EnumDefIntro 8|))

;; Axiom: EnumDefElim 1
(assert
  (!
    (forall ((smt__a1 Idv) (smt__x Idv))
      (!
        (=> (smt__TLA__Mem smt__x (smt__TLA__SetEnum_1 smt__a1))
          (= smt__x smt__a1))
        :pattern ((smt__TLA__Mem smt__x (smt__TLA__SetEnum_1 smt__a1)))))
    :named |EnumDefElim 1|))

;; Axiom: EnumDefElim 2
(assert
  (!
    (forall ((smt__a1 Idv) (smt__a2 Idv) (smt__x Idv))
      (!
        (=> (smt__TLA__Mem smt__x (smt__TLA__SetEnum_2 smt__a1 smt__a2))
          (or (= smt__x smt__a1) (= smt__x smt__a2)))
        :pattern ((smt__TLA__Mem smt__x (smt__TLA__SetEnum_2 smt__a1 smt__a2)))))
    :named |EnumDefElim 2|))

;; Axiom: EnumDefElim 3
(assert
  (!
    (forall ((smt__a1 Idv) (smt__a2 Idv) (smt__a3 Idv) (smt__x Idv))
      (!
        (=>
          (smt__TLA__Mem smt__x (smt__TLA__SetEnum_3 smt__a1 smt__a2 smt__a3))
          (or (= smt__x smt__a1) (= smt__x smt__a2) (= smt__x smt__a3)))
        :pattern ((smt__TLA__Mem smt__x
                    (smt__TLA__SetEnum_3 smt__a1 smt__a2 smt__a3)))))
    :named |EnumDefElim 3|))

;; Axiom: EnumDefElim 8
(assert
  (!
    (forall
      ((smt__a1 Idv) (smt__a2 Idv) (smt__a3 Idv) (smt__a4 Idv) (smt__a5 Idv)
        (smt__a6 Idv) (smt__a7 Idv) (smt__a8 Idv) (smt__x Idv))
      (!
        (=>
          (smt__TLA__Mem smt__x
            (smt__TLA__SetEnum_8 smt__a1 smt__a2 smt__a3 smt__a4 smt__a5
              smt__a6 smt__a7 smt__a8))
          (or (= smt__x smt__a1) (= smt__x smt__a2) (= smt__x smt__a3)
            (= smt__x smt__a4) (= smt__x smt__a5) (= smt__x smt__a6)
            (= smt__x smt__a7) (= smt__x smt__a8)))
        :pattern ((smt__TLA__Mem smt__x
                    (smt__TLA__SetEnum_8 smt__a1 smt__a2 smt__a3 smt__a4
                      smt__a5 smt__a6 smt__a7 smt__a8)))))
    :named |EnumDefElim 8|))

;; Axiom: StrLitIsstr cs
(assert
  (! (smt__TLA__Mem smt__TLA__StrLit_cs smt__TLA__StrSet)
    :named |StrLitIsstr cs|))

;; Axiom: StrLitIsstr p1
(assert
  (! (smt__TLA__Mem smt__TLA__StrLit_p1 smt__TLA__StrSet)
    :named |StrLitIsstr p1|))

;; Axiom: StrLitIsstr p2
(assert
  (! (smt__TLA__Mem smt__TLA__StrLit_p2 smt__TLA__StrSet)
    :named |StrLitIsstr p2|))

;; Axiom: StrLitIsstr p3
(assert
  (! (smt__TLA__Mem smt__TLA__StrLit_p3 smt__TLA__StrSet)
    :named |StrLitIsstr p3|))

;; Axiom: StrLitIsstr p4
(assert
  (! (smt__TLA__Mem smt__TLA__StrLit_p4 smt__TLA__StrSet)
    :named |StrLitIsstr p4|))

;; Axiom: StrLitIsstr p5
(assert
  (! (smt__TLA__Mem smt__TLA__StrLit_p5 smt__TLA__StrSet)
    :named |StrLitIsstr p5|))

;; Axiom: StrLitIsstr p6
(assert
  (! (smt__TLA__Mem smt__TLA__StrLit_p6 smt__TLA__StrSet)
    :named |StrLitIsstr p6|))

;; Axiom: StrLitIsstr p7
(assert
  (! (smt__TLA__Mem smt__TLA__StrLit_p7 smt__TLA__StrSet)
    :named |StrLitIsstr p7|))

;; Axiom: StrLitDistinct cs p1
(assert
  (! (distinct smt__TLA__StrLit_cs smt__TLA__StrLit_p1)
    :named |StrLitDistinct cs p1|))

;; Axiom: StrLitDistinct cs p2
(assert
  (! (distinct smt__TLA__StrLit_cs smt__TLA__StrLit_p2)
    :named |StrLitDistinct cs p2|))

;; Axiom: StrLitDistinct cs p3
(assert
  (! (distinct smt__TLA__StrLit_cs smt__TLA__StrLit_p3)
    :named |StrLitDistinct cs p3|))

;; Axiom: StrLitDistinct cs p4
(assert
  (! (distinct smt__TLA__StrLit_cs smt__TLA__StrLit_p4)
    :named |StrLitDistinct cs p4|))

;; Axiom: StrLitDistinct cs p5
(assert
  (! (distinct smt__TLA__StrLit_cs smt__TLA__StrLit_p5)
    :named |StrLitDistinct cs p5|))

;; Axiom: StrLitDistinct cs p6
(assert
  (! (distinct smt__TLA__StrLit_cs smt__TLA__StrLit_p6)
    :named |StrLitDistinct cs p6|))

;; Axiom: StrLitDistinct p2 p1
(assert
  (! (distinct smt__TLA__StrLit_p2 smt__TLA__StrLit_p1)
    :named |StrLitDistinct p2 p1|))

;; Axiom: StrLitDistinct p3 p1
(assert
  (! (distinct smt__TLA__StrLit_p3 smt__TLA__StrLit_p1)
    :named |StrLitDistinct p3 p1|))

;; Axiom: StrLitDistinct p3 p2
(assert
  (! (distinct smt__TLA__StrLit_p3 smt__TLA__StrLit_p2)
    :named |StrLitDistinct p3 p2|))

;; Axiom: StrLitDistinct p3 p5
(assert
  (! (distinct smt__TLA__StrLit_p3 smt__TLA__StrLit_p5)
    :named |StrLitDistinct p3 p5|))

;; Axiom: StrLitDistinct p3 p6
(assert
  (! (distinct smt__TLA__StrLit_p3 smt__TLA__StrLit_p6)
    :named |StrLitDistinct p3 p6|))

;; Axiom: StrLitDistinct p4 p1
(assert
  (! (distinct smt__TLA__StrLit_p4 smt__TLA__StrLit_p1)
    :named |StrLitDistinct p4 p1|))

;; Axiom: StrLitDistinct p4 p2
(assert
  (! (distinct smt__TLA__StrLit_p4 smt__TLA__StrLit_p2)
    :named |StrLitDistinct p4 p2|))

;; Axiom: StrLitDistinct p4 p3
(assert
  (! (distinct smt__TLA__StrLit_p4 smt__TLA__StrLit_p3)
    :named |StrLitDistinct p4 p3|))

;; Axiom: StrLitDistinct p4 p5
(assert
  (! (distinct smt__TLA__StrLit_p4 smt__TLA__StrLit_p5)
    :named |StrLitDistinct p4 p5|))

;; Axiom: StrLitDistinct p4 p6
(assert
  (! (distinct smt__TLA__StrLit_p4 smt__TLA__StrLit_p6)
    :named |StrLitDistinct p4 p6|))

;; Axiom: StrLitDistinct p5 p1
(assert
  (! (distinct smt__TLA__StrLit_p5 smt__TLA__StrLit_p1)
    :named |StrLitDistinct p5 p1|))

;; Axiom: StrLitDistinct p5 p2
(assert
  (! (distinct smt__TLA__StrLit_p5 smt__TLA__StrLit_p2)
    :named |StrLitDistinct p5 p2|))

;; Axiom: StrLitDistinct p6 p1
(assert
  (! (distinct smt__TLA__StrLit_p6 smt__TLA__StrLit_p1)
    :named |StrLitDistinct p6 p1|))

;; Axiom: StrLitDistinct p6 p2
(assert
  (! (distinct smt__TLA__StrLit_p6 smt__TLA__StrLit_p2)
    :named |StrLitDistinct p6 p2|))

;; Axiom: StrLitDistinct p6 p5
(assert
  (! (distinct smt__TLA__StrLit_p6 smt__TLA__StrLit_p5)
    :named |StrLitDistinct p6 p5|))

;; Axiom: StrLitDistinct p7 cs
(assert
  (! (distinct smt__TLA__StrLit_p7 smt__TLA__StrLit_cs)
    :named |StrLitDistinct p7 cs|))

;; Axiom: StrLitDistinct p7 p1
(assert
  (! (distinct smt__TLA__StrLit_p7 smt__TLA__StrLit_p1)
    :named |StrLitDistinct p7 p1|))

;; Axiom: StrLitDistinct p7 p2
(assert
  (! (distinct smt__TLA__StrLit_p7 smt__TLA__StrLit_p2)
    :named |StrLitDistinct p7 p2|))

;; Axiom: StrLitDistinct p7 p3
(assert
  (! (distinct smt__TLA__StrLit_p7 smt__TLA__StrLit_p3)
    :named |StrLitDistinct p7 p3|))

;; Axiom: StrLitDistinct p7 p4
(assert
  (! (distinct smt__TLA__StrLit_p7 smt__TLA__StrLit_p4)
    :named |StrLitDistinct p7 p4|))

;; Axiom: StrLitDistinct p7 p5
(assert
  (! (distinct smt__TLA__StrLit_p7 smt__TLA__StrLit_p5)
    :named |StrLitDistinct p7 p5|))

;; Axiom: StrLitDistinct p7 p6
(assert
  (! (distinct smt__TLA__StrLit_p7 smt__TLA__StrLit_p6)
    :named |StrLitDistinct p7 p6|))

;; Axiom: CastInjAlt Bool
(assert
  (!
    (and (= (smt__TLA__Cast_Bool true) smt__TLA__Tt_Idv)
      (distinct (smt__TLA__Cast_Bool false) smt__TLA__Tt_Idv))
    :named |CastInjAlt Bool|))

;; Axiom: CastInjAlt Int
(assert
  (!
    (forall ((smt__x Int))
      (! (= smt__x (smt__TLA__Proj_Int (smt__TLA__Cast_Int smt__x)))
        :pattern ((smt__TLA__Cast_Int smt__x)))) :named |CastInjAlt Int|))

;; Axiom: TypeGuardIntro Bool
(assert
  (!
    (forall ((smt__z Bool))
      (! (smt__TLA__Mem (smt__TLA__Cast_Bool smt__z) smt__TLA__BoolSet)
        :pattern ((smt__TLA__Cast_Bool smt__z))))
    :named |TypeGuardIntro Bool|))

;; Axiom: TypeGuardIntro Int
(assert
  (!
    (forall ((smt__z Int))
      (! (smt__TLA__Mem (smt__TLA__Cast_Int smt__z) smt__TLA__IntSet)
        :pattern ((smt__TLA__Cast_Int smt__z)))) :named |TypeGuardIntro Int|))

;; Axiom: TypeGuardElim Bool
(assert
  (!
    (forall ((smt__x Idv))
      (!
        (=> (smt__TLA__Mem smt__x smt__TLA__BoolSet)
          (or (= smt__x (smt__TLA__Cast_Bool true))
            (= smt__x (smt__TLA__Cast_Bool false))))
        :pattern ((smt__TLA__Mem smt__x smt__TLA__BoolSet))))
    :named |TypeGuardElim Bool|))

;; Axiom: TypeGuardElim Int
(assert
  (!
    (forall ((smt__x Idv))
      (!
        (=> (smt__TLA__Mem smt__x smt__TLA__IntSet)
          (= smt__x (smt__TLA__Cast_Int (smt__TLA__Proj_Int smt__x))))
        :pattern ((smt__TLA__Mem smt__x smt__TLA__IntSet))))
    :named |TypeGuardElim Int|))

;; Axiom: Typing TIntLteq
(assert
  (!
    (forall ((smt__x1 Int) (smt__x2 Int))
      (!
        (=
          (smt__TLA__IntLteq (smt__TLA__Cast_Int smt__x1)
            (smt__TLA__Cast_Int smt__x2)) (<= smt__x1 smt__x2))
        :pattern ((smt__TLA__IntLteq (smt__TLA__Cast_Int smt__x1)
                    (smt__TLA__Cast_Int smt__x2))))) :named |Typing TIntLteq|))

;; Axiom: ExtTrigEqDef Idv
(assert
  (!
    (forall ((smt__x Idv) (smt__y Idv))
      (! (= (smt__TLA__TrigEq_Idv smt__x smt__y) (= smt__x smt__y))
        :pattern ((smt__TLA__TrigEq_Idv smt__x smt__y))))
    :named |ExtTrigEqDef Idv|))

; hidden fact

; hidden fact

; omitted declaration of 'CONSTANT_EnabledWrapper_' (second-order)

; omitted declaration of 'CONSTANT_CdotWrapper_' (second-order)

(declare-fun smt__CONSTANT_N_ () Idv)

; hidden fact

(declare-fun smt__CONSTANT_P_ () Idv)

(declare-fun smt__VARIABLE_num_ () Idv)

(declare-fun smt__VARIABLE_num__prime () Idv)

(declare-fun smt__VARIABLE_flag_ () Idv)

(declare-fun smt__VARIABLE_flag__prime () Idv)

(declare-fun smt__VARIABLE_pc_ () Idv)

(declare-fun smt__VARIABLE_pc__prime () Idv)

(declare-fun smt__STATE_LL_ (Idv Idv) Idv)

(declare-fun smt__VARIABLE_unread_ () Idv)

(declare-fun smt__VARIABLE_unread__prime () Idv)

(declare-fun smt__VARIABLE_max_ () Idv)

(declare-fun smt__VARIABLE_max__prime () Idv)

(declare-fun smt__VARIABLE_nxt_ () Idv)

(declare-fun smt__VARIABLE_nxt__prime () Idv)

(declare-fun smt__STATE_vars_ () Idv)

(declare-fun smt__ACTION_p1_ (Idv) Idv)

(declare-fun smt__ACTION_p2_ (Idv) Idv)

(declare-fun smt__ACTION_p3_ (Idv) Idv)

(declare-fun smt__ACTION_p4_ (Idv) Idv)

(declare-fun smt__ACTION_p5_ (Idv) Idv)

(declare-fun smt__ACTION_p6_ (Idv) Idv)

(declare-fun smt__ACTION_cs_ (Idv) Idv)

(declare-fun smt__ACTION_p7_ (Idv) Idv)

(declare-fun smt__ACTION_p_ (Idv) Idv)

(declare-fun smt__ACTION_Next_ () Idv)

(declare-fun smt__TEMPORAL_Spec_ () Idv)

(declare-fun smt__STATE_MutualExclusion_ () Idv)

(declare-fun smt__STATE_After_ (Idv Idv) Idv)

; hidden fact

; hidden fact

(declare-fun smt__TLA__FunFcn_flatnd_1 (Idv) Idv)

;; Axiom: FunConstrIsafcn TLA__FunFcn_flatnd_1
(assert
  (!
    (forall ((smt__a Idv))
      (! (smt__TLA__FunIsafcn (smt__TLA__FunFcn_flatnd_1 smt__a))
        :pattern ((smt__TLA__FunFcn_flatnd_1 smt__a))))
    :named |FunConstrIsafcn TLA__FunFcn_flatnd_1|))

;; Axiom: FunDomDef TLA__FunFcn_flatnd_1
(assert
  (!
    (forall ((smt__a Idv))
      (! (= (smt__TLA__FunDom (smt__TLA__FunFcn_flatnd_1 smt__a)) smt__a)
        :pattern ((smt__TLA__FunFcn_flatnd_1 smt__a))))
    :named |FunDomDef TLA__FunFcn_flatnd_1|))

;; Axiom: FunAppDef TLA__FunFcn_flatnd_1
(assert
  (!
    (forall ((smt__a Idv) (smt__x Idv))
      (!
        (=> (smt__TLA__Mem smt__x smt__a)
          (= (smt__TLA__FunApp (smt__TLA__FunFcn_flatnd_1 smt__a) smt__x)
            (smt__TLA__Cast_Int 0)))
        :pattern ((smt__TLA__FunApp (smt__TLA__FunFcn_flatnd_1 smt__a) smt__x))
        :pattern ((smt__TLA__Mem smt__x smt__a)
                   (smt__TLA__FunFcn_flatnd_1 smt__a))))
    :named |FunAppDef TLA__FunFcn_flatnd_1|))

(declare-fun smt__TLA__FunFcn_flatnd_2 (Idv) Idv)

;; Axiom: FunConstrIsafcn TLA__FunFcn_flatnd_2
(assert
  (!
    (forall ((smt__a Idv))
      (! (smt__TLA__FunIsafcn (smt__TLA__FunFcn_flatnd_2 smt__a))
        :pattern ((smt__TLA__FunFcn_flatnd_2 smt__a))))
    :named |FunConstrIsafcn TLA__FunFcn_flatnd_2|))

;; Axiom: FunDomDef TLA__FunFcn_flatnd_2
(assert
  (!
    (forall ((smt__a Idv))
      (! (= (smt__TLA__FunDom (smt__TLA__FunFcn_flatnd_2 smt__a)) smt__a)
        :pattern ((smt__TLA__FunFcn_flatnd_2 smt__a))))
    :named |FunDomDef TLA__FunFcn_flatnd_2|))

;; Axiom: FunAppDef TLA__FunFcn_flatnd_2
(assert
  (!
    (forall ((smt__a Idv) (smt__x Idv))
      (!
        (=> (smt__TLA__Mem smt__x smt__a)
          (= (smt__TLA__FunApp (smt__TLA__FunFcn_flatnd_2 smt__a) smt__x)
            (smt__TLA__Cast_Bool false)))
        :pattern ((smt__TLA__FunApp (smt__TLA__FunFcn_flatnd_2 smt__a) smt__x))
        :pattern ((smt__TLA__Mem smt__x smt__a)
                   (smt__TLA__FunFcn_flatnd_2 smt__a))))
    :named |FunAppDef TLA__FunFcn_flatnd_2|))

(declare-fun smt__TLA__FunFcn_flatnd_3 (Idv) Idv)

;; Axiom: FunConstrIsafcn TLA__FunFcn_flatnd_3
(assert
  (!
    (forall ((smt__a Idv))
      (! (smt__TLA__FunIsafcn (smt__TLA__FunFcn_flatnd_3 smt__a))
        :pattern ((smt__TLA__FunFcn_flatnd_3 smt__a))))
    :named |FunConstrIsafcn TLA__FunFcn_flatnd_3|))

;; Axiom: FunDomDef TLA__FunFcn_flatnd_3
(assert
  (!
    (forall ((smt__a Idv))
      (! (= (smt__TLA__FunDom (smt__TLA__FunFcn_flatnd_3 smt__a)) smt__a)
        :pattern ((smt__TLA__FunFcn_flatnd_3 smt__a))))
    :named |FunDomDef TLA__FunFcn_flatnd_3|))

;; Axiom: FunAppDef TLA__FunFcn_flatnd_3
(assert
  (!
    (forall ((smt__a Idv) (smt__x Idv))
      (!
        (=> (smt__TLA__Mem smt__x smt__a)
          (= (smt__TLA__FunApp (smt__TLA__FunFcn_flatnd_3 smt__a) smt__x)
            smt__TLA__StrLit_p1))
        :pattern ((smt__TLA__FunApp (smt__TLA__FunFcn_flatnd_3 smt__a) smt__x))
        :pattern ((smt__TLA__Mem smt__x smt__a)
                   (smt__TLA__FunFcn_flatnd_3 smt__a))))
    :named |FunAppDef TLA__FunFcn_flatnd_3|))

;; Goal
(assert
  (!
    (not
      (=>
        (and
          (smt__TLA__TrigEq_Idv smt__VARIABLE_num_
            (smt__TLA__FunFcn_flatnd_1 smt__CONSTANT_P_))
          (smt__TLA__TrigEq_Idv smt__VARIABLE_flag_
            (smt__TLA__FunFcn_flatnd_2 smt__CONSTANT_P_))
          (smt__TLA__Mem smt__VARIABLE_unread_
            (smt__TLA__FunSet smt__CONSTANT_P_
              (smt__TLA__Subset smt__CONSTANT_P_)))
          (smt__TLA__Mem smt__VARIABLE_max_
            (smt__TLA__FunSet smt__CONSTANT_P_ smt__TLA__NatSet))
          (smt__TLA__Mem smt__VARIABLE_nxt_
            (smt__TLA__FunSet smt__CONSTANT_P_ smt__CONSTANT_P_))
          (smt__TLA__TrigEq_Idv smt__VARIABLE_pc_
            (smt__TLA__FunFcn_flatnd_3 smt__CONSTANT_P_)))
        (and
          (and
            (smt__TLA__Mem smt__VARIABLE_num_
              (smt__TLA__FunSet smt__CONSTANT_P_ smt__TLA__NatSet))
            (smt__TLA__Mem smt__VARIABLE_flag_
              (smt__TLA__FunSet smt__CONSTANT_P_ smt__TLA__BoolSet))
            (smt__TLA__Mem smt__VARIABLE_unread_
              (smt__TLA__FunSet smt__CONSTANT_P_
                (smt__TLA__Subset smt__CONSTANT_P_)))
            (forall ((smt__CONSTANT_i_ Idv))
              (=> (smt__TLA__Mem smt__CONSTANT_i_ smt__CONSTANT_P_)
                (=>
                  (smt__TLA__Mem
                    (smt__TLA__FunApp smt__VARIABLE_pc_ smt__CONSTANT_i_)
                    (smt__TLA__SetEnum_3 smt__TLA__StrLit_p2
                      smt__TLA__StrLit_p5 smt__TLA__StrLit_p6))
                  (not
                    (smt__TLA__Mem smt__CONSTANT_i_
                      (smt__TLA__FunApp smt__VARIABLE_unread_
                        smt__CONSTANT_i_))))))
            (smt__TLA__Mem smt__VARIABLE_max_
              (smt__TLA__FunSet smt__CONSTANT_P_ smt__TLA__NatSet))
            (smt__TLA__Mem smt__VARIABLE_nxt_
              (smt__TLA__FunSet smt__CONSTANT_P_ smt__CONSTANT_P_))
            (forall ((smt__CONSTANT_i_ Idv))
              (=> (smt__TLA__Mem smt__CONSTANT_i_ smt__CONSTANT_P_)
                (=>
                  (smt__TLA__TrigEq_Idv
                    (smt__TLA__FunApp smt__VARIABLE_pc_ smt__CONSTANT_i_)
                    smt__TLA__StrLit_p6)
                  (not
                    (smt__TLA__TrigEq_Idv
                      (smt__TLA__FunApp smt__VARIABLE_nxt_ smt__CONSTANT_i_)
                      smt__CONSTANT_i_)))))
            (smt__TLA__Mem smt__VARIABLE_pc_
              (smt__TLA__FunSet smt__CONSTANT_P_
                (smt__TLA__SetEnum_8 smt__TLA__StrLit_p1 smt__TLA__StrLit_p2
                  smt__TLA__StrLit_p3 smt__TLA__StrLit_p4 smt__TLA__StrLit_p5
                  smt__TLA__StrLit_p6 smt__TLA__StrLit_cs smt__TLA__StrLit_p7))))
          (forall ((smt__CONSTANT_i_ Idv))
            (=> (smt__TLA__Mem smt__CONSTANT_i_ smt__CONSTANT_P_)
              (and
                (=
                  (smt__TLA__TrigEq_Idv
                    (smt__TLA__FunApp smt__VARIABLE_num_ smt__CONSTANT_i_)
                    (smt__TLA__Cast_Int 0))
                  (smt__TLA__Mem
                    (smt__TLA__FunApp smt__VARIABLE_pc_ smt__CONSTANT_i_)
                    (smt__TLA__SetEnum_3 smt__TLA__StrLit_p1
                      smt__TLA__StrLit_p2 smt__TLA__StrLit_p3)))
                (=
                  (= (smt__TLA__FunApp smt__VARIABLE_flag_ smt__CONSTANT_i_)
                    smt__TLA__Tt_Idv)
                  (smt__TLA__Mem
                    (smt__TLA__FunApp smt__VARIABLE_pc_ smt__CONSTANT_i_)
                    (smt__TLA__SetEnum_3 smt__TLA__StrLit_p2
                      smt__TLA__StrLit_p3 smt__TLA__StrLit_p4)))
                (=>
                  (smt__TLA__Mem
                    (smt__TLA__FunApp smt__VARIABLE_pc_ smt__CONSTANT_i_)
                    (smt__TLA__SetEnum_2 smt__TLA__StrLit_p5
                      smt__TLA__StrLit_p6))
                  (forall ((smt__CONSTANT_j_ Idv))
                    (=>
                      (smt__TLA__Mem smt__CONSTANT_j_
                        (smt__TLA__SetMinus
                          (smt__TLA__SetMinus smt__CONSTANT_P_
                            (smt__TLA__FunApp smt__VARIABLE_unread_
                              smt__CONSTANT_i_))
                          (smt__TLA__SetEnum_1 smt__CONSTANT_i_)))
                      (=
                        (smt__STATE_After_ smt__CONSTANT_j_ smt__CONSTANT_i_)
                        smt__TLA__Tt_Idv))))
                (=>
                  (and
                    (smt__TLA__TrigEq_Idv
                      (smt__TLA__FunApp smt__VARIABLE_pc_ smt__CONSTANT_i_)
                      smt__TLA__StrLit_p6)
                    (or
                      (and
                        (smt__TLA__TrigEq_Idv
                          (smt__TLA__FunApp smt__VARIABLE_pc_
                            (smt__TLA__FunApp smt__VARIABLE_nxt_
                              smt__CONSTANT_i_)) smt__TLA__StrLit_p2)
                        (not
                          (smt__TLA__Mem smt__CONSTANT_i_
                            (smt__TLA__FunApp smt__VARIABLE_unread_
                              (smt__TLA__FunApp smt__VARIABLE_nxt_
                                smt__CONSTANT_i_)))))
                      (smt__TLA__TrigEq_Idv
                        (smt__TLA__FunApp smt__VARIABLE_pc_
                          (smt__TLA__FunApp smt__VARIABLE_nxt_
                            smt__CONSTANT_i_)) smt__TLA__StrLit_p3)))
                  (smt__TLA__IntLteq
                    (smt__TLA__FunApp smt__VARIABLE_num_ smt__CONSTANT_i_)
                    (smt__TLA__FunApp smt__VARIABLE_max_
                      (smt__TLA__FunApp smt__VARIABLE_nxt_ smt__CONSTANT_i_))))
                (=>
                  (smt__TLA__Mem
                    (smt__TLA__FunApp smt__VARIABLE_pc_ smt__CONSTANT_i_)
                    (smt__TLA__SetEnum_2 smt__TLA__StrLit_cs
                      smt__TLA__StrLit_p7))
                  (forall ((smt__CONSTANT_j_ Idv))
                    (=>
                      (smt__TLA__Mem smt__CONSTANT_j_
                        (smt__TLA__SetMinus smt__CONSTANT_P_
                          (smt__TLA__SetEnum_1 smt__CONSTANT_i_)))
                      (=
                        (smt__STATE_After_ smt__CONSTANT_j_ smt__CONSTANT_i_)
                        smt__TLA__Tt_Idv)))))))))) :named |Goal|))

(check-sat)
(exit)
