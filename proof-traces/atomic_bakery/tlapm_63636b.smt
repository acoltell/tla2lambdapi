;; Proof obligation:
;;	ASSUME NEW CONSTANT CONSTANT_N_,
;;	       NEW VARIABLE VARIABLE_num_,
;;	       NEW VARIABLE VARIABLE_flag_,
;;	       NEW VARIABLE VARIABLE_pc_,
;;	       NEW VARIABLE VARIABLE_unread_,
;;	       NEW VARIABLE VARIABLE_max_,
;;	       NEW VARIABLE VARIABLE_nxt_,
;;	       (/\ VARIABLE_num_ \in [CONSTANT_P_ -> Nat]
;;	        /\ VARIABLE_flag_ \in [CONSTANT_P_ -> BOOLEAN]
;;	        /\ VARIABLE_unread_ \in [CONSTANT_P_ -> SUBSET CONSTANT_P_]
;;	        /\ \A CONSTANT_i_ \in CONSTANT_P_ :
;;	              VARIABLE_pc_[CONSTANT_i_] \in {"p2", "p5", "p6"}
;;	              => CONSTANT_i_ \notin VARIABLE_unread_[CONSTANT_i_]
;;	        /\ VARIABLE_max_ \in [CONSTANT_P_ -> Nat]
;;	        /\ VARIABLE_nxt_ \in [CONSTANT_P_ -> CONSTANT_P_]
;;	        /\ \A CONSTANT_i_ \in CONSTANT_P_ :
;;	              VARIABLE_pc_[CONSTANT_i_] = "p6"
;;	              => VARIABLE_nxt_[CONSTANT_i_] # CONSTANT_i_
;;	        /\ VARIABLE_pc_
;;	           \in [CONSTANT_P_ ->
;;	                  {"p1", "p2", "p3", "p4", "p5", "p6", "cs", "p7"}])
;;	       /\ (\A CONSTANT_i_ \in CONSTANT_P_ :
;;	              /\ VARIABLE_num_[CONSTANT_i_] = 0
;;	                 <=> VARIABLE_pc_[CONSTANT_i_] \in {"p1", "p2", "p3"}
;;	              /\ VARIABLE_flag_[CONSTANT_i_]
;;	                 <=> VARIABLE_pc_[CONSTANT_i_] \in {"p2", "p3", "p4"}
;;	              /\ VARIABLE_pc_[CONSTANT_i_] \in {"p5", "p6"}
;;	                 => (\A CONSTANT_j_
;;	                        \in (CONSTANT_P_ \ VARIABLE_unread_[CONSTANT_i_])
;;	                            \ {CONSTANT_i_} :
;;	                        /\ VARIABLE_num_[CONSTANT_i_] > 0
;;	                        /\ \/ VARIABLE_pc_[CONSTANT_j_] = "p1"
;;	                           \/ /\ VARIABLE_pc_[CONSTANT_j_] = "p2"
;;	                              /\ \/ CONSTANT_i_
;;	                                    \in VARIABLE_unread_[CONSTANT_j_]
;;	                                 \/ VARIABLE_max_[CONSTANT_j_]
;;	                                    >= VARIABLE_num_[CONSTANT_i_]
;;	                           \/ /\ VARIABLE_pc_[CONSTANT_j_] = "p3"
;;	                              /\ VARIABLE_max_[CONSTANT_j_]
;;	                                 >= VARIABLE_num_[CONSTANT_i_]
;;	                           \/ /\ VARIABLE_pc_[CONSTANT_j_]
;;	                                 \in {"p4", "p5", "p6"}
;;	                              /\ \/ VARIABLE_num_[CONSTANT_i_]
;;	                                    < VARIABLE_num_[CONSTANT_j_]
;;	                                 \/ /\ VARIABLE_num_[CONSTANT_j_]
;;	                                       = VARIABLE_num_[CONSTANT_i_]
;;	                                    /\ CONSTANT_i_ =< CONSTANT_j_
;;	                              /\ VARIABLE_pc_[CONSTANT_j_] \in {"p5", "p6"}
;;	                                 => CONSTANT_i_
;;	                                    \in VARIABLE_unread_[CONSTANT_j_])
;;	              /\ (/\ VARIABLE_pc_[CONSTANT_i_] = "p6"
;;	                  /\ \/ VARIABLE_pc_[VARIABLE_nxt_[CONSTANT_i_]] = "p2"
;;	                        /\ CONSTANT_i_
;;	                           \notin VARIABLE_unread_[VARIABLE_nxt_[CONSTANT_i_]]
;;	                     \/ VARIABLE_pc_[VARIABLE_nxt_[CONSTANT_i_]] = "p3")
;;	                 => VARIABLE_max_[VARIABLE_nxt_[CONSTANT_i_]]
;;	                    >= VARIABLE_num_[CONSTANT_i_]
;;	              /\ VARIABLE_pc_[CONSTANT_i_] \in {"cs", "p7"}
;;	                 => (\A CONSTANT_j_ \in CONSTANT_P_ \ {CONSTANT_i_} :
;;	                        /\ VARIABLE_num_[CONSTANT_i_] > 0
;;	                        /\ \/ VARIABLE_pc_[CONSTANT_j_] = "p1"
;;	                           \/ /\ VARIABLE_pc_[CONSTANT_j_] = "p2"
;;	                              /\ \/ CONSTANT_i_
;;	                                    \in VARIABLE_unread_[CONSTANT_j_]
;;	                                 \/ VARIABLE_max_[CONSTANT_j_]
;;	                                    >= VARIABLE_num_[CONSTANT_i_]
;;	                           \/ /\ VARIABLE_pc_[CONSTANT_j_] = "p3"
;;	                              /\ VARIABLE_max_[CONSTANT_j_]
;;	                                 >= VARIABLE_num_[CONSTANT_i_]
;;	                           \/ /\ VARIABLE_pc_[CONSTANT_j_]
;;	                                 \in {"p4", "p5", "p6"}
;;	                              /\ \/ VARIABLE_num_[CONSTANT_i_]
;;	                                    < VARIABLE_num_[CONSTANT_j_]
;;	                                 \/ /\ VARIABLE_num_[CONSTANT_j_]
;;	                                       = VARIABLE_num_[CONSTANT_i_]
;;	                                    /\ CONSTANT_i_ =< CONSTANT_j_
;;	                              /\ VARIABLE_pc_[CONSTANT_j_] \in {"p5", "p6"}
;;	                                 => CONSTANT_i_
;;	                                    \in VARIABLE_unread_[CONSTANT_j_])) ,
;;	       ACTION_Next_
;;	       \/ (/\ ?VARIABLE_num_#prime = VARIABLE_num_
;;	           /\ ?VARIABLE_flag_#prime = VARIABLE_flag_
;;	           /\ ?VARIABLE_pc_#prime = VARIABLE_pc_
;;	           /\ ?VARIABLE_unread_#prime = VARIABLE_unread_
;;	           /\ ?VARIABLE_max_#prime = VARIABLE_max_
;;	           /\ ?VARIABLE_nxt_#prime = VARIABLE_nxt_) ,
;;	       ?VARIABLE_num_#prime = VARIABLE_num_ ,
;;	       ?VARIABLE_flag_#prime = VARIABLE_flag_ ,
;;	       ?VARIABLE_pc_#prime = VARIABLE_pc_ ,
;;	       ?VARIABLE_unread_#prime = VARIABLE_unread_ ,
;;	       ?VARIABLE_max_#prime = VARIABLE_max_ ,
;;	       ?VARIABLE_nxt_#prime = VARIABLE_nxt_ 
;;	PROVE  (/\ ?VARIABLE_num_#prime \in [CONSTANT_P_ -> Nat]
;;	        /\ ?VARIABLE_flag_#prime \in [CONSTANT_P_ -> BOOLEAN]
;;	        /\ ?VARIABLE_unread_#prime \in [CONSTANT_P_ -> SUBSET CONSTANT_P_]
;;	        /\ \A CONSTANT_i_ \in CONSTANT_P_ :
;;	              ?VARIABLE_pc_#prime[CONSTANT_i_] \in {"p2", "p5", "p6"}
;;	              => CONSTANT_i_ \notin ?VARIABLE_unread_#prime[CONSTANT_i_]
;;	        /\ ?VARIABLE_max_#prime \in [CONSTANT_P_ -> Nat]
;;	        /\ ?VARIABLE_nxt_#prime \in [CONSTANT_P_ -> CONSTANT_P_]
;;	        /\ \A CONSTANT_i_ \in CONSTANT_P_ :
;;	              ?VARIABLE_pc_#prime[CONSTANT_i_] = "p6"
;;	              => ?VARIABLE_nxt_#prime[CONSTANT_i_] # CONSTANT_i_
;;	        /\ ?VARIABLE_pc_#prime
;;	           \in [CONSTANT_P_ ->
;;	                  {"p1", "p2", "p3", "p4", "p5", "p6", "cs", "p7"}])
;;	       /\ (\A CONSTANT_i_ \in CONSTANT_P_ :
;;	              /\ ?VARIABLE_num_#prime[CONSTANT_i_] = 0
;;	                 <=> ?VARIABLE_pc_#prime[CONSTANT_i_] \in {"p1", "p2", "p3"}
;;	              /\ ?VARIABLE_flag_#prime[CONSTANT_i_]
;;	                 <=> ?VARIABLE_pc_#prime[CONSTANT_i_] \in {"p2", "p3", "p4"}
;;	              /\ ?VARIABLE_pc_#prime[CONSTANT_i_] \in {"p5", "p6"}
;;	                 => (\A CONSTANT_j_
;;	                        \in (CONSTANT_P_
;;	                             \ ?VARIABLE_unread_#prime[CONSTANT_i_])
;;	                            \ {CONSTANT_i_} :
;;	                        /\ ?VARIABLE_num_#prime[CONSTANT_i_] > 0
;;	                        /\ \/ ?VARIABLE_pc_#prime[CONSTANT_j_] = "p1"
;;	                           \/ /\ ?VARIABLE_pc_#prime[CONSTANT_j_] = "p2"
;;	                              /\ \/ CONSTANT_i_
;;	                                    \in ?VARIABLE_unread_#prime[CONSTANT_j_]
;;	                                 \/ ?VARIABLE_max_#prime[CONSTANT_j_]
;;	                                    >= ?VARIABLE_num_#prime[CONSTANT_i_]
;;	                           \/ /\ ?VARIABLE_pc_#prime[CONSTANT_j_] = "p3"
;;	                              /\ ?VARIABLE_max_#prime[CONSTANT_j_]
;;	                                 >= ?VARIABLE_num_#prime[CONSTANT_i_]
;;	                           \/ /\ ?VARIABLE_pc_#prime[CONSTANT_j_]
;;	                                 \in {"p4", "p5", "p6"}
;;	                              /\ \/ ?VARIABLE_num_#prime[CONSTANT_i_]
;;	                                    < ?VARIABLE_num_#prime[CONSTANT_j_]
;;	                                 \/ /\ ?VARIABLE_num_#prime[CONSTANT_j_]
;;	                                       = ?VARIABLE_num_#prime[CONSTANT_i_]
;;	                                    /\ CONSTANT_i_ =< CONSTANT_j_
;;	                              /\ ?VARIABLE_pc_#prime[CONSTANT_j_]
;;	                                 \in {"p5", "p6"}
;;	                                 => CONSTANT_i_
;;	                                    \in ?VARIABLE_unread_#prime[CONSTANT_j_])
;;	              /\ (/\ ?VARIABLE_pc_#prime[CONSTANT_i_] = "p6"
;;	                  /\ \/ ?VARIABLE_pc_#prime[?VARIABLE_nxt_#prime[CONSTANT_i_]]
;;	                        = "p2"
;;	                        /\ CONSTANT_i_
;;	                           \notin ?VARIABLE_unread_#prime[?VARIABLE_nxt_#prime[CONSTANT_i_]]
;;	                     \/ ?VARIABLE_pc_#prime[?VARIABLE_nxt_#prime[CONSTANT_i_]]
;;	                        = "p3")
;;	                 => ?VARIABLE_max_#prime[?VARIABLE_nxt_#prime[CONSTANT_i_]]
;;	                    >= ?VARIABLE_num_#prime[CONSTANT_i_]
;;	              /\ ?VARIABLE_pc_#prime[CONSTANT_i_] \in {"cs", "p7"}
;;	                 => (\A CONSTANT_j_ \in CONSTANT_P_ \ {CONSTANT_i_} :
;;	                        /\ ?VARIABLE_num_#prime[CONSTANT_i_] > 0
;;	                        /\ \/ ?VARIABLE_pc_#prime[CONSTANT_j_] = "p1"
;;	                           \/ /\ ?VARIABLE_pc_#prime[CONSTANT_j_] = "p2"
;;	                              /\ \/ CONSTANT_i_
;;	                                    \in ?VARIABLE_unread_#prime[CONSTANT_j_]
;;	                                 \/ ?VARIABLE_max_#prime[CONSTANT_j_]
;;	                                    >= ?VARIABLE_num_#prime[CONSTANT_i_]
;;	                           \/ /\ ?VARIABLE_pc_#prime[CONSTANT_j_] = "p3"
;;	                              /\ ?VARIABLE_max_#prime[CONSTANT_j_]
;;	                                 >= ?VARIABLE_num_#prime[CONSTANT_i_]
;;	                           \/ /\ ?VARIABLE_pc_#prime[CONSTANT_j_]
;;	                                 \in {"p4", "p5", "p6"}
;;	                              /\ \/ ?VARIABLE_num_#prime[CONSTANT_i_]
;;	                                    < ?VARIABLE_num_#prime[CONSTANT_j_]
;;	                                 \/ /\ ?VARIABLE_num_#prime[CONSTANT_j_]
;;	                                       = ?VARIABLE_num_#prime[CONSTANT_i_]
;;	                                    /\ CONSTANT_i_ =< CONSTANT_j_
;;	                              /\ ?VARIABLE_pc_#prime[CONSTANT_j_]
;;	                                 \in {"p5", "p6"}
;;	                                 => CONSTANT_i_
;;	                                    \in ?VARIABLE_unread_#prime[CONSTANT_j_]))
;; TLA+ Proof Manager 1.5.0
;; Proof obligation #59
;; Generated from file "./tla_specs/Original/TLAPS_Examples/AtomicBakery.tla", line 283, characters 5-6

(set-logic UFLIA)

;; Sorts

(declare-sort Idv 0)

;; Hypotheses

(declare-fun smt__TLA__BoolSet () Idv)

(declare-fun smt__TLA__Cast_Int (Int) Idv)

(declare-fun smt__TLA__FunApp (Idv Idv) Idv)

(declare-fun smt__TLA__FunDom (Idv) Idv)

; omitted declaration of 'TLA__FunFcn' (second-order)

(declare-fun smt__TLA__FunIsafcn (Idv) Bool)

(declare-fun smt__TLA__FunSet (Idv Idv) Idv)

(declare-fun smt__TLA__IntLteq (Idv Idv) Bool)

(declare-fun smt__TLA__IntSet () Idv)

(declare-fun smt__TLA__Mem (Idv Idv) Bool)

(declare-fun smt__TLA__NatSet () Idv)

(declare-fun smt__TLA__Proj_Int (Idv) Int)

(declare-fun smt__TLA__SetEnum_1 (Idv) Idv)

(declare-fun smt__TLA__SetEnum_2 (Idv Idv) Idv)

(declare-fun smt__TLA__SetEnum_3 (Idv Idv Idv) Idv)

(declare-fun smt__TLA__SetEnum_8 (Idv Idv Idv Idv Idv Idv Idv Idv) Idv)

(declare-fun smt__TLA__SetExtTrigger (Idv Idv) Bool)

(declare-fun smt__TLA__SetMinus (Idv Idv) Idv)

(declare-fun smt__TLA__StrLit_cs () Idv)

(declare-fun smt__TLA__StrLit_p1 () Idv)

(declare-fun smt__TLA__StrLit_p2 () Idv)

(declare-fun smt__TLA__StrLit_p3 () Idv)

(declare-fun smt__TLA__StrLit_p4 () Idv)

(declare-fun smt__TLA__StrLit_p5 () Idv)

(declare-fun smt__TLA__StrLit_p6 () Idv)

(declare-fun smt__TLA__StrLit_p7 () Idv)

(declare-fun smt__TLA__StrSet () Idv)

(declare-fun smt__TLA__Subset (Idv) Idv)

(declare-fun smt__TLA__SubsetEq (Idv Idv) Bool)

(declare-fun smt__TLA__TrigEq_Idv (Idv Idv) Bool)

(declare-fun smt__TLA__Tt_Idv () Idv)

;; Axiom: SetExt
(assert
  (!
    (forall ((smt__x Idv) (smt__y Idv))
      (!
        (=>
          (forall ((smt__z Idv))
            (= (smt__TLA__Mem smt__z smt__x) (smt__TLA__Mem smt__z smt__y)))
          (= smt__x smt__y))
        :pattern ((smt__TLA__SetExtTrigger smt__x smt__y)))) :named |SetExt|))

;; Axiom: SubsetEqIntro
(assert
  (!
    (forall ((smt__x Idv) (smt__y Idv))
      (!
        (=>
          (forall ((smt__z Idv))
            (=> (smt__TLA__Mem smt__z smt__x) (smt__TLA__Mem smt__z smt__y)))
          (smt__TLA__SubsetEq smt__x smt__y))
        :pattern ((smt__TLA__SubsetEq smt__x smt__y))))
    :named |SubsetEqIntro|))

;; Axiom: SubsetEqElim
(assert
  (!
    (forall ((smt__x Idv) (smt__y Idv) (smt__z Idv))
      (!
        (=>
          (and (smt__TLA__SubsetEq smt__x smt__y)
            (smt__TLA__Mem smt__z smt__x)) (smt__TLA__Mem smt__z smt__y))
        :pattern ((smt__TLA__SubsetEq smt__x smt__y)
                   (smt__TLA__Mem smt__z smt__x)))) :named |SubsetEqElim|))

;; Axiom: SubsetDefAlt
(assert
  (!
    (forall ((smt__a Idv) (smt__x Idv))
      (!
        (= (smt__TLA__Mem smt__x (smt__TLA__Subset smt__a))
          (smt__TLA__SubsetEq smt__x smt__a))
        :pattern ((smt__TLA__Mem smt__x (smt__TLA__Subset smt__a)))
        :pattern ((smt__TLA__SubsetEq smt__x smt__a)
                   (smt__TLA__Subset smt__a)))) :named |SubsetDefAlt|))

;; Axiom: SetMinusDef
(assert
  (!
    (forall ((smt__a Idv) (smt__b Idv) (smt__x Idv))
      (!
        (= (smt__TLA__Mem smt__x (smt__TLA__SetMinus smt__a smt__b))
          (and (smt__TLA__Mem smt__x smt__a)
            (not (smt__TLA__Mem smt__x smt__b))))
        :pattern ((smt__TLA__Mem smt__x (smt__TLA__SetMinus smt__a smt__b)))
        :pattern ((smt__TLA__Mem smt__x smt__a)
                   (smt__TLA__SetMinus smt__a smt__b))
        :pattern ((smt__TLA__Mem smt__x smt__b)
                   (smt__TLA__SetMinus smt__a smt__b)))) :named |SetMinusDef|))

;; Axiom: NatSetDef
(assert
  (!
    (forall ((smt__x Idv))
      (!
        (= (smt__TLA__Mem smt__x smt__TLA__NatSet)
          (and (smt__TLA__Mem smt__x smt__TLA__IntSet)
            (smt__TLA__IntLteq (smt__TLA__Cast_Int 0) smt__x)))
        :pattern ((smt__TLA__Mem smt__x smt__TLA__NatSet))))
    :named |NatSetDef|))

;; Axiom: FunExt
(assert
  (!
    (forall ((smt__f Idv) (smt__g Idv))
      (!
        (=>
          (and (smt__TLA__FunIsafcn smt__f) (smt__TLA__FunIsafcn smt__g)
            (= (smt__TLA__FunDom smt__f) (smt__TLA__FunDom smt__g))
            (forall ((smt__x Idv))
              (=> (smt__TLA__Mem smt__x (smt__TLA__FunDom smt__f))
                (= (smt__TLA__FunApp smt__f smt__x)
                  (smt__TLA__FunApp smt__g smt__x))))) (= smt__f smt__g))
        :pattern ((smt__TLA__FunIsafcn smt__f) (smt__TLA__FunIsafcn smt__g))))
    :named |FunExt|))

; omitted fact (second-order)

;; Axiom: FunSetIntro
(assert
  (!
    (forall ((smt__a Idv) (smt__b Idv) (smt__f Idv))
      (!
        (=>
          (and (smt__TLA__FunIsafcn smt__f)
            (= (smt__TLA__FunDom smt__f) smt__a)
            (forall ((smt__x Idv))
              (=> (smt__TLA__Mem smt__x smt__a)
                (smt__TLA__Mem (smt__TLA__FunApp smt__f smt__x) smt__b))))
          (smt__TLA__Mem smt__f (smt__TLA__FunSet smt__a smt__b)))
        :pattern ((smt__TLA__Mem smt__f (smt__TLA__FunSet smt__a smt__b)))))
    :named |FunSetIntro|))

;; Axiom: FunSetElim1
(assert
  (!
    (forall ((smt__a Idv) (smt__b Idv) (smt__f Idv))
      (!
        (=> (smt__TLA__Mem smt__f (smt__TLA__FunSet smt__a smt__b))
          (and (smt__TLA__FunIsafcn smt__f)
            (= (smt__TLA__FunDom smt__f) smt__a)))
        :pattern ((smt__TLA__Mem smt__f (smt__TLA__FunSet smt__a smt__b)))))
    :named |FunSetElim1|))

;; Axiom: FunSetElim2
(assert
  (!
    (forall ((smt__a Idv) (smt__b Idv) (smt__f Idv) (smt__x Idv))
      (!
        (=>
          (and (smt__TLA__Mem smt__f (smt__TLA__FunSet smt__a smt__b))
            (smt__TLA__Mem smt__x smt__a))
          (smt__TLA__Mem (smt__TLA__FunApp smt__f smt__x) smt__b))
        :pattern ((smt__TLA__Mem smt__f (smt__TLA__FunSet smt__a smt__b))
                   (smt__TLA__Mem smt__x smt__a))
        :pattern ((smt__TLA__Mem smt__f (smt__TLA__FunSet smt__a smt__b))
                   (smt__TLA__FunApp smt__f smt__x)))) :named |FunSetElim2|))

; omitted fact (second-order)

; omitted fact (second-order)

;; Axiom: EnumDefIntro 1
(assert
  (!
    (forall ((smt__a1 Idv))
      (! (smt__TLA__Mem smt__a1 (smt__TLA__SetEnum_1 smt__a1))
        :pattern ((smt__TLA__SetEnum_1 smt__a1)))) :named |EnumDefIntro 1|))

;; Axiom: EnumDefIntro 2
(assert
  (!
    (forall ((smt__a1 Idv) (smt__a2 Idv))
      (!
        (and (smt__TLA__Mem smt__a1 (smt__TLA__SetEnum_2 smt__a1 smt__a2))
          (smt__TLA__Mem smt__a2 (smt__TLA__SetEnum_2 smt__a1 smt__a2)))
        :pattern ((smt__TLA__SetEnum_2 smt__a1 smt__a2))))
    :named |EnumDefIntro 2|))

;; Axiom: EnumDefIntro 3
(assert
  (!
    (forall ((smt__a1 Idv) (smt__a2 Idv) (smt__a3 Idv))
      (!
        (and
          (smt__TLA__Mem smt__a1
            (smt__TLA__SetEnum_3 smt__a1 smt__a2 smt__a3))
          (smt__TLA__Mem smt__a2
            (smt__TLA__SetEnum_3 smt__a1 smt__a2 smt__a3))
          (smt__TLA__Mem smt__a3
            (smt__TLA__SetEnum_3 smt__a1 smt__a2 smt__a3)))
        :pattern ((smt__TLA__SetEnum_3 smt__a1 smt__a2 smt__a3))))
    :named |EnumDefIntro 3|))

;; Axiom: EnumDefIntro 8
(assert
  (!
    (forall
      ((smt__a1 Idv) (smt__a2 Idv) (smt__a3 Idv) (smt__a4 Idv) (smt__a5 Idv)
        (smt__a6 Idv) (smt__a7 Idv) (smt__a8 Idv))
      (!
        (and
          (smt__TLA__Mem smt__a1
            (smt__TLA__SetEnum_8 smt__a1 smt__a2 smt__a3 smt__a4 smt__a5
              smt__a6 smt__a7 smt__a8))
          (smt__TLA__Mem smt__a2
            (smt__TLA__SetEnum_8 smt__a1 smt__a2 smt__a3 smt__a4 smt__a5
              smt__a6 smt__a7 smt__a8))
          (smt__TLA__Mem smt__a3
            (smt__TLA__SetEnum_8 smt__a1 smt__a2 smt__a3 smt__a4 smt__a5
              smt__a6 smt__a7 smt__a8))
          (smt__TLA__Mem smt__a4
            (smt__TLA__SetEnum_8 smt__a1 smt__a2 smt__a3 smt__a4 smt__a5
              smt__a6 smt__a7 smt__a8))
          (smt__TLA__Mem smt__a5
            (smt__TLA__SetEnum_8 smt__a1 smt__a2 smt__a3 smt__a4 smt__a5
              smt__a6 smt__a7 smt__a8))
          (smt__TLA__Mem smt__a6
            (smt__TLA__SetEnum_8 smt__a1 smt__a2 smt__a3 smt__a4 smt__a5
              smt__a6 smt__a7 smt__a8))
          (smt__TLA__Mem smt__a7
            (smt__TLA__SetEnum_8 smt__a1 smt__a2 smt__a3 smt__a4 smt__a5
              smt__a6 smt__a7 smt__a8))
          (smt__TLA__Mem smt__a8
            (smt__TLA__SetEnum_8 smt__a1 smt__a2 smt__a3 smt__a4 smt__a5
              smt__a6 smt__a7 smt__a8)))
        :pattern ((smt__TLA__SetEnum_8 smt__a1 smt__a2 smt__a3 smt__a4
                    smt__a5 smt__a6 smt__a7 smt__a8))))
    :named |EnumDefIntro 8|))

;; Axiom: EnumDefElim 1
(assert
  (!
    (forall ((smt__a1 Idv) (smt__x Idv))
      (!
        (=> (smt__TLA__Mem smt__x (smt__TLA__SetEnum_1 smt__a1))
          (= smt__x smt__a1))
        :pattern ((smt__TLA__Mem smt__x (smt__TLA__SetEnum_1 smt__a1)))))
    :named |EnumDefElim 1|))

;; Axiom: EnumDefElim 2
(assert
  (!
    (forall ((smt__a1 Idv) (smt__a2 Idv) (smt__x Idv))
      (!
        (=> (smt__TLA__Mem smt__x (smt__TLA__SetEnum_2 smt__a1 smt__a2))
          (or (= smt__x smt__a1) (= smt__x smt__a2)))
        :pattern ((smt__TLA__Mem smt__x (smt__TLA__SetEnum_2 smt__a1 smt__a2)))))
    :named |EnumDefElim 2|))

;; Axiom: EnumDefElim 3
(assert
  (!
    (forall ((smt__a1 Idv) (smt__a2 Idv) (smt__a3 Idv) (smt__x Idv))
      (!
        (=>
          (smt__TLA__Mem smt__x (smt__TLA__SetEnum_3 smt__a1 smt__a2 smt__a3))
          (or (= smt__x smt__a1) (= smt__x smt__a2) (= smt__x smt__a3)))
        :pattern ((smt__TLA__Mem smt__x
                    (smt__TLA__SetEnum_3 smt__a1 smt__a2 smt__a3)))))
    :named |EnumDefElim 3|))

;; Axiom: EnumDefElim 8
(assert
  (!
    (forall
      ((smt__a1 Idv) (smt__a2 Idv) (smt__a3 Idv) (smt__a4 Idv) (smt__a5 Idv)
        (smt__a6 Idv) (smt__a7 Idv) (smt__a8 Idv) (smt__x Idv))
      (!
        (=>
          (smt__TLA__Mem smt__x
            (smt__TLA__SetEnum_8 smt__a1 smt__a2 smt__a3 smt__a4 smt__a5
              smt__a6 smt__a7 smt__a8))
          (or (= smt__x smt__a1) (= smt__x smt__a2) (= smt__x smt__a3)
            (= smt__x smt__a4) (= smt__x smt__a5) (= smt__x smt__a6)
            (= smt__x smt__a7) (= smt__x smt__a8)))
        :pattern ((smt__TLA__Mem smt__x
                    (smt__TLA__SetEnum_8 smt__a1 smt__a2 smt__a3 smt__a4
                      smt__a5 smt__a6 smt__a7 smt__a8)))))
    :named |EnumDefElim 8|))

;; Axiom: StrLitIsstr cs
(assert
  (! (smt__TLA__Mem smt__TLA__StrLit_cs smt__TLA__StrSet)
    :named |StrLitIsstr cs|))

;; Axiom: StrLitIsstr p1
(assert
  (! (smt__TLA__Mem smt__TLA__StrLit_p1 smt__TLA__StrSet)
    :named |StrLitIsstr p1|))

;; Axiom: StrLitIsstr p2
(assert
  (! (smt__TLA__Mem smt__TLA__StrLit_p2 smt__TLA__StrSet)
    :named |StrLitIsstr p2|))

;; Axiom: StrLitIsstr p3
(assert
  (! (smt__TLA__Mem smt__TLA__StrLit_p3 smt__TLA__StrSet)
    :named |StrLitIsstr p3|))

;; Axiom: StrLitIsstr p4
(assert
  (! (smt__TLA__Mem smt__TLA__StrLit_p4 smt__TLA__StrSet)
    :named |StrLitIsstr p4|))

;; Axiom: StrLitIsstr p5
(assert
  (! (smt__TLA__Mem smt__TLA__StrLit_p5 smt__TLA__StrSet)
    :named |StrLitIsstr p5|))

;; Axiom: StrLitIsstr p6
(assert
  (! (smt__TLA__Mem smt__TLA__StrLit_p6 smt__TLA__StrSet)
    :named |StrLitIsstr p6|))

;; Axiom: StrLitIsstr p7
(assert
  (! (smt__TLA__Mem smt__TLA__StrLit_p7 smt__TLA__StrSet)
    :named |StrLitIsstr p7|))

;; Axiom: StrLitDistinct cs p1
(assert
  (! (distinct smt__TLA__StrLit_cs smt__TLA__StrLit_p1)
    :named |StrLitDistinct cs p1|))

;; Axiom: StrLitDistinct cs p2
(assert
  (! (distinct smt__TLA__StrLit_cs smt__TLA__StrLit_p2)
    :named |StrLitDistinct cs p2|))

;; Axiom: StrLitDistinct cs p3
(assert
  (! (distinct smt__TLA__StrLit_cs smt__TLA__StrLit_p3)
    :named |StrLitDistinct cs p3|))

;; Axiom: StrLitDistinct cs p4
(assert
  (! (distinct smt__TLA__StrLit_cs smt__TLA__StrLit_p4)
    :named |StrLitDistinct cs p4|))

;; Axiom: StrLitDistinct cs p5
(assert
  (! (distinct smt__TLA__StrLit_cs smt__TLA__StrLit_p5)
    :named |StrLitDistinct cs p5|))

;; Axiom: StrLitDistinct cs p6
(assert
  (! (distinct smt__TLA__StrLit_cs smt__TLA__StrLit_p6)
    :named |StrLitDistinct cs p6|))

;; Axiom: StrLitDistinct p1 p2
(assert
  (! (distinct smt__TLA__StrLit_p1 smt__TLA__StrLit_p2)
    :named |StrLitDistinct p1 p2|))

;; Axiom: StrLitDistinct p1 p5
(assert
  (! (distinct smt__TLA__StrLit_p1 smt__TLA__StrLit_p5)
    :named |StrLitDistinct p1 p5|))

;; Axiom: StrLitDistinct p1 p6
(assert
  (! (distinct smt__TLA__StrLit_p1 smt__TLA__StrLit_p6)
    :named |StrLitDistinct p1 p6|))

;; Axiom: StrLitDistinct p3 p1
(assert
  (! (distinct smt__TLA__StrLit_p3 smt__TLA__StrLit_p1)
    :named |StrLitDistinct p3 p1|))

;; Axiom: StrLitDistinct p3 p2
(assert
  (! (distinct smt__TLA__StrLit_p3 smt__TLA__StrLit_p2)
    :named |StrLitDistinct p3 p2|))

;; Axiom: StrLitDistinct p3 p5
(assert
  (! (distinct smt__TLA__StrLit_p3 smt__TLA__StrLit_p5)
    :named |StrLitDistinct p3 p5|))

;; Axiom: StrLitDistinct p3 p6
(assert
  (! (distinct smt__TLA__StrLit_p3 smt__TLA__StrLit_p6)
    :named |StrLitDistinct p3 p6|))

;; Axiom: StrLitDistinct p4 p1
(assert
  (! (distinct smt__TLA__StrLit_p4 smt__TLA__StrLit_p1)
    :named |StrLitDistinct p4 p1|))

;; Axiom: StrLitDistinct p4 p2
(assert
  (! (distinct smt__TLA__StrLit_p4 smt__TLA__StrLit_p2)
    :named |StrLitDistinct p4 p2|))

;; Axiom: StrLitDistinct p4 p3
(assert
  (! (distinct smt__TLA__StrLit_p4 smt__TLA__StrLit_p3)
    :named |StrLitDistinct p4 p3|))

;; Axiom: StrLitDistinct p4 p5
(assert
  (! (distinct smt__TLA__StrLit_p4 smt__TLA__StrLit_p5)
    :named |StrLitDistinct p4 p5|))

;; Axiom: StrLitDistinct p4 p6
(assert
  (! (distinct smt__TLA__StrLit_p4 smt__TLA__StrLit_p6)
    :named |StrLitDistinct p4 p6|))

;; Axiom: StrLitDistinct p5 p2
(assert
  (! (distinct smt__TLA__StrLit_p5 smt__TLA__StrLit_p2)
    :named |StrLitDistinct p5 p2|))

;; Axiom: StrLitDistinct p6 p2
(assert
  (! (distinct smt__TLA__StrLit_p6 smt__TLA__StrLit_p2)
    :named |StrLitDistinct p6 p2|))

;; Axiom: StrLitDistinct p6 p5
(assert
  (! (distinct smt__TLA__StrLit_p6 smt__TLA__StrLit_p5)
    :named |StrLitDistinct p6 p5|))

;; Axiom: StrLitDistinct p7 cs
(assert
  (! (distinct smt__TLA__StrLit_p7 smt__TLA__StrLit_cs)
    :named |StrLitDistinct p7 cs|))

;; Axiom: StrLitDistinct p7 p1
(assert
  (! (distinct smt__TLA__StrLit_p7 smt__TLA__StrLit_p1)
    :named |StrLitDistinct p7 p1|))

;; Axiom: StrLitDistinct p7 p2
(assert
  (! (distinct smt__TLA__StrLit_p7 smt__TLA__StrLit_p2)
    :named |StrLitDistinct p7 p2|))

;; Axiom: StrLitDistinct p7 p3
(assert
  (! (distinct smt__TLA__StrLit_p7 smt__TLA__StrLit_p3)
    :named |StrLitDistinct p7 p3|))

;; Axiom: StrLitDistinct p7 p4
(assert
  (! (distinct smt__TLA__StrLit_p7 smt__TLA__StrLit_p4)
    :named |StrLitDistinct p7 p4|))

;; Axiom: StrLitDistinct p7 p5
(assert
  (! (distinct smt__TLA__StrLit_p7 smt__TLA__StrLit_p5)
    :named |StrLitDistinct p7 p5|))

;; Axiom: StrLitDistinct p7 p6
(assert
  (! (distinct smt__TLA__StrLit_p7 smt__TLA__StrLit_p6)
    :named |StrLitDistinct p7 p6|))

;; Axiom: CastInjAlt Int
(assert
  (!
    (forall ((smt__x Int))
      (! (= smt__x (smt__TLA__Proj_Int (smt__TLA__Cast_Int smt__x)))
        :pattern ((smt__TLA__Cast_Int smt__x)))) :named |CastInjAlt Int|))

;; Axiom: TypeGuardIntro Int
(assert
  (!
    (forall ((smt__z Int))
      (! (smt__TLA__Mem (smt__TLA__Cast_Int smt__z) smt__TLA__IntSet)
        :pattern ((smt__TLA__Cast_Int smt__z)))) :named |TypeGuardIntro Int|))

;; Axiom: TypeGuardElim Int
(assert
  (!
    (forall ((smt__x Idv))
      (!
        (=> (smt__TLA__Mem smt__x smt__TLA__IntSet)
          (= smt__x (smt__TLA__Cast_Int (smt__TLA__Proj_Int smt__x))))
        :pattern ((smt__TLA__Mem smt__x smt__TLA__IntSet))))
    :named |TypeGuardElim Int|))

;; Axiom: Typing TIntLteq
(assert
  (!
    (forall ((smt__x1 Int) (smt__x2 Int))
      (!
        (=
          (smt__TLA__IntLteq (smt__TLA__Cast_Int smt__x1)
            (smt__TLA__Cast_Int smt__x2)) (<= smt__x1 smt__x2))
        :pattern ((smt__TLA__IntLteq (smt__TLA__Cast_Int smt__x1)
                    (smt__TLA__Cast_Int smt__x2))))) :named |Typing TIntLteq|))

;; Axiom: ExtTrigEqDef Idv
(assert
  (!
    (forall ((smt__x Idv) (smt__y Idv))
      (! (= (smt__TLA__TrigEq_Idv smt__x smt__y) (= smt__x smt__y))
        :pattern ((smt__TLA__TrigEq_Idv smt__x smt__y))))
    :named |ExtTrigEqDef Idv|))

; hidden fact

; hidden fact

; omitted declaration of 'CONSTANT_EnabledWrapper_' (second-order)

; omitted declaration of 'CONSTANT_CdotWrapper_' (second-order)

(declare-fun smt__CONSTANT_N_ () Idv)

; hidden fact

(declare-fun smt__CONSTANT_P_ () Idv)

(declare-fun smt__VARIABLE_num_ () Idv)

(declare-fun smt__VARIABLE_num__prime () Idv)

(declare-fun smt__VARIABLE_flag_ () Idv)

(declare-fun smt__VARIABLE_flag__prime () Idv)

(declare-fun smt__VARIABLE_pc_ () Idv)

(declare-fun smt__VARIABLE_pc__prime () Idv)

(declare-fun smt__VARIABLE_unread_ () Idv)

(declare-fun smt__VARIABLE_unread__prime () Idv)

(declare-fun smt__VARIABLE_max_ () Idv)

(declare-fun smt__VARIABLE_max__prime () Idv)

(declare-fun smt__VARIABLE_nxt_ () Idv)

(declare-fun smt__VARIABLE_nxt__prime () Idv)

(declare-fun smt__STATE_Init_ () Idv)

(declare-fun smt__ACTION_p1_ (Idv) Idv)

(declare-fun smt__ACTION_p2_ (Idv) Idv)

(declare-fun smt__ACTION_p3_ (Idv) Idv)

(declare-fun smt__ACTION_p4_ (Idv) Idv)

(declare-fun smt__ACTION_p5_ (Idv) Idv)

(declare-fun smt__ACTION_p6_ (Idv) Idv)

(declare-fun smt__ACTION_cs_ (Idv) Idv)

(declare-fun smt__ACTION_p7_ (Idv) Idv)

(declare-fun smt__ACTION_p_ (Idv) Idv)

(declare-fun smt__ACTION_Next_ () Idv)

(declare-fun smt__TEMPORAL_Spec_ () Idv)

(declare-fun smt__STATE_MutualExclusion_ () Idv)

; hidden fact

; hidden fact

; hidden fact

(assert
  (and
    (and
      (smt__TLA__Mem smt__VARIABLE_num_
        (smt__TLA__FunSet smt__CONSTANT_P_ smt__TLA__NatSet))
      (smt__TLA__Mem smt__VARIABLE_flag_
        (smt__TLA__FunSet smt__CONSTANT_P_ smt__TLA__BoolSet))
      (smt__TLA__Mem smt__VARIABLE_unread_
        (smt__TLA__FunSet smt__CONSTANT_P_
          (smt__TLA__Subset smt__CONSTANT_P_)))
      (forall ((smt__CONSTANT_i_ Idv))
        (=> (smt__TLA__Mem smt__CONSTANT_i_ smt__CONSTANT_P_)
          (=>
            (smt__TLA__Mem
              (smt__TLA__FunApp smt__VARIABLE_pc_ smt__CONSTANT_i_)
              (smt__TLA__SetEnum_3 smt__TLA__StrLit_p2 smt__TLA__StrLit_p5
                smt__TLA__StrLit_p6))
            (not
              (smt__TLA__Mem smt__CONSTANT_i_
                (smt__TLA__FunApp smt__VARIABLE_unread_ smt__CONSTANT_i_))))))
      (smt__TLA__Mem smt__VARIABLE_max_
        (smt__TLA__FunSet smt__CONSTANT_P_ smt__TLA__NatSet))
      (smt__TLA__Mem smt__VARIABLE_nxt_
        (smt__TLA__FunSet smt__CONSTANT_P_ smt__CONSTANT_P_))
      (forall ((smt__CONSTANT_i_ Idv))
        (=> (smt__TLA__Mem smt__CONSTANT_i_ smt__CONSTANT_P_)
          (=>
            (smt__TLA__TrigEq_Idv
              (smt__TLA__FunApp smt__VARIABLE_pc_ smt__CONSTANT_i_)
              smt__TLA__StrLit_p6)
            (not
              (smt__TLA__TrigEq_Idv
                (smt__TLA__FunApp smt__VARIABLE_nxt_ smt__CONSTANT_i_)
                smt__CONSTANT_i_)))))
      (smt__TLA__Mem smt__VARIABLE_pc_
        (smt__TLA__FunSet smt__CONSTANT_P_
          (smt__TLA__SetEnum_8 smt__TLA__StrLit_p1 smt__TLA__StrLit_p2
            smt__TLA__StrLit_p3 smt__TLA__StrLit_p4 smt__TLA__StrLit_p5
            smt__TLA__StrLit_p6 smt__TLA__StrLit_cs smt__TLA__StrLit_p7))))
    (forall ((smt__CONSTANT_i_ Idv))
      (=> (smt__TLA__Mem smt__CONSTANT_i_ smt__CONSTANT_P_)
        (and
          (=
            (smt__TLA__TrigEq_Idv
              (smt__TLA__FunApp smt__VARIABLE_num_ smt__CONSTANT_i_)
              (smt__TLA__Cast_Int 0))
            (smt__TLA__Mem
              (smt__TLA__FunApp smt__VARIABLE_pc_ smt__CONSTANT_i_)
              (smt__TLA__SetEnum_3 smt__TLA__StrLit_p1 smt__TLA__StrLit_p2
                smt__TLA__StrLit_p3)))
          (=
            (= (smt__TLA__FunApp smt__VARIABLE_flag_ smt__CONSTANT_i_)
              smt__TLA__Tt_Idv)
            (smt__TLA__Mem
              (smt__TLA__FunApp smt__VARIABLE_pc_ smt__CONSTANT_i_)
              (smt__TLA__SetEnum_3 smt__TLA__StrLit_p2 smt__TLA__StrLit_p3
                smt__TLA__StrLit_p4)))
          (=>
            (smt__TLA__Mem
              (smt__TLA__FunApp smt__VARIABLE_pc_ smt__CONSTANT_i_)
              (smt__TLA__SetEnum_2 smt__TLA__StrLit_p5 smt__TLA__StrLit_p6))
            (forall ((smt__CONSTANT_j_ Idv))
              (=>
                (smt__TLA__Mem smt__CONSTANT_j_
                  (smt__TLA__SetMinus
                    (smt__TLA__SetMinus smt__CONSTANT_P_
                      (smt__TLA__FunApp smt__VARIABLE_unread_
                        smt__CONSTANT_i_))
                    (smt__TLA__SetEnum_1 smt__CONSTANT_i_)))
                (and
                  (and
                    (smt__TLA__IntLteq (smt__TLA__Cast_Int 0)
                      (smt__TLA__FunApp smt__VARIABLE_num_ smt__CONSTANT_i_))
                    (not
                      (smt__TLA__TrigEq_Idv (smt__TLA__Cast_Int 0)
                        (smt__TLA__FunApp smt__VARIABLE_num_ smt__CONSTANT_i_))))
                  (or
                    (smt__TLA__TrigEq_Idv
                      (smt__TLA__FunApp smt__VARIABLE_pc_ smt__CONSTANT_j_)
                      smt__TLA__StrLit_p1)
                    (and
                      (smt__TLA__TrigEq_Idv
                        (smt__TLA__FunApp smt__VARIABLE_pc_ smt__CONSTANT_j_)
                        smt__TLA__StrLit_p2)
                      (or
                        (smt__TLA__Mem smt__CONSTANT_i_
                          (smt__TLA__FunApp smt__VARIABLE_unread_
                            smt__CONSTANT_j_))
                        (smt__TLA__IntLteq
                          (smt__TLA__FunApp smt__VARIABLE_num_
                            smt__CONSTANT_i_)
                          (smt__TLA__FunApp smt__VARIABLE_max_
                            smt__CONSTANT_j_))))
                    (and
                      (smt__TLA__TrigEq_Idv
                        (smt__TLA__FunApp smt__VARIABLE_pc_ smt__CONSTANT_j_)
                        smt__TLA__StrLit_p3)
                      (smt__TLA__IntLteq
                        (smt__TLA__FunApp smt__VARIABLE_num_ smt__CONSTANT_i_)
                        (smt__TLA__FunApp smt__VARIABLE_max_ smt__CONSTANT_j_)))
                    (and
                      (smt__TLA__Mem
                        (smt__TLA__FunApp smt__VARIABLE_pc_ smt__CONSTANT_j_)
                        (smt__TLA__SetEnum_3 smt__TLA__StrLit_p4
                          smt__TLA__StrLit_p5 smt__TLA__StrLit_p6))
                      (or
                        (and
                          (smt__TLA__IntLteq
                            (smt__TLA__FunApp smt__VARIABLE_num_
                              smt__CONSTANT_i_)
                            (smt__TLA__FunApp smt__VARIABLE_num_
                              smt__CONSTANT_j_))
                          (not
                            (smt__TLA__TrigEq_Idv
                              (smt__TLA__FunApp smt__VARIABLE_num_
                                smt__CONSTANT_i_)
                              (smt__TLA__FunApp smt__VARIABLE_num_
                                smt__CONSTANT_j_))))
                        (and
                          (smt__TLA__TrigEq_Idv
                            (smt__TLA__FunApp smt__VARIABLE_num_
                              smt__CONSTANT_j_)
                            (smt__TLA__FunApp smt__VARIABLE_num_
                              smt__CONSTANT_i_))
                          (smt__TLA__IntLteq smt__CONSTANT_i_
                            smt__CONSTANT_j_)))
                      (=>
                        (smt__TLA__Mem
                          (smt__TLA__FunApp smt__VARIABLE_pc_
                            smt__CONSTANT_j_)
                          (smt__TLA__SetEnum_2 smt__TLA__StrLit_p5
                            smt__TLA__StrLit_p6))
                        (smt__TLA__Mem smt__CONSTANT_i_
                          (smt__TLA__FunApp smt__VARIABLE_unread_
                            smt__CONSTANT_j_)))))))))
          (=>
            (and
              (smt__TLA__TrigEq_Idv
                (smt__TLA__FunApp smt__VARIABLE_pc_ smt__CONSTANT_i_)
                smt__TLA__StrLit_p6)
              (or
                (and
                  (smt__TLA__TrigEq_Idv
                    (smt__TLA__FunApp smt__VARIABLE_pc_
                      (smt__TLA__FunApp smt__VARIABLE_nxt_ smt__CONSTANT_i_))
                    smt__TLA__StrLit_p2)
                  (not
                    (smt__TLA__Mem smt__CONSTANT_i_
                      (smt__TLA__FunApp smt__VARIABLE_unread_
                        (smt__TLA__FunApp smt__VARIABLE_nxt_ smt__CONSTANT_i_)))))
                (smt__TLA__TrigEq_Idv
                  (smt__TLA__FunApp smt__VARIABLE_pc_
                    (smt__TLA__FunApp smt__VARIABLE_nxt_ smt__CONSTANT_i_))
                  smt__TLA__StrLit_p3)))
            (smt__TLA__IntLteq
              (smt__TLA__FunApp smt__VARIABLE_num_ smt__CONSTANT_i_)
              (smt__TLA__FunApp smt__VARIABLE_max_
                (smt__TLA__FunApp smt__VARIABLE_nxt_ smt__CONSTANT_i_))))
          (=>
            (smt__TLA__Mem
              (smt__TLA__FunApp smt__VARIABLE_pc_ smt__CONSTANT_i_)
              (smt__TLA__SetEnum_2 smt__TLA__StrLit_cs smt__TLA__StrLit_p7))
            (forall ((smt__CONSTANT_j_ Idv))
              (=>
                (smt__TLA__Mem smt__CONSTANT_j_
                  (smt__TLA__SetMinus smt__CONSTANT_P_
                    (smt__TLA__SetEnum_1 smt__CONSTANT_i_)))
                (and
                  (and
                    (smt__TLA__IntLteq (smt__TLA__Cast_Int 0)
                      (smt__TLA__FunApp smt__VARIABLE_num_ smt__CONSTANT_i_))
                    (not
                      (smt__TLA__TrigEq_Idv (smt__TLA__Cast_Int 0)
                        (smt__TLA__FunApp smt__VARIABLE_num_ smt__CONSTANT_i_))))
                  (or
                    (smt__TLA__TrigEq_Idv
                      (smt__TLA__FunApp smt__VARIABLE_pc_ smt__CONSTANT_j_)
                      smt__TLA__StrLit_p1)
                    (and
                      (smt__TLA__TrigEq_Idv
                        (smt__TLA__FunApp smt__VARIABLE_pc_ smt__CONSTANT_j_)
                        smt__TLA__StrLit_p2)
                      (or
                        (smt__TLA__Mem smt__CONSTANT_i_
                          (smt__TLA__FunApp smt__VARIABLE_unread_
                            smt__CONSTANT_j_))
                        (smt__TLA__IntLteq
                          (smt__TLA__FunApp smt__VARIABLE_num_
                            smt__CONSTANT_i_)
                          (smt__TLA__FunApp smt__VARIABLE_max_
                            smt__CONSTANT_j_))))
                    (and
                      (smt__TLA__TrigEq_Idv
                        (smt__TLA__FunApp smt__VARIABLE_pc_ smt__CONSTANT_j_)
                        smt__TLA__StrLit_p3)
                      (smt__TLA__IntLteq
                        (smt__TLA__FunApp smt__VARIABLE_num_ smt__CONSTANT_i_)
                        (smt__TLA__FunApp smt__VARIABLE_max_ smt__CONSTANT_j_)))
                    (and
                      (smt__TLA__Mem
                        (smt__TLA__FunApp smt__VARIABLE_pc_ smt__CONSTANT_j_)
                        (smt__TLA__SetEnum_3 smt__TLA__StrLit_p4
                          smt__TLA__StrLit_p5 smt__TLA__StrLit_p6))
                      (or
                        (and
                          (smt__TLA__IntLteq
                            (smt__TLA__FunApp smt__VARIABLE_num_
                              smt__CONSTANT_i_)
                            (smt__TLA__FunApp smt__VARIABLE_num_
                              smt__CONSTANT_j_))
                          (not
                            (smt__TLA__TrigEq_Idv
                              (smt__TLA__FunApp smt__VARIABLE_num_
                                smt__CONSTANT_i_)
                              (smt__TLA__FunApp smt__VARIABLE_num_
                                smt__CONSTANT_j_))))
                        (and
                          (smt__TLA__TrigEq_Idv
                            (smt__TLA__FunApp smt__VARIABLE_num_
                              smt__CONSTANT_j_)
                            (smt__TLA__FunApp smt__VARIABLE_num_
                              smt__CONSTANT_i_))
                          (smt__TLA__IntLteq smt__CONSTANT_i_
                            smt__CONSTANT_j_)))
                      (=>
                        (smt__TLA__Mem
                          (smt__TLA__FunApp smt__VARIABLE_pc_
                            smt__CONSTANT_j_)
                          (smt__TLA__SetEnum_2 smt__TLA__StrLit_p5
                            smt__TLA__StrLit_p6))
                        (smt__TLA__Mem smt__CONSTANT_i_
                          (smt__TLA__FunApp smt__VARIABLE_unread_
                            smt__CONSTANT_j_))))))))))))))

(assert
  (or (= smt__ACTION_Next_ smt__TLA__Tt_Idv)
    (and (smt__TLA__TrigEq_Idv smt__VARIABLE_num__prime smt__VARIABLE_num_)
      (smt__TLA__TrigEq_Idv smt__VARIABLE_flag__prime smt__VARIABLE_flag_)
      (smt__TLA__TrigEq_Idv smt__VARIABLE_pc__prime smt__VARIABLE_pc_)
      (smt__TLA__TrigEq_Idv smt__VARIABLE_unread__prime smt__VARIABLE_unread_)
      (smt__TLA__TrigEq_Idv smt__VARIABLE_max__prime smt__VARIABLE_max_)
      (smt__TLA__TrigEq_Idv smt__VARIABLE_nxt__prime smt__VARIABLE_nxt_))))

; hidden fact

; hidden fact

; hidden fact

; hidden fact

; hidden fact

; hidden fact

; hidden fact

; hidden fact

; hidden fact

; hidden fact

; hidden fact

; hidden fact

; hidden fact

(assert (smt__TLA__TrigEq_Idv smt__VARIABLE_num__prime smt__VARIABLE_num_))

(assert (smt__TLA__TrigEq_Idv smt__VARIABLE_flag__prime smt__VARIABLE_flag_))

(assert (smt__TLA__TrigEq_Idv smt__VARIABLE_pc__prime smt__VARIABLE_pc_))

(assert
  (smt__TLA__TrigEq_Idv smt__VARIABLE_unread__prime smt__VARIABLE_unread_))

(assert (smt__TLA__TrigEq_Idv smt__VARIABLE_max__prime smt__VARIABLE_max_))

(assert (smt__TLA__TrigEq_Idv smt__VARIABLE_nxt__prime smt__VARIABLE_nxt_))

;; Goal
(assert
  (!
    (not
      (and
        (and
          (smt__TLA__Mem smt__VARIABLE_num__prime
            (smt__TLA__FunSet smt__CONSTANT_P_ smt__TLA__NatSet))
          (smt__TLA__Mem smt__VARIABLE_flag__prime
            (smt__TLA__FunSet smt__CONSTANT_P_ smt__TLA__BoolSet))
          (smt__TLA__Mem smt__VARIABLE_unread__prime
            (smt__TLA__FunSet smt__CONSTANT_P_
              (smt__TLA__Subset smt__CONSTANT_P_)))
          (forall ((smt__CONSTANT_i_ Idv))
            (=> (smt__TLA__Mem smt__CONSTANT_i_ smt__CONSTANT_P_)
              (=>
                (smt__TLA__Mem
                  (smt__TLA__FunApp smt__VARIABLE_pc__prime smt__CONSTANT_i_)
                  (smt__TLA__SetEnum_3 smt__TLA__StrLit_p2
                    smt__TLA__StrLit_p5 smt__TLA__StrLit_p6))
                (not
                  (smt__TLA__Mem smt__CONSTANT_i_
                    (smt__TLA__FunApp smt__VARIABLE_unread__prime
                      smt__CONSTANT_i_))))))
          (smt__TLA__Mem smt__VARIABLE_max__prime
            (smt__TLA__FunSet smt__CONSTANT_P_ smt__TLA__NatSet))
          (smt__TLA__Mem smt__VARIABLE_nxt__prime
            (smt__TLA__FunSet smt__CONSTANT_P_ smt__CONSTANT_P_))
          (forall ((smt__CONSTANT_i_ Idv))
            (=> (smt__TLA__Mem smt__CONSTANT_i_ smt__CONSTANT_P_)
              (=>
                (smt__TLA__TrigEq_Idv
                  (smt__TLA__FunApp smt__VARIABLE_pc__prime smt__CONSTANT_i_)
                  smt__TLA__StrLit_p6)
                (not
                  (smt__TLA__TrigEq_Idv
                    (smt__TLA__FunApp smt__VARIABLE_nxt__prime
                      smt__CONSTANT_i_) smt__CONSTANT_i_)))))
          (smt__TLA__Mem smt__VARIABLE_pc__prime
            (smt__TLA__FunSet smt__CONSTANT_P_
              (smt__TLA__SetEnum_8 smt__TLA__StrLit_p1 smt__TLA__StrLit_p2
                smt__TLA__StrLit_p3 smt__TLA__StrLit_p4 smt__TLA__StrLit_p5
                smt__TLA__StrLit_p6 smt__TLA__StrLit_cs smt__TLA__StrLit_p7))))
        (forall ((smt__CONSTANT_i_ Idv))
          (=> (smt__TLA__Mem smt__CONSTANT_i_ smt__CONSTANT_P_)
            (and
              (=
                (smt__TLA__TrigEq_Idv
                  (smt__TLA__FunApp smt__VARIABLE_num__prime smt__CONSTANT_i_)
                  (smt__TLA__Cast_Int 0))
                (smt__TLA__Mem
                  (smt__TLA__FunApp smt__VARIABLE_pc__prime smt__CONSTANT_i_)
                  (smt__TLA__SetEnum_3 smt__TLA__StrLit_p1
                    smt__TLA__StrLit_p2 smt__TLA__StrLit_p3)))
              (=
                (=
                  (smt__TLA__FunApp smt__VARIABLE_flag__prime
                    smt__CONSTANT_i_) smt__TLA__Tt_Idv)
                (smt__TLA__Mem
                  (smt__TLA__FunApp smt__VARIABLE_pc__prime smt__CONSTANT_i_)
                  (smt__TLA__SetEnum_3 smt__TLA__StrLit_p2
                    smt__TLA__StrLit_p3 smt__TLA__StrLit_p4)))
              (=>
                (smt__TLA__Mem
                  (smt__TLA__FunApp smt__VARIABLE_pc__prime smt__CONSTANT_i_)
                  (smt__TLA__SetEnum_2 smt__TLA__StrLit_p5
                    smt__TLA__StrLit_p6))
                (forall ((smt__CONSTANT_j_ Idv))
                  (=>
                    (smt__TLA__Mem smt__CONSTANT_j_
                      (smt__TLA__SetMinus
                        (smt__TLA__SetMinus smt__CONSTANT_P_
                          (smt__TLA__FunApp smt__VARIABLE_unread__prime
                            smt__CONSTANT_i_))
                        (smt__TLA__SetEnum_1 smt__CONSTANT_i_)))
                    (and
                      (and
                        (smt__TLA__IntLteq (smt__TLA__Cast_Int 0)
                          (smt__TLA__FunApp smt__VARIABLE_num__prime
                            smt__CONSTANT_i_))
                        (not
                          (smt__TLA__TrigEq_Idv (smt__TLA__Cast_Int 0)
                            (smt__TLA__FunApp smt__VARIABLE_num__prime
                              smt__CONSTANT_i_))))
                      (or
                        (smt__TLA__TrigEq_Idv
                          (smt__TLA__FunApp smt__VARIABLE_pc__prime
                            smt__CONSTANT_j_) smt__TLA__StrLit_p1)
                        (and
                          (smt__TLA__TrigEq_Idv
                            (smt__TLA__FunApp smt__VARIABLE_pc__prime
                              smt__CONSTANT_j_) smt__TLA__StrLit_p2)
                          (or
                            (smt__TLA__Mem smt__CONSTANT_i_
                              (smt__TLA__FunApp smt__VARIABLE_unread__prime
                                smt__CONSTANT_j_))
                            (smt__TLA__IntLteq
                              (smt__TLA__FunApp smt__VARIABLE_num__prime
                                smt__CONSTANT_i_)
                              (smt__TLA__FunApp smt__VARIABLE_max__prime
                                smt__CONSTANT_j_))))
                        (and
                          (smt__TLA__TrigEq_Idv
                            (smt__TLA__FunApp smt__VARIABLE_pc__prime
                              smt__CONSTANT_j_) smt__TLA__StrLit_p3)
                          (smt__TLA__IntLteq
                            (smt__TLA__FunApp smt__VARIABLE_num__prime
                              smt__CONSTANT_i_)
                            (smt__TLA__FunApp smt__VARIABLE_max__prime
                              smt__CONSTANT_j_)))
                        (and
                          (smt__TLA__Mem
                            (smt__TLA__FunApp smt__VARIABLE_pc__prime
                              smt__CONSTANT_j_)
                            (smt__TLA__SetEnum_3 smt__TLA__StrLit_p4
                              smt__TLA__StrLit_p5 smt__TLA__StrLit_p6))
                          (or
                            (and
                              (smt__TLA__IntLteq
                                (smt__TLA__FunApp smt__VARIABLE_num__prime
                                  smt__CONSTANT_i_)
                                (smt__TLA__FunApp smt__VARIABLE_num__prime
                                  smt__CONSTANT_j_))
                              (not
                                (smt__TLA__TrigEq_Idv
                                  (smt__TLA__FunApp smt__VARIABLE_num__prime
                                    smt__CONSTANT_i_)
                                  (smt__TLA__FunApp smt__VARIABLE_num__prime
                                    smt__CONSTANT_j_))))
                            (and
                              (smt__TLA__TrigEq_Idv
                                (smt__TLA__FunApp smt__VARIABLE_num__prime
                                  smt__CONSTANT_j_)
                                (smt__TLA__FunApp smt__VARIABLE_num__prime
                                  smt__CONSTANT_i_))
                              (smt__TLA__IntLteq smt__CONSTANT_i_
                                smt__CONSTANT_j_)))
                          (=>
                            (smt__TLA__Mem
                              (smt__TLA__FunApp smt__VARIABLE_pc__prime
                                smt__CONSTANT_j_)
                              (smt__TLA__SetEnum_2 smt__TLA__StrLit_p5
                                smt__TLA__StrLit_p6))
                            (smt__TLA__Mem smt__CONSTANT_i_
                              (smt__TLA__FunApp smt__VARIABLE_unread__prime
                                smt__CONSTANT_j_)))))))))
              (=>
                (and
                  (smt__TLA__TrigEq_Idv
                    (smt__TLA__FunApp smt__VARIABLE_pc__prime
                      smt__CONSTANT_i_) smt__TLA__StrLit_p6)
                  (or
                    (and
                      (smt__TLA__TrigEq_Idv
                        (smt__TLA__FunApp smt__VARIABLE_pc__prime
                          (smt__TLA__FunApp smt__VARIABLE_nxt__prime
                            smt__CONSTANT_i_)) smt__TLA__StrLit_p2)
                      (not
                        (smt__TLA__Mem smt__CONSTANT_i_
                          (smt__TLA__FunApp smt__VARIABLE_unread__prime
                            (smt__TLA__FunApp smt__VARIABLE_nxt__prime
                              smt__CONSTANT_i_)))))
                    (smt__TLA__TrigEq_Idv
                      (smt__TLA__FunApp smt__VARIABLE_pc__prime
                        (smt__TLA__FunApp smt__VARIABLE_nxt__prime
                          smt__CONSTANT_i_)) smt__TLA__StrLit_p3)))
                (smt__TLA__IntLteq
                  (smt__TLA__FunApp smt__VARIABLE_num__prime smt__CONSTANT_i_)
                  (smt__TLA__FunApp smt__VARIABLE_max__prime
                    (smt__TLA__FunApp smt__VARIABLE_nxt__prime
                      smt__CONSTANT_i_))))
              (=>
                (smt__TLA__Mem
                  (smt__TLA__FunApp smt__VARIABLE_pc__prime smt__CONSTANT_i_)
                  (smt__TLA__SetEnum_2 smt__TLA__StrLit_cs
                    smt__TLA__StrLit_p7))
                (forall ((smt__CONSTANT_j_ Idv))
                  (=>
                    (smt__TLA__Mem smt__CONSTANT_j_
                      (smt__TLA__SetMinus smt__CONSTANT_P_
                        (smt__TLA__SetEnum_1 smt__CONSTANT_i_)))
                    (and
                      (and
                        (smt__TLA__IntLteq (smt__TLA__Cast_Int 0)
                          (smt__TLA__FunApp smt__VARIABLE_num__prime
                            smt__CONSTANT_i_))
                        (not
                          (smt__TLA__TrigEq_Idv (smt__TLA__Cast_Int 0)
                            (smt__TLA__FunApp smt__VARIABLE_num__prime
                              smt__CONSTANT_i_))))
                      (or
                        (smt__TLA__TrigEq_Idv
                          (smt__TLA__FunApp smt__VARIABLE_pc__prime
                            smt__CONSTANT_j_) smt__TLA__StrLit_p1)
                        (and
                          (smt__TLA__TrigEq_Idv
                            (smt__TLA__FunApp smt__VARIABLE_pc__prime
                              smt__CONSTANT_j_) smt__TLA__StrLit_p2)
                          (or
                            (smt__TLA__Mem smt__CONSTANT_i_
                              (smt__TLA__FunApp smt__VARIABLE_unread__prime
                                smt__CONSTANT_j_))
                            (smt__TLA__IntLteq
                              (smt__TLA__FunApp smt__VARIABLE_num__prime
                                smt__CONSTANT_i_)
                              (smt__TLA__FunApp smt__VARIABLE_max__prime
                                smt__CONSTANT_j_))))
                        (and
                          (smt__TLA__TrigEq_Idv
                            (smt__TLA__FunApp smt__VARIABLE_pc__prime
                              smt__CONSTANT_j_) smt__TLA__StrLit_p3)
                          (smt__TLA__IntLteq
                            (smt__TLA__FunApp smt__VARIABLE_num__prime
                              smt__CONSTANT_i_)
                            (smt__TLA__FunApp smt__VARIABLE_max__prime
                              smt__CONSTANT_j_)))
                        (and
                          (smt__TLA__Mem
                            (smt__TLA__FunApp smt__VARIABLE_pc__prime
                              smt__CONSTANT_j_)
                            (smt__TLA__SetEnum_3 smt__TLA__StrLit_p4
                              smt__TLA__StrLit_p5 smt__TLA__StrLit_p6))
                          (or
                            (and
                              (smt__TLA__IntLteq
                                (smt__TLA__FunApp smt__VARIABLE_num__prime
                                  smt__CONSTANT_i_)
                                (smt__TLA__FunApp smt__VARIABLE_num__prime
                                  smt__CONSTANT_j_))
                              (not
                                (smt__TLA__TrigEq_Idv
                                  (smt__TLA__FunApp smt__VARIABLE_num__prime
                                    smt__CONSTANT_i_)
                                  (smt__TLA__FunApp smt__VARIABLE_num__prime
                                    smt__CONSTANT_j_))))
                            (and
                              (smt__TLA__TrigEq_Idv
                                (smt__TLA__FunApp smt__VARIABLE_num__prime
                                  smt__CONSTANT_j_)
                                (smt__TLA__FunApp smt__VARIABLE_num__prime
                                  smt__CONSTANT_i_))
                              (smt__TLA__IntLteq smt__CONSTANT_i_
                                smt__CONSTANT_j_)))
                          (=>
                            (smt__TLA__Mem
                              (smt__TLA__FunApp smt__VARIABLE_pc__prime
                                smt__CONSTANT_j_)
                              (smt__TLA__SetEnum_2 smt__TLA__StrLit_p5
                                smt__TLA__StrLit_p6))
                            (smt__TLA__Mem smt__CONSTANT_i_
                              (smt__TLA__FunApp smt__VARIABLE_unread__prime
                                smt__CONSTANT_j_)))))))))))))) :named |Goal|))

(check-sat)
(exit)
