;; Proof obligation:
;;	ASSUME NEW CONSTANT CONSTANT_N_,
;;	       NEW VARIABLE VARIABLE_active_,
;;	       NEW VARIABLE VARIABLE_color_,
;;	       NEW VARIABLE VARIABLE_tpos_,
;;	       NEW VARIABLE VARIABLE_tcolor_,
;;	       ASSUME VARIABLE_tpos_ = 0 ,
;;	              VARIABLE_tcolor_ = "white" ,
;;	              VARIABLE_color_[0] = "white" ,
;;	              ~VARIABLE_active_[0] ,
;;	              STATE_Inv_ 
;;	       PROVE  \A CONSTANT_i_ \in CONSTANT_Nodes_ :
;;	                 ~VARIABLE_active_[CONSTANT_i_] 
;;	PROVE  STATE_Inv_
;;	       => ((/\ VARIABLE_tpos_ = 0 /\ VARIABLE_tcolor_ = "white"
;;	            /\ VARIABLE_color_[0] = "white" /\ ~VARIABLE_active_[0])
;;	           => (\A CONSTANT_i_ \in CONSTANT_Nodes_ :
;;	                  ~VARIABLE_active_[CONSTANT_i_]))
;; TLA+ Proof Manager 1.5.0
;; Proof obligation #34
;; Generated from file "./tla_specs/Original/TLAPS_Examples/EWD840.tla", line 220, characters 3-4

(set-logic UFLIA)

;; Sorts

(declare-sort Idv 0)

;; Hypotheses

(declare-fun smt__TLA__Cast_Int (Int) Idv)

(declare-fun smt__TLA__FunApp (Idv Idv) Idv)

(declare-fun smt__TLA__FunDom (Idv) Idv)

; omitted declaration of 'TLA__FunFcn' (second-order)

(declare-fun smt__TLA__FunIsafcn (Idv) Bool)

(declare-fun smt__TLA__IntSet () Idv)

(declare-fun smt__TLA__Mem (Idv Idv) Bool)

(declare-fun smt__TLA__Proj_Int (Idv) Int)

(declare-fun smt__TLA__SetExtTrigger (Idv Idv) Bool)

(declare-fun smt__TLA__StrLit_white () Idv)

(declare-fun smt__TLA__StrSet () Idv)

(declare-fun smt__TLA__TrigEq_Idv (Idv Idv) Bool)

(declare-fun smt__TLA__Tt_Idv () Idv)

;; Axiom: SetExt
(assert
  (!
    (forall ((smt__x Idv) (smt__y Idv))
      (!
        (=>
          (forall ((smt__z Idv))
            (= (smt__TLA__Mem smt__z smt__x) (smt__TLA__Mem smt__z smt__y)))
          (= smt__x smt__y))
        :pattern ((smt__TLA__SetExtTrigger smt__x smt__y)))) :named |SetExt|))

;; Axiom: FunExt
(assert
  (!
    (forall ((smt__f Idv) (smt__g Idv))
      (!
        (=>
          (and (smt__TLA__FunIsafcn smt__f) (smt__TLA__FunIsafcn smt__g)
            (= (smt__TLA__FunDom smt__f) (smt__TLA__FunDom smt__g))
            (forall ((smt__x Idv))
              (=> (smt__TLA__Mem smt__x (smt__TLA__FunDom smt__f))
                (= (smt__TLA__FunApp smt__f smt__x)
                  (smt__TLA__FunApp smt__g smt__x))))) (= smt__f smt__g))
        :pattern ((smt__TLA__FunIsafcn smt__f) (smt__TLA__FunIsafcn smt__g))))
    :named |FunExt|))

; omitted fact (second-order)

; omitted fact (second-order)

; omitted fact (second-order)

;; Axiom: StrLitIsstr white
(assert
  (! (smt__TLA__Mem smt__TLA__StrLit_white smt__TLA__StrSet)
    :named |StrLitIsstr white|))

;; Axiom: CastInjAlt Int
(assert
  (!
    (forall ((smt__x Int))
      (! (= smt__x (smt__TLA__Proj_Int (smt__TLA__Cast_Int smt__x)))
        :pattern ((smt__TLA__Cast_Int smt__x)))) :named |CastInjAlt Int|))

;; Axiom: TypeGuardIntro Int
(assert
  (!
    (forall ((smt__z Int))
      (! (smt__TLA__Mem (smt__TLA__Cast_Int smt__z) smt__TLA__IntSet)
        :pattern ((smt__TLA__Cast_Int smt__z)))) :named |TypeGuardIntro Int|))

;; Axiom: TypeGuardElim Int
(assert
  (!
    (forall ((smt__x Idv))
      (!
        (=> (smt__TLA__Mem smt__x smt__TLA__IntSet)
          (= smt__x (smt__TLA__Cast_Int (smt__TLA__Proj_Int smt__x))))
        :pattern ((smt__TLA__Mem smt__x smt__TLA__IntSet))))
    :named |TypeGuardElim Int|))

;; Axiom: ExtTrigEqDef Idv
(assert
  (!
    (forall ((smt__x Idv) (smt__y Idv))
      (! (= (smt__TLA__TrigEq_Idv smt__x smt__y) (= smt__x smt__y))
        :pattern ((smt__TLA__TrigEq_Idv smt__x smt__y))))
    :named |ExtTrigEqDef Idv|))

; hidden fact

; hidden fact

; omitted declaration of 'CONSTANT_EnabledWrapper_' (second-order)

; omitted declaration of 'CONSTANT_CdotWrapper_' (second-order)

(declare-fun smt__CONSTANT_N_ () Idv)

; hidden fact

(declare-fun smt__VARIABLE_active_ () Idv)

(declare-fun smt__VARIABLE_active__prime () Idv)

(declare-fun smt__VARIABLE_color_ () Idv)

(declare-fun smt__VARIABLE_color__prime () Idv)

(declare-fun smt__VARIABLE_tpos_ () Idv)

(declare-fun smt__VARIABLE_tpos__prime () Idv)

(declare-fun smt__VARIABLE_tcolor_ () Idv)

(declare-fun smt__VARIABLE_tcolor__prime () Idv)

(declare-fun smt__CONSTANT_Nodes_ () Idv)

(declare-fun smt__CONSTANT_Color_ () Idv)

(declare-fun smt__STATE_TypeOK_ () Idv)

(declare-fun smt__STATE_Init_ () Idv)

(declare-fun smt__ACTION_InitiateProbe_ () Idv)

(declare-fun smt__ACTION_PassToken_ (Idv) Idv)

(declare-fun smt__ACTION_SendMsg_ (Idv) Idv)

(declare-fun smt__ACTION_Deactivate_ (Idv) Idv)

(declare-fun smt__ACTION_Controlled_ () Idv)

(declare-fun smt__ACTION_Environment_ () Idv)

(declare-fun smt__ACTION_Next_ () Idv)

(declare-fun smt__STATE_vars_ () Idv)

(declare-fun smt__TEMPORAL_Fairness_ () Idv)

(declare-fun smt__TEMPORAL_Spec_ () Idv)

(declare-fun smt__STATE_NeverBlack_ () Idv)

(declare-fun smt__TEMPORAL_NeverChangeColor_ () Idv)

(declare-fun smt__TEMPORAL_Liveness_ () Idv)

(declare-fun smt__TEMPORAL_AllNodesTerminateIfNoMessages_ () Idv)

(declare-fun smt__STATE_Inv_ () Idv)

; hidden fact

; hidden fact

; hidden fact

(assert
  (=> (smt__TLA__TrigEq_Idv smt__VARIABLE_tpos_ (smt__TLA__Cast_Int 0))
    (=> (smt__TLA__TrigEq_Idv smt__VARIABLE_tcolor_ smt__TLA__StrLit_white)
      (=>
        (smt__TLA__TrigEq_Idv
          (smt__TLA__FunApp smt__VARIABLE_color_ (smt__TLA__Cast_Int 0))
          smt__TLA__StrLit_white)
        (=>
          (not
            (=
              (smt__TLA__FunApp smt__VARIABLE_active_ (smt__TLA__Cast_Int 0))
              smt__TLA__Tt_Idv))
          (=> (= smt__STATE_Inv_ smt__TLA__Tt_Idv)
            (forall ((smt__CONSTANT_i_ Idv))
              (=> (smt__TLA__Mem smt__CONSTANT_i_ smt__CONSTANT_Nodes_)
                (not
                  (=
                    (smt__TLA__FunApp smt__VARIABLE_active_ smt__CONSTANT_i_)
                    smt__TLA__Tt_Idv))))))))))

;; Goal
(assert
  (!
    (not
      (=> (= smt__STATE_Inv_ smt__TLA__Tt_Idv)
        (=>
          (and
            (and
              (smt__TLA__TrigEq_Idv smt__VARIABLE_tpos_
                (smt__TLA__Cast_Int 0))
              (smt__TLA__TrigEq_Idv smt__VARIABLE_tcolor_
                smt__TLA__StrLit_white))
            (and
              (smt__TLA__TrigEq_Idv
                (smt__TLA__FunApp smt__VARIABLE_color_ (smt__TLA__Cast_Int 0))
                smt__TLA__StrLit_white)
              (not
                (=
                  (smt__TLA__FunApp smt__VARIABLE_active_
                    (smt__TLA__Cast_Int 0)) smt__TLA__Tt_Idv))))
          (forall ((smt__CONSTANT_i_ Idv))
            (=> (smt__TLA__Mem smt__CONSTANT_i_ smt__CONSTANT_Nodes_)
              (not
                (= (smt__TLA__FunApp smt__VARIABLE_active_ smt__CONSTANT_i_)
                  smt__TLA__Tt_Idv))))))) :named |Goal|))

(check-sat)
(exit)
