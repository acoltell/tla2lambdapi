;; Proof obligation:
;;	ASSUME NEW CONSTANT CONSTANT_N_,
;;	       NEW VARIABLE VARIABLE_active_,
;;	       NEW VARIABLE VARIABLE_color_,
;;	       NEW VARIABLE VARIABLE_tpos_,
;;	       NEW VARIABLE VARIABLE_tcolor_,
;;	       CONSTANT_N_ \in Nat \ {0} ,
;;	       /\ VARIABLE_active_ \in [0..CONSTANT_N_ - 1 -> BOOLEAN]
;;	       /\ VARIABLE_color_ \in [0..CONSTANT_N_ - 1 -> {"white", "black"}]
;;	       /\ VARIABLE_tpos_ \in 0..CONSTANT_N_ - 1
;;	       /\ VARIABLE_tcolor_ \in {"white", "black"} ,
;;	       ACTION_Next_ \/ ?h6fbaa = STATE_vars_ ,
;;	       NEW CONSTANT CONSTANT_i_ \in 0..CONSTANT_N_ - 1,
;;	       /\ VARIABLE_active_[CONSTANT_i_]
;;	       /\ \E CONSTANT_j_ \in 0..CONSTANT_N_ - 1 \ {CONSTANT_i_} :
;;	             /\ ?VARIABLE_active_#prime
;;	                = [VARIABLE_active_ EXCEPT ![CONSTANT_j_] = TRUE]
;;	             /\ ?VARIABLE_color_#prime
;;	                = [VARIABLE_color_ EXCEPT
;;	                     ![CONSTANT_i_] = IF CONSTANT_j_ > CONSTANT_i_
;;	                                        THEN "black"
;;	                                        ELSE VARIABLE_color_[CONSTANT_i_]] ,
;;	       ?VARIABLE_tpos_#prime = VARIABLE_tpos_ ,
;;	       ?VARIABLE_tcolor_#prime = VARIABLE_tcolor_ 
;;	PROVE  /\ ?VARIABLE_active_#prime \in [0..CONSTANT_N_ - 1 -> BOOLEAN]
;;	       /\ ?VARIABLE_color_#prime
;;	          \in [0..CONSTANT_N_ - 1 -> {"white", "black"}]
;;	       /\ ?VARIABLE_tpos_#prime \in 0..CONSTANT_N_ - 1
;;	       /\ ?VARIABLE_tcolor_#prime \in {"white", "black"}
;; TLA+ Proof Manager 1.5.0
;; Proof obligation #20
;; Generated from file "./tla_specs/Original/TLAPS_Examples/EWD840.tla", line 160, characters 5-6

(set-logic UFLIA)

;; Sorts

(declare-sort Idv 0)

;; Hypotheses

(declare-fun smt__TLA__Anon_h6fbaa () Idv)

(declare-fun smt__TLA__BoolSet () Idv)

(declare-fun smt__TLA__Cast_Bool (Bool) Idv)

(declare-fun smt__TLA__Cast_Int (Int) Idv)

(declare-fun smt__TLA__FunApp (Idv Idv) Idv)

(declare-fun smt__TLA__FunDom (Idv) Idv)

(declare-fun smt__TLA__FunExcept (Idv Idv Idv) Idv)

; omitted declaration of 'TLA__FunFcn' (second-order)

(declare-fun smt__TLA__FunIsafcn (Idv) Bool)

(declare-fun smt__TLA__FunSet (Idv Idv) Idv)

(declare-fun smt__TLA__IntLteq (Idv Idv) Bool)

(declare-fun smt__TLA__IntMinus (Idv Idv) Idv)

(declare-fun smt__TLA__IntRange (Idv Idv) Idv)

(declare-fun smt__TLA__IntSet () Idv)

(declare-fun smt__TLA__Mem (Idv Idv) Bool)

(declare-fun smt__TLA__NatSet () Idv)

(declare-fun smt__TLA__Proj_Int (Idv) Int)

(declare-fun smt__TLA__SetEnum_1 (Idv) Idv)

(declare-fun smt__TLA__SetEnum_2 (Idv Idv) Idv)

(declare-fun smt__TLA__SetExtTrigger (Idv Idv) Bool)

(declare-fun smt__TLA__SetMinus (Idv Idv) Idv)

(declare-fun smt__TLA__StrLit_black () Idv)

(declare-fun smt__TLA__StrLit_white () Idv)

(declare-fun smt__TLA__StrSet () Idv)

(declare-fun smt__TLA__TrigEq_Idv (Idv Idv) Bool)

(declare-fun smt__TLA__Tt_Idv () Idv)

;; Axiom: SetExt
(assert
  (!
    (forall ((smt__x Idv) (smt__y Idv))
      (!
        (=>
          (forall ((smt__z Idv))
            (= (smt__TLA__Mem smt__z smt__x) (smt__TLA__Mem smt__z smt__y)))
          (= smt__x smt__y))
        :pattern ((smt__TLA__SetExtTrigger smt__x smt__y)))) :named |SetExt|))

;; Axiom: SetMinusDef
(assert
  (!
    (forall ((smt__a Idv) (smt__b Idv) (smt__x Idv))
      (!
        (= (smt__TLA__Mem smt__x (smt__TLA__SetMinus smt__a smt__b))
          (and (smt__TLA__Mem smt__x smt__a)
            (not (smt__TLA__Mem smt__x smt__b))))
        :pattern ((smt__TLA__Mem smt__x (smt__TLA__SetMinus smt__a smt__b)))
        :pattern ((smt__TLA__Mem smt__x smt__a)
                   (smt__TLA__SetMinus smt__a smt__b))
        :pattern ((smt__TLA__Mem smt__x smt__b)
                   (smt__TLA__SetMinus smt__a smt__b)))) :named |SetMinusDef|))

;; Axiom: NatSetDef
(assert
  (!
    (forall ((smt__x Idv))
      (!
        (= (smt__TLA__Mem smt__x smt__TLA__NatSet)
          (and (smt__TLA__Mem smt__x smt__TLA__IntSet)
            (smt__TLA__IntLteq (smt__TLA__Cast_Int 0) smt__x)))
        :pattern ((smt__TLA__Mem smt__x smt__TLA__NatSet))))
    :named |NatSetDef|))

;; Axiom: IntRangeDef
(assert
  (!
    (forall ((smt__a Idv) (smt__b Idv) (smt__x Idv))
      (!
        (= (smt__TLA__Mem smt__x (smt__TLA__IntRange smt__a smt__b))
          (and (smt__TLA__Mem smt__x smt__TLA__IntSet)
            (smt__TLA__IntLteq smt__a smt__x)
            (smt__TLA__IntLteq smt__x smt__b)))
        :pattern ((smt__TLA__Mem smt__x (smt__TLA__IntRange smt__a smt__b)))))
    :named |IntRangeDef|))

;; Axiom: FunExt
(assert
  (!
    (forall ((smt__f Idv) (smt__g Idv))
      (!
        (=>
          (and (smt__TLA__FunIsafcn smt__f) (smt__TLA__FunIsafcn smt__g)
            (= (smt__TLA__FunDom smt__f) (smt__TLA__FunDom smt__g))
            (forall ((smt__x Idv))
              (=> (smt__TLA__Mem smt__x (smt__TLA__FunDom smt__f))
                (= (smt__TLA__FunApp smt__f smt__x)
                  (smt__TLA__FunApp smt__g smt__x))))) (= smt__f smt__g))
        :pattern ((smt__TLA__FunIsafcn smt__f) (smt__TLA__FunIsafcn smt__g))))
    :named |FunExt|))

; omitted fact (second-order)

;; Axiom: FunSetIntro
(assert
  (!
    (forall ((smt__a Idv) (smt__b Idv) (smt__f Idv))
      (!
        (=>
          (and (smt__TLA__FunIsafcn smt__f)
            (= (smt__TLA__FunDom smt__f) smt__a)
            (forall ((smt__x Idv))
              (=> (smt__TLA__Mem smt__x smt__a)
                (smt__TLA__Mem (smt__TLA__FunApp smt__f smt__x) smt__b))))
          (smt__TLA__Mem smt__f (smt__TLA__FunSet smt__a smt__b)))
        :pattern ((smt__TLA__Mem smt__f (smt__TLA__FunSet smt__a smt__b)))))
    :named |FunSetIntro|))

;; Axiom: FunSetElim1
(assert
  (!
    (forall ((smt__a Idv) (smt__b Idv) (smt__f Idv))
      (!
        (=> (smt__TLA__Mem smt__f (smt__TLA__FunSet smt__a smt__b))
          (and (smt__TLA__FunIsafcn smt__f)
            (= (smt__TLA__FunDom smt__f) smt__a)))
        :pattern ((smt__TLA__Mem smt__f (smt__TLA__FunSet smt__a smt__b)))))
    :named |FunSetElim1|))

;; Axiom: FunSetElim2
(assert
  (!
    (forall ((smt__a Idv) (smt__b Idv) (smt__f Idv) (smt__x Idv))
      (!
        (=>
          (and (smt__TLA__Mem smt__f (smt__TLA__FunSet smt__a smt__b))
            (smt__TLA__Mem smt__x smt__a))
          (smt__TLA__Mem (smt__TLA__FunApp smt__f smt__x) smt__b))
        :pattern ((smt__TLA__Mem smt__f (smt__TLA__FunSet smt__a smt__b))
                   (smt__TLA__Mem smt__x smt__a))
        :pattern ((smt__TLA__Mem smt__f (smt__TLA__FunSet smt__a smt__b))
                   (smt__TLA__FunApp smt__f smt__x)))) :named |FunSetElim2|))

; omitted fact (second-order)

; omitted fact (second-order)

;; Axiom: FunExceptIsafcn
(assert
  (!
    (forall ((smt__f Idv) (smt__x Idv) (smt__y Idv))
      (! (smt__TLA__FunIsafcn (smt__TLA__FunExcept smt__f smt__x smt__y))
        :pattern ((smt__TLA__FunExcept smt__f smt__x smt__y))))
    :named |FunExceptIsafcn|))

;; Axiom: FunExceptDomDef
(assert
  (!
    (forall ((smt__f Idv) (smt__x Idv) (smt__y Idv))
      (!
        (= (smt__TLA__FunDom (smt__TLA__FunExcept smt__f smt__x smt__y))
          (smt__TLA__FunDom smt__f))
        :pattern ((smt__TLA__FunExcept smt__f smt__x smt__y))))
    :named |FunExceptDomDef|))

;; Axiom: FunExceptAppDef1
(assert
  (!
    (forall ((smt__f Idv) (smt__x Idv) (smt__y Idv))
      (!
        (=> (smt__TLA__Mem smt__x (smt__TLA__FunDom smt__f))
          (=
            (smt__TLA__FunApp (smt__TLA__FunExcept smt__f smt__x smt__y)
              smt__x) smt__y))
        :pattern ((smt__TLA__FunExcept smt__f smt__x smt__y))))
    :named |FunExceptAppDef1|))

;; Axiom: FunExceptAppDef2
(assert
  (!
    (forall ((smt__f Idv) (smt__x Idv) (smt__y Idv) (smt__z Idv))
      (!
        (=> (smt__TLA__Mem smt__z (smt__TLA__FunDom smt__f))
          (and
            (=> (= smt__z smt__x)
              (=
                (smt__TLA__FunApp (smt__TLA__FunExcept smt__f smt__x smt__y)
                  smt__z) smt__y))
            (=> (distinct smt__z smt__x)
              (=
                (smt__TLA__FunApp (smt__TLA__FunExcept smt__f smt__x smt__y)
                  smt__z) (smt__TLA__FunApp smt__f smt__z)))))
        :pattern ((smt__TLA__FunApp
                    (smt__TLA__FunExcept smt__f smt__x smt__y) smt__z))
        :pattern ((smt__TLA__FunExcept smt__f smt__x smt__y)
                   (smt__TLA__FunApp smt__f smt__z))))
    :named |FunExceptAppDef2|))

;; Axiom: EnumDefIntro 1
(assert
  (!
    (forall ((smt__a1 Idv))
      (! (smt__TLA__Mem smt__a1 (smt__TLA__SetEnum_1 smt__a1))
        :pattern ((smt__TLA__SetEnum_1 smt__a1)))) :named |EnumDefIntro 1|))

;; Axiom: EnumDefIntro 2
(assert
  (!
    (forall ((smt__a1 Idv) (smt__a2 Idv))
      (!
        (and (smt__TLA__Mem smt__a1 (smt__TLA__SetEnum_2 smt__a1 smt__a2))
          (smt__TLA__Mem smt__a2 (smt__TLA__SetEnum_2 smt__a1 smt__a2)))
        :pattern ((smt__TLA__SetEnum_2 smt__a1 smt__a2))))
    :named |EnumDefIntro 2|))

;; Axiom: EnumDefElim 1
(assert
  (!
    (forall ((smt__a1 Idv) (smt__x Idv))
      (!
        (=> (smt__TLA__Mem smt__x (smt__TLA__SetEnum_1 smt__a1))
          (= smt__x smt__a1))
        :pattern ((smt__TLA__Mem smt__x (smt__TLA__SetEnum_1 smt__a1)))))
    :named |EnumDefElim 1|))

;; Axiom: EnumDefElim 2
(assert
  (!
    (forall ((smt__a1 Idv) (smt__a2 Idv) (smt__x Idv))
      (!
        (=> (smt__TLA__Mem smt__x (smt__TLA__SetEnum_2 smt__a1 smt__a2))
          (or (= smt__x smt__a1) (= smt__x smt__a2)))
        :pattern ((smt__TLA__Mem smt__x (smt__TLA__SetEnum_2 smt__a1 smt__a2)))))
    :named |EnumDefElim 2|))

;; Axiom: StrLitIsstr black
(assert
  (! (smt__TLA__Mem smt__TLA__StrLit_black smt__TLA__StrSet)
    :named |StrLitIsstr black|))

;; Axiom: StrLitIsstr white
(assert
  (! (smt__TLA__Mem smt__TLA__StrLit_white smt__TLA__StrSet)
    :named |StrLitIsstr white|))

;; Axiom: StrLitDistinct black white
(assert
  (! (distinct smt__TLA__StrLit_black smt__TLA__StrLit_white)
    :named |StrLitDistinct black white|))

;; Axiom: CastInjAlt Bool
(assert
  (!
    (and (= (smt__TLA__Cast_Bool true) smt__TLA__Tt_Idv)
      (distinct (smt__TLA__Cast_Bool false) smt__TLA__Tt_Idv))
    :named |CastInjAlt Bool|))

;; Axiom: CastInjAlt Int
(assert
  (!
    (forall ((smt__x Int))
      (! (= smt__x (smt__TLA__Proj_Int (smt__TLA__Cast_Int smt__x)))
        :pattern ((smt__TLA__Cast_Int smt__x)))) :named |CastInjAlt Int|))

;; Axiom: TypeGuardIntro Bool
(assert
  (!
    (forall ((smt__z Bool))
      (! (smt__TLA__Mem (smt__TLA__Cast_Bool smt__z) smt__TLA__BoolSet)
        :pattern ((smt__TLA__Cast_Bool smt__z))))
    :named |TypeGuardIntro Bool|))

;; Axiom: TypeGuardIntro Int
(assert
  (!
    (forall ((smt__z Int))
      (! (smt__TLA__Mem (smt__TLA__Cast_Int smt__z) smt__TLA__IntSet)
        :pattern ((smt__TLA__Cast_Int smt__z)))) :named |TypeGuardIntro Int|))

;; Axiom: TypeGuardElim Bool
(assert
  (!
    (forall ((smt__x Idv))
      (!
        (=> (smt__TLA__Mem smt__x smt__TLA__BoolSet)
          (or (= smt__x (smt__TLA__Cast_Bool true))
            (= smt__x (smt__TLA__Cast_Bool false))))
        :pattern ((smt__TLA__Mem smt__x smt__TLA__BoolSet))))
    :named |TypeGuardElim Bool|))

;; Axiom: TypeGuardElim Int
(assert
  (!
    (forall ((smt__x Idv))
      (!
        (=> (smt__TLA__Mem smt__x smt__TLA__IntSet)
          (= smt__x (smt__TLA__Cast_Int (smt__TLA__Proj_Int smt__x))))
        :pattern ((smt__TLA__Mem smt__x smt__TLA__IntSet))))
    :named |TypeGuardElim Int|))

;; Axiom: Typing TIntMinus
(assert
  (!
    (forall ((smt__x1 Int) (smt__x2 Int))
      (!
        (=
          (smt__TLA__IntMinus (smt__TLA__Cast_Int smt__x1)
            (smt__TLA__Cast_Int smt__x2))
          (smt__TLA__Cast_Int (- smt__x1 smt__x2)))
        :pattern ((smt__TLA__IntMinus (smt__TLA__Cast_Int smt__x1)
                    (smt__TLA__Cast_Int smt__x2)))))
    :named |Typing TIntMinus|))

;; Axiom: Typing TIntLteq
(assert
  (!
    (forall ((smt__x1 Int) (smt__x2 Int))
      (!
        (=
          (smt__TLA__IntLteq (smt__TLA__Cast_Int smt__x1)
            (smt__TLA__Cast_Int smt__x2)) (<= smt__x1 smt__x2))
        :pattern ((smt__TLA__IntLteq (smt__TLA__Cast_Int smt__x1)
                    (smt__TLA__Cast_Int smt__x2))))) :named |Typing TIntLteq|))

;; Axiom: ExtTrigEqDef Idv
(assert
  (!
    (forall ((smt__x Idv) (smt__y Idv))
      (! (= (smt__TLA__TrigEq_Idv smt__x smt__y) (= smt__x smt__y))
        :pattern ((smt__TLA__TrigEq_Idv smt__x smt__y))))
    :named |ExtTrigEqDef Idv|))

; hidden fact

; hidden fact

; omitted declaration of 'CONSTANT_EnabledWrapper_' (second-order)

; omitted declaration of 'CONSTANT_CdotWrapper_' (second-order)

(declare-fun smt__CONSTANT_N_ () Idv)

; hidden fact

(declare-fun smt__VARIABLE_active_ () Idv)

(declare-fun smt__VARIABLE_active__prime () Idv)

(declare-fun smt__VARIABLE_color_ () Idv)

(declare-fun smt__VARIABLE_color__prime () Idv)

(declare-fun smt__VARIABLE_tpos_ () Idv)

(declare-fun smt__VARIABLE_tpos__prime () Idv)

(declare-fun smt__VARIABLE_tcolor_ () Idv)

(declare-fun smt__VARIABLE_tcolor__prime () Idv)

(declare-fun smt__STATE_Init_ () Idv)

(declare-fun smt__ACTION_InitiateProbe_ () Idv)

(declare-fun smt__ACTION_PassToken_ (Idv) Idv)

(declare-fun smt__ACTION_Deactivate_ (Idv) Idv)

(declare-fun smt__ACTION_Controlled_ () Idv)

(declare-fun smt__ACTION_Environment_ () Idv)

(declare-fun smt__ACTION_Next_ () Idv)

(declare-fun smt__STATE_vars_ () Idv)

(declare-fun smt__TEMPORAL_Fairness_ () Idv)

(declare-fun smt__TEMPORAL_Spec_ () Idv)

(declare-fun smt__STATE_NeverBlack_ () Idv)

(declare-fun smt__TEMPORAL_NeverChangeColor_ () Idv)

(declare-fun smt__STATE_terminationDetected_ () Idv)

(declare-fun smt__STATE_TerminationDetection_ () Idv)

(declare-fun smt__TEMPORAL_Liveness_ () Idv)

(declare-fun smt__TEMPORAL_AllNodesTerminateIfNoMessages_ () Idv)

(declare-fun smt__STATE_Inv_ () Idv)

(assert
  (smt__TLA__Mem smt__CONSTANT_N_
    (smt__TLA__SetMinus smt__TLA__NatSet
      (smt__TLA__SetEnum_1 (smt__TLA__Cast_Int 0)))))

; hidden fact

; hidden fact

; hidden fact

(assert
  (and
    (smt__TLA__Mem smt__VARIABLE_active_
      (smt__TLA__FunSet
        (smt__TLA__IntRange (smt__TLA__Cast_Int 0)
          (smt__TLA__IntMinus smt__CONSTANT_N_ (smt__TLA__Cast_Int 1)))
        smt__TLA__BoolSet))
    (smt__TLA__Mem smt__VARIABLE_color_
      (smt__TLA__FunSet
        (smt__TLA__IntRange (smt__TLA__Cast_Int 0)
          (smt__TLA__IntMinus smt__CONSTANT_N_ (smt__TLA__Cast_Int 1)))
        (smt__TLA__SetEnum_2 smt__TLA__StrLit_white smt__TLA__StrLit_black)))
    (smt__TLA__Mem smt__VARIABLE_tpos_
      (smt__TLA__IntRange (smt__TLA__Cast_Int 0)
        (smt__TLA__IntMinus smt__CONSTANT_N_ (smt__TLA__Cast_Int 1))))
    (smt__TLA__Mem smt__VARIABLE_tcolor_
      (smt__TLA__SetEnum_2 smt__TLA__StrLit_white smt__TLA__StrLit_black))))

(assert
  (or (= smt__ACTION_Next_ smt__TLA__Tt_Idv)
    (smt__TLA__TrigEq_Idv smt__TLA__Anon_h6fbaa smt__STATE_vars_)))

; hidden fact

; hidden fact

; hidden fact

; hidden fact

; hidden fact

(declare-fun smt__CONSTANT_i_ () Idv)

(assert
  (smt__TLA__Mem smt__CONSTANT_i_
    (smt__TLA__IntRange (smt__TLA__Cast_Int 0)
      (smt__TLA__IntMinus smt__CONSTANT_N_ (smt__TLA__Cast_Int 1)))))

; hidden fact

; hidden fact

; hidden fact

(assert
  (and
    (= (smt__TLA__FunApp smt__VARIABLE_active_ smt__CONSTANT_i_)
      smt__TLA__Tt_Idv)
    (exists ((smt__CONSTANT_j_ Idv))
      (and
        (smt__TLA__Mem smt__CONSTANT_j_
          (smt__TLA__SetMinus
            (smt__TLA__IntRange (smt__TLA__Cast_Int 0)
              (smt__TLA__IntMinus smt__CONSTANT_N_ (smt__TLA__Cast_Int 1)))
            (smt__TLA__SetEnum_1 smt__CONSTANT_i_)))
        (and
          (smt__TLA__TrigEq_Idv smt__VARIABLE_active__prime
            (smt__TLA__FunExcept smt__VARIABLE_active_ smt__CONSTANT_j_
              (smt__TLA__Cast_Bool true)))
          (smt__TLA__TrigEq_Idv smt__VARIABLE_color__prime
            (smt__TLA__FunExcept smt__VARIABLE_color_ smt__CONSTANT_i_
              (ite
                (and (smt__TLA__IntLteq smt__CONSTANT_i_ smt__CONSTANT_j_)
                  (not
                    (smt__TLA__TrigEq_Idv smt__CONSTANT_i_ smt__CONSTANT_j_)))
                smt__TLA__StrLit_black
                (smt__TLA__FunApp smt__VARIABLE_color_ smt__CONSTANT_i_)))))))))

(assert (smt__TLA__TrigEq_Idv smt__VARIABLE_tpos__prime smt__VARIABLE_tpos_))

(assert
  (smt__TLA__TrigEq_Idv smt__VARIABLE_tcolor__prime smt__VARIABLE_tcolor_))

;; Goal
(assert
  (!
    (not
      (and
        (smt__TLA__Mem smt__VARIABLE_active__prime
          (smt__TLA__FunSet
            (smt__TLA__IntRange (smt__TLA__Cast_Int 0)
              (smt__TLA__IntMinus smt__CONSTANT_N_ (smt__TLA__Cast_Int 1)))
            smt__TLA__BoolSet))
        (smt__TLA__Mem smt__VARIABLE_color__prime
          (smt__TLA__FunSet
            (smt__TLA__IntRange (smt__TLA__Cast_Int 0)
              (smt__TLA__IntMinus smt__CONSTANT_N_ (smt__TLA__Cast_Int 1)))
            (smt__TLA__SetEnum_2 smt__TLA__StrLit_white
              smt__TLA__StrLit_black)))
        (smt__TLA__Mem smt__VARIABLE_tpos__prime
          (smt__TLA__IntRange (smt__TLA__Cast_Int 0)
            (smt__TLA__IntMinus smt__CONSTANT_N_ (smt__TLA__Cast_Int 1))))
        (smt__TLA__Mem smt__VARIABLE_tcolor__prime
          (smt__TLA__SetEnum_2 smt__TLA__StrLit_white smt__TLA__StrLit_black))))
    :named |Goal|))

(check-sat)
(exit)
