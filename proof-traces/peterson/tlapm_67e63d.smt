;; Proof obligation:
;;	ASSUME NEW VARIABLE VARIABLE_flag_,
;;	       NEW VARIABLE VARIABLE_turn_,
;;	       NEW VARIABLE VARIABLE_pc_
;;	PROVE  (/\ VARIABLE_flag_ = [CONSTANT_i_ \in {0, 1} |-> FALSE]
;;	        /\ VARIABLE_turn_ = 0
;;	        /\ VARIABLE_pc_ = [CONSTANT_self_ \in {0, 1} |-> "a0"])
;;	       => (/\ VARIABLE_pc_
;;	              \in [{0, 1} -> {"a0", "a1", "a2", "a3a", "a3b", "cs", "a4"}]
;;	           /\ VARIABLE_turn_ \in {0, 1}
;;	           /\ VARIABLE_flag_ \in [{0, 1} -> BOOLEAN])
;;	          /\ (\A CONSTANT_i_ \in {0, 1} :
;;	                 /\ VARIABLE_pc_[CONSTANT_i_]
;;	                    \in {"a2", "a3a", "a3b", "cs", "a4"}
;;	                    => VARIABLE_flag_[CONSTANT_i_]
;;	                 /\ VARIABLE_pc_[CONSTANT_i_] \in {"cs", "a4"}
;;	                    => (/\ VARIABLE_pc_[CONSTANT_Not_(CONSTANT_i_)]
;;	                           \notin {"cs", "a4"}
;;	                        /\ VARIABLE_pc_[CONSTANT_Not_(CONSTANT_i_)]
;;	                           \in {"a3a", "a3b"} => VARIABLE_turn_ = CONSTANT_i_))
;; TLA+ Proof Manager 1.5.0
;; Proof obligation #39
;; Generated from file "./tla_specs/Original/TLAPS_Examples/Peterson.tla", line 174, characters 3-4

(set-logic UFLIA)

;; Sorts

(declare-sort Idv 0)

;; Hypotheses

(declare-fun smt__TLA__BoolSet () Idv)

(declare-fun smt__TLA__Cast_Bool (Bool) Idv)

(declare-fun smt__TLA__Cast_Int (Int) Idv)

(declare-fun smt__TLA__FunApp (Idv Idv) Idv)

(declare-fun smt__TLA__FunDom (Idv) Idv)

; omitted declaration of 'TLA__FunFcn' (second-order)

(declare-fun smt__TLA__FunIsafcn (Idv) Bool)

(declare-fun smt__TLA__FunSet (Idv Idv) Idv)

(declare-fun smt__TLA__IntSet () Idv)

(declare-fun smt__TLA__Mem (Idv Idv) Bool)

(declare-fun smt__TLA__Proj_Int (Idv) Int)

(declare-fun smt__TLA__SetEnum_2 (Idv Idv) Idv)

(declare-fun smt__TLA__SetEnum_5 (Idv Idv Idv Idv Idv) Idv)

(declare-fun smt__TLA__SetEnum_7 (Idv Idv Idv Idv Idv Idv Idv) Idv)

(declare-fun smt__TLA__SetExtTrigger (Idv Idv) Bool)

(declare-fun smt__TLA__StrLit_a0 () Idv)

(declare-fun smt__TLA__StrLit_a1 () Idv)

(declare-fun smt__TLA__StrLit_a2 () Idv)

(declare-fun smt__TLA__StrLit_a3a () Idv)

(declare-fun smt__TLA__StrLit_a3b () Idv)

(declare-fun smt__TLA__StrLit_a4 () Idv)

(declare-fun smt__TLA__StrLit_cs () Idv)

(declare-fun smt__TLA__StrSet () Idv)

(declare-fun smt__TLA__TrigEq_Idv (Idv Idv) Bool)

(declare-fun smt__TLA__Tt_Idv () Idv)

;; Axiom: SetExt
(assert
  (!
    (forall ((smt__x Idv) (smt__y Idv))
      (!
        (=>
          (forall ((smt__z Idv))
            (= (smt__TLA__Mem smt__z smt__x) (smt__TLA__Mem smt__z smt__y)))
          (= smt__x smt__y))
        :pattern ((smt__TLA__SetExtTrigger smt__x smt__y)))) :named |SetExt|))

;; Axiom: FunExt
(assert
  (!
    (forall ((smt__f Idv) (smt__g Idv))
      (!
        (=>
          (and (smt__TLA__FunIsafcn smt__f) (smt__TLA__FunIsafcn smt__g)
            (= (smt__TLA__FunDom smt__f) (smt__TLA__FunDom smt__g))
            (forall ((smt__x Idv))
              (=> (smt__TLA__Mem smt__x (smt__TLA__FunDom smt__f))
                (= (smt__TLA__FunApp smt__f smt__x)
                  (smt__TLA__FunApp smt__g smt__x))))) (= smt__f smt__g))
        :pattern ((smt__TLA__FunIsafcn smt__f) (smt__TLA__FunIsafcn smt__g))))
    :named |FunExt|))

; omitted fact (second-order)

;; Axiom: FunSetIntro
(assert
  (!
    (forall ((smt__a Idv) (smt__b Idv) (smt__f Idv))
      (!
        (=>
          (and (smt__TLA__FunIsafcn smt__f)
            (= (smt__TLA__FunDom smt__f) smt__a)
            (forall ((smt__x Idv))
              (=> (smt__TLA__Mem smt__x smt__a)
                (smt__TLA__Mem (smt__TLA__FunApp smt__f smt__x) smt__b))))
          (smt__TLA__Mem smt__f (smt__TLA__FunSet smt__a smt__b)))
        :pattern ((smt__TLA__Mem smt__f (smt__TLA__FunSet smt__a smt__b)))))
    :named |FunSetIntro|))

;; Axiom: FunSetElim1
(assert
  (!
    (forall ((smt__a Idv) (smt__b Idv) (smt__f Idv))
      (!
        (=> (smt__TLA__Mem smt__f (smt__TLA__FunSet smt__a smt__b))
          (and (smt__TLA__FunIsafcn smt__f)
            (= (smt__TLA__FunDom smt__f) smt__a)))
        :pattern ((smt__TLA__Mem smt__f (smt__TLA__FunSet smt__a smt__b)))))
    :named |FunSetElim1|))

;; Axiom: FunSetElim2
(assert
  (!
    (forall ((smt__a Idv) (smt__b Idv) (smt__f Idv) (smt__x Idv))
      (!
        (=>
          (and (smt__TLA__Mem smt__f (smt__TLA__FunSet smt__a smt__b))
            (smt__TLA__Mem smt__x smt__a))
          (smt__TLA__Mem (smt__TLA__FunApp smt__f smt__x) smt__b))
        :pattern ((smt__TLA__Mem smt__f (smt__TLA__FunSet smt__a smt__b))
                   (smt__TLA__Mem smt__x smt__a))
        :pattern ((smt__TLA__Mem smt__f (smt__TLA__FunSet smt__a smt__b))
                   (smt__TLA__FunApp smt__f smt__x)))) :named |FunSetElim2|))

; omitted fact (second-order)

; omitted fact (second-order)

;; Axiom: EnumDefIntro 2
(assert
  (!
    (forall ((smt__a1 Idv) (smt__a2 Idv))
      (!
        (and (smt__TLA__Mem smt__a1 (smt__TLA__SetEnum_2 smt__a1 smt__a2))
          (smt__TLA__Mem smt__a2 (smt__TLA__SetEnum_2 smt__a1 smt__a2)))
        :pattern ((smt__TLA__SetEnum_2 smt__a1 smt__a2))))
    :named |EnumDefIntro 2|))

;; Axiom: EnumDefIntro 5
(assert
  (!
    (forall
      ((smt__a1 Idv) (smt__a2 Idv) (smt__a3 Idv) (smt__a4 Idv) (smt__a5 Idv))
      (!
        (and
          (smt__TLA__Mem smt__a1
            (smt__TLA__SetEnum_5 smt__a1 smt__a2 smt__a3 smt__a4 smt__a5))
          (smt__TLA__Mem smt__a2
            (smt__TLA__SetEnum_5 smt__a1 smt__a2 smt__a3 smt__a4 smt__a5))
          (smt__TLA__Mem smt__a3
            (smt__TLA__SetEnum_5 smt__a1 smt__a2 smt__a3 smt__a4 smt__a5))
          (smt__TLA__Mem smt__a4
            (smt__TLA__SetEnum_5 smt__a1 smt__a2 smt__a3 smt__a4 smt__a5))
          (smt__TLA__Mem smt__a5
            (smt__TLA__SetEnum_5 smt__a1 smt__a2 smt__a3 smt__a4 smt__a5)))
        :pattern ((smt__TLA__SetEnum_5 smt__a1 smt__a2 smt__a3 smt__a4
                    smt__a5)))) :named |EnumDefIntro 5|))

;; Axiom: EnumDefIntro 7
(assert
  (!
    (forall
      ((smt__a1 Idv) (smt__a2 Idv) (smt__a3 Idv) (smt__a4 Idv) (smt__a5 Idv)
        (smt__a6 Idv) (smt__a7 Idv))
      (!
        (and
          (smt__TLA__Mem smt__a1
            (smt__TLA__SetEnum_7 smt__a1 smt__a2 smt__a3 smt__a4 smt__a5
              smt__a6 smt__a7))
          (smt__TLA__Mem smt__a2
            (smt__TLA__SetEnum_7 smt__a1 smt__a2 smt__a3 smt__a4 smt__a5
              smt__a6 smt__a7))
          (smt__TLA__Mem smt__a3
            (smt__TLA__SetEnum_7 smt__a1 smt__a2 smt__a3 smt__a4 smt__a5
              smt__a6 smt__a7))
          (smt__TLA__Mem smt__a4
            (smt__TLA__SetEnum_7 smt__a1 smt__a2 smt__a3 smt__a4 smt__a5
              smt__a6 smt__a7))
          (smt__TLA__Mem smt__a5
            (smt__TLA__SetEnum_7 smt__a1 smt__a2 smt__a3 smt__a4 smt__a5
              smt__a6 smt__a7))
          (smt__TLA__Mem smt__a6
            (smt__TLA__SetEnum_7 smt__a1 smt__a2 smt__a3 smt__a4 smt__a5
              smt__a6 smt__a7))
          (smt__TLA__Mem smt__a7
            (smt__TLA__SetEnum_7 smt__a1 smt__a2 smt__a3 smt__a4 smt__a5
              smt__a6 smt__a7)))
        :pattern ((smt__TLA__SetEnum_7 smt__a1 smt__a2 smt__a3 smt__a4
                    smt__a5 smt__a6 smt__a7)))) :named |EnumDefIntro 7|))

;; Axiom: EnumDefElim 2
(assert
  (!
    (forall ((smt__a1 Idv) (smt__a2 Idv) (smt__x Idv))
      (!
        (=> (smt__TLA__Mem smt__x (smt__TLA__SetEnum_2 smt__a1 smt__a2))
          (or (= smt__x smt__a1) (= smt__x smt__a2)))
        :pattern ((smt__TLA__Mem smt__x (smt__TLA__SetEnum_2 smt__a1 smt__a2)))))
    :named |EnumDefElim 2|))

;; Axiom: EnumDefElim 5
(assert
  (!
    (forall
      ((smt__a1 Idv) (smt__a2 Idv) (smt__a3 Idv) (smt__a4 Idv) (smt__a5 Idv)
        (smt__x Idv))
      (!
        (=>
          (smt__TLA__Mem smt__x
            (smt__TLA__SetEnum_5 smt__a1 smt__a2 smt__a3 smt__a4 smt__a5))
          (or (= smt__x smt__a1) (= smt__x smt__a2) (= smt__x smt__a3)
            (= smt__x smt__a4) (= smt__x smt__a5)))
        :pattern ((smt__TLA__Mem smt__x
                    (smt__TLA__SetEnum_5 smt__a1 smt__a2 smt__a3 smt__a4
                      smt__a5))))) :named |EnumDefElim 5|))

;; Axiom: EnumDefElim 7
(assert
  (!
    (forall
      ((smt__a1 Idv) (smt__a2 Idv) (smt__a3 Idv) (smt__a4 Idv) (smt__a5 Idv)
        (smt__a6 Idv) (smt__a7 Idv) (smt__x Idv))
      (!
        (=>
          (smt__TLA__Mem smt__x
            (smt__TLA__SetEnum_7 smt__a1 smt__a2 smt__a3 smt__a4 smt__a5
              smt__a6 smt__a7))
          (or (= smt__x smt__a1) (= smt__x smt__a2) (= smt__x smt__a3)
            (= smt__x smt__a4) (= smt__x smt__a5) (= smt__x smt__a6)
            (= smt__x smt__a7)))
        :pattern ((smt__TLA__Mem smt__x
                    (smt__TLA__SetEnum_7 smt__a1 smt__a2 smt__a3 smt__a4
                      smt__a5 smt__a6 smt__a7))))) :named |EnumDefElim 7|))

;; Axiom: StrLitIsstr a0
(assert
  (! (smt__TLA__Mem smt__TLA__StrLit_a0 smt__TLA__StrSet)
    :named |StrLitIsstr a0|))

;; Axiom: StrLitIsstr a1
(assert
  (! (smt__TLA__Mem smt__TLA__StrLit_a1 smt__TLA__StrSet)
    :named |StrLitIsstr a1|))

;; Axiom: StrLitIsstr a2
(assert
  (! (smt__TLA__Mem smt__TLA__StrLit_a2 smt__TLA__StrSet)
    :named |StrLitIsstr a2|))

;; Axiom: StrLitIsstr a3a
(assert
  (! (smt__TLA__Mem smt__TLA__StrLit_a3a smt__TLA__StrSet)
    :named |StrLitIsstr a3a|))

;; Axiom: StrLitIsstr a3b
(assert
  (! (smt__TLA__Mem smt__TLA__StrLit_a3b smt__TLA__StrSet)
    :named |StrLitIsstr a3b|))

;; Axiom: StrLitIsstr a4
(assert
  (! (smt__TLA__Mem smt__TLA__StrLit_a4 smt__TLA__StrSet)
    :named |StrLitIsstr a4|))

;; Axiom: StrLitIsstr cs
(assert
  (! (smt__TLA__Mem smt__TLA__StrLit_cs smt__TLA__StrSet)
    :named |StrLitIsstr cs|))

;; Axiom: StrLitDistinct a1 a0
(assert
  (! (distinct smt__TLA__StrLit_a1 smt__TLA__StrLit_a0)
    :named |StrLitDistinct a1 a0|))

;; Axiom: StrLitDistinct a2 a0
(assert
  (! (distinct smt__TLA__StrLit_a2 smt__TLA__StrLit_a0)
    :named |StrLitDistinct a2 a0|))

;; Axiom: StrLitDistinct a2 a1
(assert
  (! (distinct smt__TLA__StrLit_a2 smt__TLA__StrLit_a1)
    :named |StrLitDistinct a2 a1|))

;; Axiom: StrLitDistinct a3a a0
(assert
  (! (distinct smt__TLA__StrLit_a3a smt__TLA__StrLit_a0)
    :named |StrLitDistinct a3a a0|))

;; Axiom: StrLitDistinct a3a a1
(assert
  (! (distinct smt__TLA__StrLit_a3a smt__TLA__StrLit_a1)
    :named |StrLitDistinct a3a a1|))

;; Axiom: StrLitDistinct a3a a2
(assert
  (! (distinct smt__TLA__StrLit_a3a smt__TLA__StrLit_a2)
    :named |StrLitDistinct a3a a2|))

;; Axiom: StrLitDistinct a3b a0
(assert
  (! (distinct smt__TLA__StrLit_a3b smt__TLA__StrLit_a0)
    :named |StrLitDistinct a3b a0|))

;; Axiom: StrLitDistinct a3b a1
(assert
  (! (distinct smt__TLA__StrLit_a3b smt__TLA__StrLit_a1)
    :named |StrLitDistinct a3b a1|))

;; Axiom: StrLitDistinct a3b a2
(assert
  (! (distinct smt__TLA__StrLit_a3b smt__TLA__StrLit_a2)
    :named |StrLitDistinct a3b a2|))

;; Axiom: StrLitDistinct a3b a3a
(assert
  (! (distinct smt__TLA__StrLit_a3b smt__TLA__StrLit_a3a)
    :named |StrLitDistinct a3b a3a|))

;; Axiom: StrLitDistinct a4 a0
(assert
  (! (distinct smt__TLA__StrLit_a4 smt__TLA__StrLit_a0)
    :named |StrLitDistinct a4 a0|))

;; Axiom: StrLitDistinct a4 a1
(assert
  (! (distinct smt__TLA__StrLit_a4 smt__TLA__StrLit_a1)
    :named |StrLitDistinct a4 a1|))

;; Axiom: StrLitDistinct a4 a2
(assert
  (! (distinct smt__TLA__StrLit_a4 smt__TLA__StrLit_a2)
    :named |StrLitDistinct a4 a2|))

;; Axiom: StrLitDistinct a4 a3a
(assert
  (! (distinct smt__TLA__StrLit_a4 smt__TLA__StrLit_a3a)
    :named |StrLitDistinct a4 a3a|))

;; Axiom: StrLitDistinct a4 a3b
(assert
  (! (distinct smt__TLA__StrLit_a4 smt__TLA__StrLit_a3b)
    :named |StrLitDistinct a4 a3b|))

;; Axiom: StrLitDistinct a4 cs
(assert
  (! (distinct smt__TLA__StrLit_a4 smt__TLA__StrLit_cs)
    :named |StrLitDistinct a4 cs|))

;; Axiom: StrLitDistinct cs a0
(assert
  (! (distinct smt__TLA__StrLit_cs smt__TLA__StrLit_a0)
    :named |StrLitDistinct cs a0|))

;; Axiom: StrLitDistinct cs a1
(assert
  (! (distinct smt__TLA__StrLit_cs smt__TLA__StrLit_a1)
    :named |StrLitDistinct cs a1|))

;; Axiom: StrLitDistinct cs a2
(assert
  (! (distinct smt__TLA__StrLit_cs smt__TLA__StrLit_a2)
    :named |StrLitDistinct cs a2|))

;; Axiom: StrLitDistinct cs a3a
(assert
  (! (distinct smt__TLA__StrLit_cs smt__TLA__StrLit_a3a)
    :named |StrLitDistinct cs a3a|))

;; Axiom: StrLitDistinct cs a3b
(assert
  (! (distinct smt__TLA__StrLit_cs smt__TLA__StrLit_a3b)
    :named |StrLitDistinct cs a3b|))

;; Axiom: CastInjAlt Bool
(assert
  (!
    (and (= (smt__TLA__Cast_Bool true) smt__TLA__Tt_Idv)
      (distinct (smt__TLA__Cast_Bool false) smt__TLA__Tt_Idv))
    :named |CastInjAlt Bool|))

;; Axiom: CastInjAlt Int
(assert
  (!
    (forall ((smt__x Int))
      (! (= smt__x (smt__TLA__Proj_Int (smt__TLA__Cast_Int smt__x)))
        :pattern ((smt__TLA__Cast_Int smt__x)))) :named |CastInjAlt Int|))

;; Axiom: TypeGuardIntro Bool
(assert
  (!
    (forall ((smt__z Bool))
      (! (smt__TLA__Mem (smt__TLA__Cast_Bool smt__z) smt__TLA__BoolSet)
        :pattern ((smt__TLA__Cast_Bool smt__z))))
    :named |TypeGuardIntro Bool|))

;; Axiom: TypeGuardIntro Int
(assert
  (!
    (forall ((smt__z Int))
      (! (smt__TLA__Mem (smt__TLA__Cast_Int smt__z) smt__TLA__IntSet)
        :pattern ((smt__TLA__Cast_Int smt__z)))) :named |TypeGuardIntro Int|))

;; Axiom: TypeGuardElim Bool
(assert
  (!
    (forall ((smt__x Idv))
      (!
        (=> (smt__TLA__Mem smt__x smt__TLA__BoolSet)
          (or (= smt__x (smt__TLA__Cast_Bool true))
            (= smt__x (smt__TLA__Cast_Bool false))))
        :pattern ((smt__TLA__Mem smt__x smt__TLA__BoolSet))))
    :named |TypeGuardElim Bool|))

;; Axiom: TypeGuardElim Int
(assert
  (!
    (forall ((smt__x Idv))
      (!
        (=> (smt__TLA__Mem smt__x smt__TLA__IntSet)
          (= smt__x (smt__TLA__Cast_Int (smt__TLA__Proj_Int smt__x))))
        :pattern ((smt__TLA__Mem smt__x smt__TLA__IntSet))))
    :named |TypeGuardElim Int|))

;; Axiom: ExtTrigEqDef Idv
(assert
  (!
    (forall ((smt__x Idv) (smt__y Idv))
      (! (= (smt__TLA__TrigEq_Idv smt__x smt__y) (= smt__x smt__y))
        :pattern ((smt__TLA__TrigEq_Idv smt__x smt__y))))
    :named |ExtTrigEqDef Idv|))

; hidden fact

; hidden fact

; omitted declaration of 'CONSTANT_EnabledWrapper_' (second-order)

; omitted declaration of 'CONSTANT_CdotWrapper_' (second-order)

(declare-fun smt__CONSTANT_Not_ (Idv) Idv)

(declare-fun smt__VARIABLE_flag_ () Idv)

(declare-fun smt__VARIABLE_flag__prime () Idv)

(declare-fun smt__VARIABLE_turn_ () Idv)

(declare-fun smt__VARIABLE_turn__prime () Idv)

(declare-fun smt__VARIABLE_pc_ () Idv)

(declare-fun smt__VARIABLE_pc__prime () Idv)

(declare-fun smt__STATE_vars_ () Idv)

(declare-fun smt__ACTION_a0_ (Idv) Idv)

(declare-fun smt__ACTION_a1_ (Idv) Idv)

(declare-fun smt__ACTION_a2_ (Idv) Idv)

(declare-fun smt__ACTION_a3a_ (Idv) Idv)

(declare-fun smt__ACTION_a3b_ (Idv) Idv)

(declare-fun smt__ACTION_cs_ (Idv) Idv)

(declare-fun smt__ACTION_a4_ (Idv) Idv)

(declare-fun smt__ACTION_proc_ (Idv) Idv)

(declare-fun smt__ACTION_Next_ () Idv)

(declare-fun smt__TEMPORAL_Spec_ () Idv)

(declare-fun smt__STATE_MutualExclusion_ () Idv)

(declare-fun smt__STATE_NeverCS_ () Idv)

(declare-fun smt__STATE_Wait_ (Idv) Idv)

(declare-fun smt__STATE_CS_ (Idv) Idv)

(declare-fun smt__TEMPORAL_Fairness_ () Idv)

(declare-fun smt__TEMPORAL_FairSpec_ () Idv)

(declare-fun smt__TEMPORAL_Liveness1_ () Idv)

(declare-fun smt__TEMPORAL_Liveness_ () Idv)

(declare-fun smt__TEMPORAL_ISpec_ () Idv)

; hidden fact

; hidden fact

; hidden fact

(declare-fun smt__TLA__FunFcn_flatnd_1 (Idv) Idv)

;; Axiom: FunConstrIsafcn TLA__FunFcn_flatnd_1
(assert
  (!
    (forall ((smt__a Idv))
      (! (smt__TLA__FunIsafcn (smt__TLA__FunFcn_flatnd_1 smt__a))
        :pattern ((smt__TLA__FunFcn_flatnd_1 smt__a))))
    :named |FunConstrIsafcn TLA__FunFcn_flatnd_1|))

;; Axiom: FunDomDef TLA__FunFcn_flatnd_1
(assert
  (!
    (forall ((smt__a Idv))
      (! (= (smt__TLA__FunDom (smt__TLA__FunFcn_flatnd_1 smt__a)) smt__a)
        :pattern ((smt__TLA__FunFcn_flatnd_1 smt__a))))
    :named |FunDomDef TLA__FunFcn_flatnd_1|))

;; Axiom: FunAppDef TLA__FunFcn_flatnd_1
(assert
  (!
    (forall ((smt__a Idv) (smt__x Idv))
      (!
        (=> (smt__TLA__Mem smt__x smt__a)
          (= (smt__TLA__FunApp (smt__TLA__FunFcn_flatnd_1 smt__a) smt__x)
            (smt__TLA__Cast_Bool false)))
        :pattern ((smt__TLA__FunApp (smt__TLA__FunFcn_flatnd_1 smt__a) smt__x))
        :pattern ((smt__TLA__Mem smt__x smt__a)
                   (smt__TLA__FunFcn_flatnd_1 smt__a))))
    :named |FunAppDef TLA__FunFcn_flatnd_1|))

(declare-fun smt__TLA__FunFcn_flatnd_2 (Idv) Idv)

;; Axiom: FunConstrIsafcn TLA__FunFcn_flatnd_2
(assert
  (!
    (forall ((smt__a Idv))
      (! (smt__TLA__FunIsafcn (smt__TLA__FunFcn_flatnd_2 smt__a))
        :pattern ((smt__TLA__FunFcn_flatnd_2 smt__a))))
    :named |FunConstrIsafcn TLA__FunFcn_flatnd_2|))

;; Axiom: FunDomDef TLA__FunFcn_flatnd_2
(assert
  (!
    (forall ((smt__a Idv))
      (! (= (smt__TLA__FunDom (smt__TLA__FunFcn_flatnd_2 smt__a)) smt__a)
        :pattern ((smt__TLA__FunFcn_flatnd_2 smt__a))))
    :named |FunDomDef TLA__FunFcn_flatnd_2|))

;; Axiom: FunAppDef TLA__FunFcn_flatnd_2
(assert
  (!
    (forall ((smt__a Idv) (smt__x Idv))
      (!
        (=> (smt__TLA__Mem smt__x smt__a)
          (= (smt__TLA__FunApp (smt__TLA__FunFcn_flatnd_2 smt__a) smt__x)
            smt__TLA__StrLit_a0))
        :pattern ((smt__TLA__FunApp (smt__TLA__FunFcn_flatnd_2 smt__a) smt__x))
        :pattern ((smt__TLA__Mem smt__x smt__a)
                   (smt__TLA__FunFcn_flatnd_2 smt__a))))
    :named |FunAppDef TLA__FunFcn_flatnd_2|))

;; Goal
(assert
  (!
    (not
      (=>
        (and
          (smt__TLA__TrigEq_Idv smt__VARIABLE_flag_
            (smt__TLA__FunFcn_flatnd_1
              (smt__TLA__SetEnum_2 (smt__TLA__Cast_Int 0)
                (smt__TLA__Cast_Int 1))))
          (smt__TLA__TrigEq_Idv smt__VARIABLE_turn_ (smt__TLA__Cast_Int 0))
          (smt__TLA__TrigEq_Idv smt__VARIABLE_pc_
            (smt__TLA__FunFcn_flatnd_2
              (smt__TLA__SetEnum_2 (smt__TLA__Cast_Int 0)
                (smt__TLA__Cast_Int 1)))))
        (and
          (and
            (smt__TLA__Mem smt__VARIABLE_pc_
              (smt__TLA__FunSet
                (smt__TLA__SetEnum_2 (smt__TLA__Cast_Int 0)
                  (smt__TLA__Cast_Int 1))
                (smt__TLA__SetEnum_7 smt__TLA__StrLit_a0 smt__TLA__StrLit_a1
                  smt__TLA__StrLit_a2 smt__TLA__StrLit_a3a
                  smt__TLA__StrLit_a3b smt__TLA__StrLit_cs
                  smt__TLA__StrLit_a4)))
            (smt__TLA__Mem smt__VARIABLE_turn_
              (smt__TLA__SetEnum_2 (smt__TLA__Cast_Int 0)
                (smt__TLA__Cast_Int 1)))
            (smt__TLA__Mem smt__VARIABLE_flag_
              (smt__TLA__FunSet
                (smt__TLA__SetEnum_2 (smt__TLA__Cast_Int 0)
                  (smt__TLA__Cast_Int 1)) smt__TLA__BoolSet)))
          (forall ((smt__CONSTANT_i_ Idv))
            (=>
              (smt__TLA__Mem smt__CONSTANT_i_
                (smt__TLA__SetEnum_2 (smt__TLA__Cast_Int 0)
                  (smt__TLA__Cast_Int 1)))
              (and
                (=>
                  (smt__TLA__Mem
                    (smt__TLA__FunApp smt__VARIABLE_pc_ smt__CONSTANT_i_)
                    (smt__TLA__SetEnum_5 smt__TLA__StrLit_a2
                      smt__TLA__StrLit_a3a smt__TLA__StrLit_a3b
                      smt__TLA__StrLit_cs smt__TLA__StrLit_a4))
                  (= (smt__TLA__FunApp smt__VARIABLE_flag_ smt__CONSTANT_i_)
                    smt__TLA__Tt_Idv))
                (=>
                  (smt__TLA__Mem
                    (smt__TLA__FunApp smt__VARIABLE_pc_ smt__CONSTANT_i_)
                    (smt__TLA__SetEnum_2 smt__TLA__StrLit_cs
                      smt__TLA__StrLit_a4))
                  (and
                    (not
                      (smt__TLA__Mem
                        (smt__TLA__FunApp smt__VARIABLE_pc_
                          (smt__CONSTANT_Not_ smt__CONSTANT_i_))
                        (smt__TLA__SetEnum_2 smt__TLA__StrLit_cs
                          smt__TLA__StrLit_a4)))
                    (=>
                      (smt__TLA__Mem
                        (smt__TLA__FunApp smt__VARIABLE_pc_
                          (smt__CONSTANT_Not_ smt__CONSTANT_i_))
                        (smt__TLA__SetEnum_2 smt__TLA__StrLit_a3a
                          smt__TLA__StrLit_a3b))
                      (smt__TLA__TrigEq_Idv smt__VARIABLE_turn_
                        smt__CONSTANT_i_)))))))))) :named |Goal|))

(check-sat)
(exit)
