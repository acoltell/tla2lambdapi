;; Proof obligation:
;;	ASSUME NEW VARIABLE VARIABLE_flag_,
;;	       NEW VARIABLE VARIABLE_turn_,
;;	       NEW VARIABLE VARIABLE_pc_,
;;	       ASSUME NEW CONSTANT CONSTANT_j_ \in {0, 1}
;;	       PROVE  /\ ?VARIABLE_pc_#prime[CONSTANT_j_]
;;	                 \in {"a2", "a3a", "a3b", "cs", "a4"}
;;	                 => ?VARIABLE_flag_#prime[CONSTANT_j_]
;;	              /\ ?VARIABLE_pc_#prime[CONSTANT_j_] \in {"cs", "a4"}
;;	                 => (/\ ?VARIABLE_pc_#prime[CONSTANT_Not_(CONSTANT_j_)]
;;	                        \notin {"cs", "a4"}
;;	                     /\ ?VARIABLE_pc_#prime[CONSTANT_Not_(CONSTANT_j_)]
;;	                        \in {"a3a", "a3b"}
;;	                        => ?VARIABLE_turn_#prime = CONSTANT_j_) 
;;	PROVE  \A CONSTANT_i_ \in {0, 1} :
;;	          /\ ?VARIABLE_pc_#prime[CONSTANT_i_]
;;	             \in {"a2", "a3a", "a3b", "cs", "a4"}
;;	             => ?VARIABLE_flag_#prime[CONSTANT_i_]
;;	          /\ ?VARIABLE_pc_#prime[CONSTANT_i_] \in {"cs", "a4"}
;;	             => (/\ ?VARIABLE_pc_#prime[CONSTANT_Not_(CONSTANT_i_)]
;;	                    \notin {"cs", "a4"}
;;	                 /\ ?VARIABLE_pc_#prime[CONSTANT_Not_(CONSTANT_i_)]
;;	                    \in {"a3a", "a3b"} => ?VARIABLE_turn_#prime = CONSTANT_i_)
;; TLA+ Proof Manager 1.5.0
;; Proof obligation #50
;; Generated from file "./tla_specs/Original/TLAPS_Examples/Peterson.tla", line 183, characters 7-8

(set-logic UFLIA)

;; Sorts

(declare-sort Idv 0)

;; Hypotheses

(declare-fun smt__TLA__Cast_Int (Int) Idv)

(declare-fun smt__TLA__FunApp (Idv Idv) Idv)

(declare-fun smt__TLA__FunDom (Idv) Idv)

; omitted declaration of 'TLA__FunFcn' (second-order)

(declare-fun smt__TLA__FunIsafcn (Idv) Bool)

(declare-fun smt__TLA__IntSet () Idv)

(declare-fun smt__TLA__Mem (Idv Idv) Bool)

(declare-fun smt__TLA__Proj_Int (Idv) Int)

(declare-fun smt__TLA__SetEnum_2 (Idv Idv) Idv)

(declare-fun smt__TLA__SetEnum_5 (Idv Idv Idv Idv Idv) Idv)

(declare-fun smt__TLA__SetExtTrigger (Idv Idv) Bool)

(declare-fun smt__TLA__StrLit_a2 () Idv)

(declare-fun smt__TLA__StrLit_a3a () Idv)

(declare-fun smt__TLA__StrLit_a3b () Idv)

(declare-fun smt__TLA__StrLit_a4 () Idv)

(declare-fun smt__TLA__StrLit_cs () Idv)

(declare-fun smt__TLA__StrSet () Idv)

(declare-fun smt__TLA__TrigEq_Idv (Idv Idv) Bool)

(declare-fun smt__TLA__Tt_Idv () Idv)

;; Axiom: SetExt
(assert
  (!
    (forall ((smt__x Idv) (smt__y Idv))
      (!
        (=>
          (forall ((smt__z Idv))
            (= (smt__TLA__Mem smt__z smt__x) (smt__TLA__Mem smt__z smt__y)))
          (= smt__x smt__y))
        :pattern ((smt__TLA__SetExtTrigger smt__x smt__y)))) :named |SetExt|))

;; Axiom: FunExt
(assert
  (!
    (forall ((smt__f Idv) (smt__g Idv))
      (!
        (=>
          (and (smt__TLA__FunIsafcn smt__f) (smt__TLA__FunIsafcn smt__g)
            (= (smt__TLA__FunDom smt__f) (smt__TLA__FunDom smt__g))
            (forall ((smt__x Idv))
              (=> (smt__TLA__Mem smt__x (smt__TLA__FunDom smt__f))
                (= (smt__TLA__FunApp smt__f smt__x)
                  (smt__TLA__FunApp smt__g smt__x))))) (= smt__f smt__g))
        :pattern ((smt__TLA__FunIsafcn smt__f) (smt__TLA__FunIsafcn smt__g))))
    :named |FunExt|))

; omitted fact (second-order)

; omitted fact (second-order)

; omitted fact (second-order)

;; Axiom: EnumDefIntro 2
(assert
  (!
    (forall ((smt__a1 Idv) (smt__a2 Idv))
      (!
        (and (smt__TLA__Mem smt__a1 (smt__TLA__SetEnum_2 smt__a1 smt__a2))
          (smt__TLA__Mem smt__a2 (smt__TLA__SetEnum_2 smt__a1 smt__a2)))
        :pattern ((smt__TLA__SetEnum_2 smt__a1 smt__a2))))
    :named |EnumDefIntro 2|))

;; Axiom: EnumDefIntro 5
(assert
  (!
    (forall
      ((smt__a1 Idv) (smt__a2 Idv) (smt__a3 Idv) (smt__a4 Idv) (smt__a5 Idv))
      (!
        (and
          (smt__TLA__Mem smt__a1
            (smt__TLA__SetEnum_5 smt__a1 smt__a2 smt__a3 smt__a4 smt__a5))
          (smt__TLA__Mem smt__a2
            (smt__TLA__SetEnum_5 smt__a1 smt__a2 smt__a3 smt__a4 smt__a5))
          (smt__TLA__Mem smt__a3
            (smt__TLA__SetEnum_5 smt__a1 smt__a2 smt__a3 smt__a4 smt__a5))
          (smt__TLA__Mem smt__a4
            (smt__TLA__SetEnum_5 smt__a1 smt__a2 smt__a3 smt__a4 smt__a5))
          (smt__TLA__Mem smt__a5
            (smt__TLA__SetEnum_5 smt__a1 smt__a2 smt__a3 smt__a4 smt__a5)))
        :pattern ((smt__TLA__SetEnum_5 smt__a1 smt__a2 smt__a3 smt__a4
                    smt__a5)))) :named |EnumDefIntro 5|))

;; Axiom: EnumDefElim 2
(assert
  (!
    (forall ((smt__a1 Idv) (smt__a2 Idv) (smt__x Idv))
      (!
        (=> (smt__TLA__Mem smt__x (smt__TLA__SetEnum_2 smt__a1 smt__a2))
          (or (= smt__x smt__a1) (= smt__x smt__a2)))
        :pattern ((smt__TLA__Mem smt__x (smt__TLA__SetEnum_2 smt__a1 smt__a2)))))
    :named |EnumDefElim 2|))

;; Axiom: EnumDefElim 5
(assert
  (!
    (forall
      ((smt__a1 Idv) (smt__a2 Idv) (smt__a3 Idv) (smt__a4 Idv) (smt__a5 Idv)
        (smt__x Idv))
      (!
        (=>
          (smt__TLA__Mem smt__x
            (smt__TLA__SetEnum_5 smt__a1 smt__a2 smt__a3 smt__a4 smt__a5))
          (or (= smt__x smt__a1) (= smt__x smt__a2) (= smt__x smt__a3)
            (= smt__x smt__a4) (= smt__x smt__a5)))
        :pattern ((smt__TLA__Mem smt__x
                    (smt__TLA__SetEnum_5 smt__a1 smt__a2 smt__a3 smt__a4
                      smt__a5))))) :named |EnumDefElim 5|))

;; Axiom: StrLitIsstr a2
(assert
  (! (smt__TLA__Mem smt__TLA__StrLit_a2 smt__TLA__StrSet)
    :named |StrLitIsstr a2|))

;; Axiom: StrLitIsstr a3a
(assert
  (! (smt__TLA__Mem smt__TLA__StrLit_a3a smt__TLA__StrSet)
    :named |StrLitIsstr a3a|))

;; Axiom: StrLitIsstr a3b
(assert
  (! (smt__TLA__Mem smt__TLA__StrLit_a3b smt__TLA__StrSet)
    :named |StrLitIsstr a3b|))

;; Axiom: StrLitIsstr a4
(assert
  (! (smt__TLA__Mem smt__TLA__StrLit_a4 smt__TLA__StrSet)
    :named |StrLitIsstr a4|))

;; Axiom: StrLitIsstr cs
(assert
  (! (smt__TLA__Mem smt__TLA__StrLit_cs smt__TLA__StrSet)
    :named |StrLitIsstr cs|))

;; Axiom: StrLitDistinct a3a a2
(assert
  (! (distinct smt__TLA__StrLit_a3a smt__TLA__StrLit_a2)
    :named |StrLitDistinct a3a a2|))

;; Axiom: StrLitDistinct a3b a2
(assert
  (! (distinct smt__TLA__StrLit_a3b smt__TLA__StrLit_a2)
    :named |StrLitDistinct a3b a2|))

;; Axiom: StrLitDistinct a3b a3a
(assert
  (! (distinct smt__TLA__StrLit_a3b smt__TLA__StrLit_a3a)
    :named |StrLitDistinct a3b a3a|))

;; Axiom: StrLitDistinct a4 a2
(assert
  (! (distinct smt__TLA__StrLit_a4 smt__TLA__StrLit_a2)
    :named |StrLitDistinct a4 a2|))

;; Axiom: StrLitDistinct a4 a3a
(assert
  (! (distinct smt__TLA__StrLit_a4 smt__TLA__StrLit_a3a)
    :named |StrLitDistinct a4 a3a|))

;; Axiom: StrLitDistinct a4 a3b
(assert
  (! (distinct smt__TLA__StrLit_a4 smt__TLA__StrLit_a3b)
    :named |StrLitDistinct a4 a3b|))

;; Axiom: StrLitDistinct a4 cs
(assert
  (! (distinct smt__TLA__StrLit_a4 smt__TLA__StrLit_cs)
    :named |StrLitDistinct a4 cs|))

;; Axiom: StrLitDistinct cs a2
(assert
  (! (distinct smt__TLA__StrLit_cs smt__TLA__StrLit_a2)
    :named |StrLitDistinct cs a2|))

;; Axiom: StrLitDistinct cs a3a
(assert
  (! (distinct smt__TLA__StrLit_cs smt__TLA__StrLit_a3a)
    :named |StrLitDistinct cs a3a|))

;; Axiom: StrLitDistinct cs a3b
(assert
  (! (distinct smt__TLA__StrLit_cs smt__TLA__StrLit_a3b)
    :named |StrLitDistinct cs a3b|))

;; Axiom: CastInjAlt Int
(assert
  (!
    (forall ((smt__x Int))
      (! (= smt__x (smt__TLA__Proj_Int (smt__TLA__Cast_Int smt__x)))
        :pattern ((smt__TLA__Cast_Int smt__x)))) :named |CastInjAlt Int|))

;; Axiom: TypeGuardIntro Int
(assert
  (!
    (forall ((smt__z Int))
      (! (smt__TLA__Mem (smt__TLA__Cast_Int smt__z) smt__TLA__IntSet)
        :pattern ((smt__TLA__Cast_Int smt__z)))) :named |TypeGuardIntro Int|))

;; Axiom: TypeGuardElim Int
(assert
  (!
    (forall ((smt__x Idv))
      (!
        (=> (smt__TLA__Mem smt__x smt__TLA__IntSet)
          (= smt__x (smt__TLA__Cast_Int (smt__TLA__Proj_Int smt__x))))
        :pattern ((smt__TLA__Mem smt__x smt__TLA__IntSet))))
    :named |TypeGuardElim Int|))

;; Axiom: ExtTrigEqDef Idv
(assert
  (!
    (forall ((smt__x Idv) (smt__y Idv))
      (! (= (smt__TLA__TrigEq_Idv smt__x smt__y) (= smt__x smt__y))
        :pattern ((smt__TLA__TrigEq_Idv smt__x smt__y))))
    :named |ExtTrigEqDef Idv|))

; hidden fact

; hidden fact

; omitted declaration of 'CONSTANT_EnabledWrapper_' (second-order)

; omitted declaration of 'CONSTANT_CdotWrapper_' (second-order)

(declare-fun smt__CONSTANT_Not_ (Idv) Idv)

(declare-fun smt__VARIABLE_flag_ () Idv)

(declare-fun smt__VARIABLE_flag__prime () Idv)

(declare-fun smt__VARIABLE_turn_ () Idv)

(declare-fun smt__VARIABLE_turn__prime () Idv)

(declare-fun smt__VARIABLE_pc_ () Idv)

(declare-fun smt__VARIABLE_pc__prime () Idv)

(declare-fun smt__STATE_vars_ () Idv)

(declare-fun smt__STATE_Init_ () Idv)

(declare-fun smt__ACTION_a0_ (Idv) Idv)

(declare-fun smt__ACTION_a1_ (Idv) Idv)

(declare-fun smt__ACTION_a2_ (Idv) Idv)

(declare-fun smt__ACTION_a3a_ (Idv) Idv)

(declare-fun smt__ACTION_a3b_ (Idv) Idv)

(declare-fun smt__ACTION_cs_ (Idv) Idv)

(declare-fun smt__ACTION_a4_ (Idv) Idv)

(declare-fun smt__ACTION_proc_ (Idv) Idv)

(declare-fun smt__ACTION_Next_ () Idv)

(declare-fun smt__TEMPORAL_Spec_ () Idv)

(declare-fun smt__STATE_MutualExclusion_ () Idv)

(declare-fun smt__STATE_NeverCS_ () Idv)

(declare-fun smt__STATE_Wait_ (Idv) Idv)

(declare-fun smt__STATE_CS_ (Idv) Idv)

(declare-fun smt__TEMPORAL_Fairness_ () Idv)

(declare-fun smt__TEMPORAL_FairSpec_ () Idv)

(declare-fun smt__TEMPORAL_Liveness1_ () Idv)

(declare-fun smt__TEMPORAL_Liveness_ () Idv)

(declare-fun smt__STATE_TypeOK_ () Idv)

(declare-fun smt__STATE_Inv_ () Idv)

(declare-fun smt__TEMPORAL_ISpec_ () Idv)

; hidden fact

; hidden fact

; hidden fact

; hidden fact

; hidden fact

; hidden fact

; hidden fact

; hidden fact

; hidden fact

; hidden fact

; hidden fact

(assert
  (forall ((smt__CONSTANT_j_ Idv))
    (=>
      (smt__TLA__Mem smt__CONSTANT_j_
        (smt__TLA__SetEnum_2 (smt__TLA__Cast_Int 0) (smt__TLA__Cast_Int 1)))
      (and
        (=>
          (smt__TLA__Mem
            (smt__TLA__FunApp smt__VARIABLE_pc__prime smt__CONSTANT_j_)
            (smt__TLA__SetEnum_5 smt__TLA__StrLit_a2 smt__TLA__StrLit_a3a
              smt__TLA__StrLit_a3b smt__TLA__StrLit_cs smt__TLA__StrLit_a4))
          (= (smt__TLA__FunApp smt__VARIABLE_flag__prime smt__CONSTANT_j_)
            smt__TLA__Tt_Idv))
        (=>
          (smt__TLA__Mem
            (smt__TLA__FunApp smt__VARIABLE_pc__prime smt__CONSTANT_j_)
            (smt__TLA__SetEnum_2 smt__TLA__StrLit_cs smt__TLA__StrLit_a4))
          (and
            (not
              (smt__TLA__Mem
                (smt__TLA__FunApp smt__VARIABLE_pc__prime
                  (smt__CONSTANT_Not_ smt__CONSTANT_j_))
                (smt__TLA__SetEnum_2 smt__TLA__StrLit_cs smt__TLA__StrLit_a4)))
            (=>
              (smt__TLA__Mem
                (smt__TLA__FunApp smt__VARIABLE_pc__prime
                  (smt__CONSTANT_Not_ smt__CONSTANT_j_))
                (smt__TLA__SetEnum_2 smt__TLA__StrLit_a3a
                  smt__TLA__StrLit_a3b))
              (smt__TLA__TrigEq_Idv smt__VARIABLE_turn__prime
                smt__CONSTANT_j_))))))))

;; Goal
(assert
  (!
    (not
      (forall ((smt__CONSTANT_i_ Idv))
        (=>
          (smt__TLA__Mem smt__CONSTANT_i_
            (smt__TLA__SetEnum_2 (smt__TLA__Cast_Int 0)
              (smt__TLA__Cast_Int 1)))
          (and
            (=>
              (smt__TLA__Mem
                (smt__TLA__FunApp smt__VARIABLE_pc__prime smt__CONSTANT_i_)
                (smt__TLA__SetEnum_5 smt__TLA__StrLit_a2 smt__TLA__StrLit_a3a
                  smt__TLA__StrLit_a3b smt__TLA__StrLit_cs
                  smt__TLA__StrLit_a4))
              (=
                (smt__TLA__FunApp smt__VARIABLE_flag__prime smt__CONSTANT_i_)
                smt__TLA__Tt_Idv))
            (=>
              (smt__TLA__Mem
                (smt__TLA__FunApp smt__VARIABLE_pc__prime smt__CONSTANT_i_)
                (smt__TLA__SetEnum_2 smt__TLA__StrLit_cs smt__TLA__StrLit_a4))
              (and
                (not
                  (smt__TLA__Mem
                    (smt__TLA__FunApp smt__VARIABLE_pc__prime
                      (smt__CONSTANT_Not_ smt__CONSTANT_i_))
                    (smt__TLA__SetEnum_2 smt__TLA__StrLit_cs
                      smt__TLA__StrLit_a4)))
                (=>
                  (smt__TLA__Mem
                    (smt__TLA__FunApp smt__VARIABLE_pc__prime
                      (smt__CONSTANT_Not_ smt__CONSTANT_i_))
                    (smt__TLA__SetEnum_2 smt__TLA__StrLit_a3a
                      smt__TLA__StrLit_a3b))
                  (smt__TLA__TrigEq_Idv smt__VARIABLE_turn__prime
                    smt__CONSTANT_i_)))))))) :named |Goal|))

(check-sat)
(exit)
