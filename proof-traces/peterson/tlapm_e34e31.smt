;; Proof obligation:
;;	ASSUME NEW VARIABLE VARIABLE_flag_,
;;	       NEW VARIABLE VARIABLE_turn_,
;;	       NEW VARIABLE VARIABLE_pc_,
;;	       ASSUME STATE_Inv_ ,
;;	              ACTION_Next_ \/ ?h6fbaa = STATE_vars_ 
;;	       PROVE  ?hdb2fe 
;;	PROVE  STATE_Inv_ /\ (ACTION_Next_ \/ ?h6fbaa = STATE_vars_) => ?hdb2fe
;; TLA+ Proof Manager 1.5.0
;; Proof obligation #16
;; Generated from file "./tla_specs/Original/TLAPS_Examples/Peterson.tla", line 131, characters 5-11

(set-logic UFLIA)

;; Sorts

(declare-sort Idv 0)

;; Hypotheses

(declare-fun smt__TLA__Anon_h6fbaa () Idv)

(declare-fun smt__TLA__Anon_hdb2fe () Idv)

(declare-fun smt__TLA__TrigEq_Idv (Idv Idv) Bool)

(declare-fun smt__TLA__Tt_Idv () Idv)

;; Axiom: ExtTrigEqDef Idv
(assert
  (!
    (forall ((smt__x Idv) (smt__y Idv))
      (! (= (smt__TLA__TrigEq_Idv smt__x smt__y) (= smt__x smt__y))
        :pattern ((smt__TLA__TrigEq_Idv smt__x smt__y))))
    :named |ExtTrigEqDef Idv|))

; hidden fact

; hidden fact

; omitted declaration of 'CONSTANT_EnabledWrapper_' (second-order)

; omitted declaration of 'CONSTANT_CdotWrapper_' (second-order)

(declare-fun smt__CONSTANT_Not_ (Idv) Idv)

(declare-fun smt__VARIABLE_flag_ () Idv)

(declare-fun smt__VARIABLE_flag__prime () Idv)

(declare-fun smt__VARIABLE_turn_ () Idv)

(declare-fun smt__VARIABLE_turn__prime () Idv)

(declare-fun smt__VARIABLE_pc_ () Idv)

(declare-fun smt__VARIABLE_pc__prime () Idv)

(declare-fun smt__STATE_vars_ () Idv)

(declare-fun smt__STATE_Init_ () Idv)

(declare-fun smt__ACTION_a0_ (Idv) Idv)

(declare-fun smt__ACTION_a1_ (Idv) Idv)

(declare-fun smt__ACTION_a2_ (Idv) Idv)

(declare-fun smt__ACTION_a3a_ (Idv) Idv)

(declare-fun smt__ACTION_a3b_ (Idv) Idv)

(declare-fun smt__ACTION_cs_ (Idv) Idv)

(declare-fun smt__ACTION_a4_ (Idv) Idv)

(declare-fun smt__ACTION_proc_ (Idv) Idv)

(declare-fun smt__ACTION_Next_ () Idv)

(declare-fun smt__TEMPORAL_Spec_ () Idv)

(declare-fun smt__STATE_MutualExclusion_ () Idv)

(declare-fun smt__STATE_NeverCS_ () Idv)

(declare-fun smt__STATE_Wait_ (Idv) Idv)

(declare-fun smt__STATE_CS_ (Idv) Idv)

(declare-fun smt__TEMPORAL_Fairness_ () Idv)

(declare-fun smt__TEMPORAL_FairSpec_ () Idv)

(declare-fun smt__TEMPORAL_Liveness1_ () Idv)

(declare-fun smt__TEMPORAL_Liveness_ () Idv)

(declare-fun smt__STATE_TypeOK_ () Idv)

(declare-fun smt__STATE_I_ () Idv)

(declare-fun smt__STATE_Inv_ () Idv)

(declare-fun smt__TEMPORAL_ISpec_ () Idv)

; hidden fact

; hidden fact

; hidden fact

(assert
  (=> (= smt__STATE_Inv_ smt__TLA__Tt_Idv)
    (=>
      (or (= smt__ACTION_Next_ smt__TLA__Tt_Idv)
        (smt__TLA__TrigEq_Idv smt__TLA__Anon_h6fbaa smt__STATE_vars_))
      (= smt__TLA__Anon_hdb2fe smt__TLA__Tt_Idv))))

;; Goal
(assert
  (!
    (not
      (=>
        (and (= smt__STATE_Inv_ smt__TLA__Tt_Idv)
          (or (= smt__ACTION_Next_ smt__TLA__Tt_Idv)
            (smt__TLA__TrigEq_Idv smt__TLA__Anon_h6fbaa smt__STATE_vars_)))
        (= smt__TLA__Anon_hdb2fe smt__TLA__Tt_Idv))) :named |Goal|))

(check-sat)
(exit)
