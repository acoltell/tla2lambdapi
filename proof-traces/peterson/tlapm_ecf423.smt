;; Proof obligation:
;;	ASSUME NEW VARIABLE VARIABLE_flag_,
;;	       NEW VARIABLE VARIABLE_turn_,
;;	       NEW VARIABLE VARIABLE_pc_,
;;	       (/\ VARIABLE_pc_
;;	           \in [{0, 1} -> {"a0", "a1", "a2", "a3a", "a3b", "cs", "a4"}]
;;	        /\ VARIABLE_turn_ \in {0, 1}
;;	        /\ VARIABLE_flag_ \in [{0, 1} -> BOOLEAN])
;;	       /\ STATE_I_ ,
;;	       \E CONSTANT_self_ \in {0, 1} :
;;	          (/\ VARIABLE_pc_[CONSTANT_self_] = "a0"
;;	           /\ ?VARIABLE_pc_#prime
;;	              = [VARIABLE_pc_ EXCEPT ![CONSTANT_self_] = "a1"]
;;	           /\ /\ ?VARIABLE_flag_#prime = VARIABLE_flag_
;;	              /\ ?VARIABLE_turn_#prime = VARIABLE_turn_)
;;	          \/ (/\ VARIABLE_pc_[CONSTANT_self_] = "a1"
;;	              /\ ?VARIABLE_flag_#prime
;;	                 = [VARIABLE_flag_ EXCEPT ![CONSTANT_self_] = TRUE]
;;	              /\ ?VARIABLE_pc_#prime
;;	                 = [VARIABLE_pc_ EXCEPT ![CONSTANT_self_] = "a2"]
;;	              /\ ?VARIABLE_turn_#prime = VARIABLE_turn_)
;;	          \/ (/\ VARIABLE_pc_[CONSTANT_self_] = "a2"
;;	              /\ ?VARIABLE_turn_#prime
;;	                 = (IF CONSTANT_self_ = 0 THEN 1 ELSE 0)
;;	              /\ ?VARIABLE_pc_#prime
;;	                 = [VARIABLE_pc_ EXCEPT ![CONSTANT_self_] = "a3a"]
;;	              /\ ?VARIABLE_flag_#prime = VARIABLE_flag_)
;;	          \/ (/\ VARIABLE_pc_[CONSTANT_self_] = "a3a"
;;	              /\ IF VARIABLE_flag_[IF CONSTANT_self_ = 0 THEN 1 ELSE 0]
;;	                   THEN /\ ?VARIABLE_pc_#prime
;;	                           = [VARIABLE_pc_ EXCEPT ![CONSTANT_self_] = "a3b"]
;;	                   ELSE /\ ?VARIABLE_pc_#prime
;;	                           = [VARIABLE_pc_ EXCEPT ![CONSTANT_self_] = "cs"]
;;	              /\ /\ ?VARIABLE_flag_#prime = VARIABLE_flag_
;;	                 /\ ?VARIABLE_turn_#prime = VARIABLE_turn_)
;;	          \/ (/\ VARIABLE_pc_[CONSTANT_self_] = "a3b"
;;	              /\ IF VARIABLE_turn_ = (IF CONSTANT_self_ = 0 THEN 1 ELSE 0)
;;	                   THEN /\ ?VARIABLE_pc_#prime
;;	                           = [VARIABLE_pc_ EXCEPT ![CONSTANT_self_] = "a3a"]
;;	                   ELSE /\ ?VARIABLE_pc_#prime
;;	                           = [VARIABLE_pc_ EXCEPT ![CONSTANT_self_] = "cs"]
;;	              /\ /\ ?VARIABLE_flag_#prime = VARIABLE_flag_
;;	                 /\ ?VARIABLE_turn_#prime = VARIABLE_turn_)
;;	          \/ (/\ VARIABLE_pc_[CONSTANT_self_] = "cs"
;;	              /\ TRUE
;;	              /\ ?VARIABLE_pc_#prime
;;	                 = [VARIABLE_pc_ EXCEPT ![CONSTANT_self_] = "a4"]
;;	              /\ /\ ?VARIABLE_flag_#prime = VARIABLE_flag_
;;	                 /\ ?VARIABLE_turn_#prime = VARIABLE_turn_)
;;	          \/ (/\ VARIABLE_pc_[CONSTANT_self_] = "a4"
;;	              /\ ?VARIABLE_flag_#prime
;;	                 = [VARIABLE_flag_ EXCEPT ![CONSTANT_self_] = FALSE]
;;	              /\ ?VARIABLE_pc_#prime
;;	                 = [VARIABLE_pc_ EXCEPT ![CONSTANT_self_] = "a0"]
;;	              /\ ?VARIABLE_turn_#prime = VARIABLE_turn_) 
;;	PROVE  /\ ?VARIABLE_pc_#prime
;;	          \in [{0, 1} -> {"a0", "a1", "a2", "a3a", "a3b", "cs", "a4"}]
;;	       /\ ?VARIABLE_turn_#prime \in {0, 1}
;;	       /\ ?VARIABLE_flag_#prime \in [{0, 1} -> BOOLEAN]
;; TLA+ Proof Manager 1.5.0
;; Proof obligation #44
;; Generated from file "./tla_specs/Original/TLAPS_Examples/Peterson.tla", line 179, characters 5-6

(set-logic UFLIA)

;; Sorts

(declare-sort Idv 0)

;; Hypotheses

(declare-fun smt__TLA__BoolSet () Idv)

(declare-fun smt__TLA__Cast_Bool (Bool) Idv)

(declare-fun smt__TLA__Cast_Int (Int) Idv)

(declare-fun smt__TLA__FunApp (Idv Idv) Idv)

(declare-fun smt__TLA__FunDom (Idv) Idv)

(declare-fun smt__TLA__FunExcept (Idv Idv Idv) Idv)

; omitted declaration of 'TLA__FunFcn' (second-order)

(declare-fun smt__TLA__FunIsafcn (Idv) Bool)

(declare-fun smt__TLA__FunSet (Idv Idv) Idv)

(declare-fun smt__TLA__IntSet () Idv)

(declare-fun smt__TLA__Mem (Idv Idv) Bool)

(declare-fun smt__TLA__Proj_Int (Idv) Int)

(declare-fun smt__TLA__SetEnum_2 (Idv Idv) Idv)

(declare-fun smt__TLA__SetEnum_7 (Idv Idv Idv Idv Idv Idv Idv) Idv)

(declare-fun smt__TLA__SetExtTrigger (Idv Idv) Bool)

(declare-fun smt__TLA__StrLit_a0 () Idv)

(declare-fun smt__TLA__StrLit_a1 () Idv)

(declare-fun smt__TLA__StrLit_a2 () Idv)

(declare-fun smt__TLA__StrLit_a3a () Idv)

(declare-fun smt__TLA__StrLit_a3b () Idv)

(declare-fun smt__TLA__StrLit_a4 () Idv)

(declare-fun smt__TLA__StrLit_cs () Idv)

(declare-fun smt__TLA__StrSet () Idv)

(declare-fun smt__TLA__TrigEq_Idv (Idv Idv) Bool)

(declare-fun smt__TLA__Tt_Idv () Idv)

;; Axiom: SetExt
(assert
  (!
    (forall ((smt__x Idv) (smt__y Idv))
      (!
        (=>
          (forall ((smt__z Idv))
            (= (smt__TLA__Mem smt__z smt__x) (smt__TLA__Mem smt__z smt__y)))
          (= smt__x smt__y))
        :pattern ((smt__TLA__SetExtTrigger smt__x smt__y)))) :named |SetExt|))

;; Axiom: FunExt
(assert
  (!
    (forall ((smt__f Idv) (smt__g Idv))
      (!
        (=>
          (and (smt__TLA__FunIsafcn smt__f) (smt__TLA__FunIsafcn smt__g)
            (= (smt__TLA__FunDom smt__f) (smt__TLA__FunDom smt__g))
            (forall ((smt__x Idv))
              (=> (smt__TLA__Mem smt__x (smt__TLA__FunDom smt__f))
                (= (smt__TLA__FunApp smt__f smt__x)
                  (smt__TLA__FunApp smt__g smt__x))))) (= smt__f smt__g))
        :pattern ((smt__TLA__FunIsafcn smt__f) (smt__TLA__FunIsafcn smt__g))))
    :named |FunExt|))

; omitted fact (second-order)

;; Axiom: FunSetIntro
(assert
  (!
    (forall ((smt__a Idv) (smt__b Idv) (smt__f Idv))
      (!
        (=>
          (and (smt__TLA__FunIsafcn smt__f)
            (= (smt__TLA__FunDom smt__f) smt__a)
            (forall ((smt__x Idv))
              (=> (smt__TLA__Mem smt__x smt__a)
                (smt__TLA__Mem (smt__TLA__FunApp smt__f smt__x) smt__b))))
          (smt__TLA__Mem smt__f (smt__TLA__FunSet smt__a smt__b)))
        :pattern ((smt__TLA__Mem smt__f (smt__TLA__FunSet smt__a smt__b)))))
    :named |FunSetIntro|))

;; Axiom: FunSetElim1
(assert
  (!
    (forall ((smt__a Idv) (smt__b Idv) (smt__f Idv))
      (!
        (=> (smt__TLA__Mem smt__f (smt__TLA__FunSet smt__a smt__b))
          (and (smt__TLA__FunIsafcn smt__f)
            (= (smt__TLA__FunDom smt__f) smt__a)))
        :pattern ((smt__TLA__Mem smt__f (smt__TLA__FunSet smt__a smt__b)))))
    :named |FunSetElim1|))

;; Axiom: FunSetElim2
(assert
  (!
    (forall ((smt__a Idv) (smt__b Idv) (smt__f Idv) (smt__x Idv))
      (!
        (=>
          (and (smt__TLA__Mem smt__f (smt__TLA__FunSet smt__a smt__b))
            (smt__TLA__Mem smt__x smt__a))
          (smt__TLA__Mem (smt__TLA__FunApp smt__f smt__x) smt__b))
        :pattern ((smt__TLA__Mem smt__f (smt__TLA__FunSet smt__a smt__b))
                   (smt__TLA__Mem smt__x smt__a))
        :pattern ((smt__TLA__Mem smt__f (smt__TLA__FunSet smt__a smt__b))
                   (smt__TLA__FunApp smt__f smt__x)))) :named |FunSetElim2|))

; omitted fact (second-order)

; omitted fact (second-order)

;; Axiom: FunExceptIsafcn
(assert
  (!
    (forall ((smt__f Idv) (smt__x Idv) (smt__y Idv))
      (! (smt__TLA__FunIsafcn (smt__TLA__FunExcept smt__f smt__x smt__y))
        :pattern ((smt__TLA__FunExcept smt__f smt__x smt__y))))
    :named |FunExceptIsafcn|))

;; Axiom: FunExceptDomDef
(assert
  (!
    (forall ((smt__f Idv) (smt__x Idv) (smt__y Idv))
      (!
        (= (smt__TLA__FunDom (smt__TLA__FunExcept smt__f smt__x smt__y))
          (smt__TLA__FunDom smt__f))
        :pattern ((smt__TLA__FunExcept smt__f smt__x smt__y))))
    :named |FunExceptDomDef|))

;; Axiom: FunExceptAppDef1
(assert
  (!
    (forall ((smt__f Idv) (smt__x Idv) (smt__y Idv))
      (!
        (=> (smt__TLA__Mem smt__x (smt__TLA__FunDom smt__f))
          (=
            (smt__TLA__FunApp (smt__TLA__FunExcept smt__f smt__x smt__y)
              smt__x) smt__y))
        :pattern ((smt__TLA__FunExcept smt__f smt__x smt__y))))
    :named |FunExceptAppDef1|))

;; Axiom: FunExceptAppDef2
(assert
  (!
    (forall ((smt__f Idv) (smt__x Idv) (smt__y Idv) (smt__z Idv))
      (!
        (=> (smt__TLA__Mem smt__z (smt__TLA__FunDom smt__f))
          (and
            (=> (= smt__z smt__x)
              (=
                (smt__TLA__FunApp (smt__TLA__FunExcept smt__f smt__x smt__y)
                  smt__z) smt__y))
            (=> (distinct smt__z smt__x)
              (=
                (smt__TLA__FunApp (smt__TLA__FunExcept smt__f smt__x smt__y)
                  smt__z) (smt__TLA__FunApp smt__f smt__z)))))
        :pattern ((smt__TLA__FunApp
                    (smt__TLA__FunExcept smt__f smt__x smt__y) smt__z))
        :pattern ((smt__TLA__FunExcept smt__f smt__x smt__y)
                   (smt__TLA__FunApp smt__f smt__z))))
    :named |FunExceptAppDef2|))

;; Axiom: EnumDefIntro 2
(assert
  (!
    (forall ((smt__a1 Idv) (smt__a2 Idv))
      (!
        (and (smt__TLA__Mem smt__a1 (smt__TLA__SetEnum_2 smt__a1 smt__a2))
          (smt__TLA__Mem smt__a2 (smt__TLA__SetEnum_2 smt__a1 smt__a2)))
        :pattern ((smt__TLA__SetEnum_2 smt__a1 smt__a2))))
    :named |EnumDefIntro 2|))

;; Axiom: EnumDefIntro 7
(assert
  (!
    (forall
      ((smt__a1 Idv) (smt__a2 Idv) (smt__a3 Idv) (smt__a4 Idv) (smt__a5 Idv)
        (smt__a6 Idv) (smt__a7 Idv))
      (!
        (and
          (smt__TLA__Mem smt__a1
            (smt__TLA__SetEnum_7 smt__a1 smt__a2 smt__a3 smt__a4 smt__a5
              smt__a6 smt__a7))
          (smt__TLA__Mem smt__a2
            (smt__TLA__SetEnum_7 smt__a1 smt__a2 smt__a3 smt__a4 smt__a5
              smt__a6 smt__a7))
          (smt__TLA__Mem smt__a3
            (smt__TLA__SetEnum_7 smt__a1 smt__a2 smt__a3 smt__a4 smt__a5
              smt__a6 smt__a7))
          (smt__TLA__Mem smt__a4
            (smt__TLA__SetEnum_7 smt__a1 smt__a2 smt__a3 smt__a4 smt__a5
              smt__a6 smt__a7))
          (smt__TLA__Mem smt__a5
            (smt__TLA__SetEnum_7 smt__a1 smt__a2 smt__a3 smt__a4 smt__a5
              smt__a6 smt__a7))
          (smt__TLA__Mem smt__a6
            (smt__TLA__SetEnum_7 smt__a1 smt__a2 smt__a3 smt__a4 smt__a5
              smt__a6 smt__a7))
          (smt__TLA__Mem smt__a7
            (smt__TLA__SetEnum_7 smt__a1 smt__a2 smt__a3 smt__a4 smt__a5
              smt__a6 smt__a7)))
        :pattern ((smt__TLA__SetEnum_7 smt__a1 smt__a2 smt__a3 smt__a4
                    smt__a5 smt__a6 smt__a7)))) :named |EnumDefIntro 7|))

;; Axiom: EnumDefElim 2
(assert
  (!
    (forall ((smt__a1 Idv) (smt__a2 Idv) (smt__x Idv))
      (!
        (=> (smt__TLA__Mem smt__x (smt__TLA__SetEnum_2 smt__a1 smt__a2))
          (or (= smt__x smt__a1) (= smt__x smt__a2)))
        :pattern ((smt__TLA__Mem smt__x (smt__TLA__SetEnum_2 smt__a1 smt__a2)))))
    :named |EnumDefElim 2|))

;; Axiom: EnumDefElim 7
(assert
  (!
    (forall
      ((smt__a1 Idv) (smt__a2 Idv) (smt__a3 Idv) (smt__a4 Idv) (smt__a5 Idv)
        (smt__a6 Idv) (smt__a7 Idv) (smt__x Idv))
      (!
        (=>
          (smt__TLA__Mem smt__x
            (smt__TLA__SetEnum_7 smt__a1 smt__a2 smt__a3 smt__a4 smt__a5
              smt__a6 smt__a7))
          (or (= smt__x smt__a1) (= smt__x smt__a2) (= smt__x smt__a3)
            (= smt__x smt__a4) (= smt__x smt__a5) (= smt__x smt__a6)
            (= smt__x smt__a7)))
        :pattern ((smt__TLA__Mem smt__x
                    (smt__TLA__SetEnum_7 smt__a1 smt__a2 smt__a3 smt__a4
                      smt__a5 smt__a6 smt__a7))))) :named |EnumDefElim 7|))

;; Axiom: StrLitIsstr a0
(assert
  (! (smt__TLA__Mem smt__TLA__StrLit_a0 smt__TLA__StrSet)
    :named |StrLitIsstr a0|))

;; Axiom: StrLitIsstr a1
(assert
  (! (smt__TLA__Mem smt__TLA__StrLit_a1 smt__TLA__StrSet)
    :named |StrLitIsstr a1|))

;; Axiom: StrLitIsstr a2
(assert
  (! (smt__TLA__Mem smt__TLA__StrLit_a2 smt__TLA__StrSet)
    :named |StrLitIsstr a2|))

;; Axiom: StrLitIsstr a3a
(assert
  (! (smt__TLA__Mem smt__TLA__StrLit_a3a smt__TLA__StrSet)
    :named |StrLitIsstr a3a|))

;; Axiom: StrLitIsstr a3b
(assert
  (! (smt__TLA__Mem smt__TLA__StrLit_a3b smt__TLA__StrSet)
    :named |StrLitIsstr a3b|))

;; Axiom: StrLitIsstr a4
(assert
  (! (smt__TLA__Mem smt__TLA__StrLit_a4 smt__TLA__StrSet)
    :named |StrLitIsstr a4|))

;; Axiom: StrLitIsstr cs
(assert
  (! (smt__TLA__Mem smt__TLA__StrLit_cs smt__TLA__StrSet)
    :named |StrLitIsstr cs|))

;; Axiom: StrLitDistinct a1 a0
(assert
  (! (distinct smt__TLA__StrLit_a1 smt__TLA__StrLit_a0)
    :named |StrLitDistinct a1 a0|))

;; Axiom: StrLitDistinct a2 a0
(assert
  (! (distinct smt__TLA__StrLit_a2 smt__TLA__StrLit_a0)
    :named |StrLitDistinct a2 a0|))

;; Axiom: StrLitDistinct a2 a1
(assert
  (! (distinct smt__TLA__StrLit_a2 smt__TLA__StrLit_a1)
    :named |StrLitDistinct a2 a1|))

;; Axiom: StrLitDistinct a3a a0
(assert
  (! (distinct smt__TLA__StrLit_a3a smt__TLA__StrLit_a0)
    :named |StrLitDistinct a3a a0|))

;; Axiom: StrLitDistinct a3a a1
(assert
  (! (distinct smt__TLA__StrLit_a3a smt__TLA__StrLit_a1)
    :named |StrLitDistinct a3a a1|))

;; Axiom: StrLitDistinct a3a a2
(assert
  (! (distinct smt__TLA__StrLit_a3a smt__TLA__StrLit_a2)
    :named |StrLitDistinct a3a a2|))

;; Axiom: StrLitDistinct a3b a0
(assert
  (! (distinct smt__TLA__StrLit_a3b smt__TLA__StrLit_a0)
    :named |StrLitDistinct a3b a0|))

;; Axiom: StrLitDistinct a3b a1
(assert
  (! (distinct smt__TLA__StrLit_a3b smt__TLA__StrLit_a1)
    :named |StrLitDistinct a3b a1|))

;; Axiom: StrLitDistinct a3b a2
(assert
  (! (distinct smt__TLA__StrLit_a3b smt__TLA__StrLit_a2)
    :named |StrLitDistinct a3b a2|))

;; Axiom: StrLitDistinct a3b a3a
(assert
  (! (distinct smt__TLA__StrLit_a3b smt__TLA__StrLit_a3a)
    :named |StrLitDistinct a3b a3a|))

;; Axiom: StrLitDistinct a4 a0
(assert
  (! (distinct smt__TLA__StrLit_a4 smt__TLA__StrLit_a0)
    :named |StrLitDistinct a4 a0|))

;; Axiom: StrLitDistinct a4 a1
(assert
  (! (distinct smt__TLA__StrLit_a4 smt__TLA__StrLit_a1)
    :named |StrLitDistinct a4 a1|))

;; Axiom: StrLitDistinct a4 a2
(assert
  (! (distinct smt__TLA__StrLit_a4 smt__TLA__StrLit_a2)
    :named |StrLitDistinct a4 a2|))

;; Axiom: StrLitDistinct a4 a3a
(assert
  (! (distinct smt__TLA__StrLit_a4 smt__TLA__StrLit_a3a)
    :named |StrLitDistinct a4 a3a|))

;; Axiom: StrLitDistinct a4 a3b
(assert
  (! (distinct smt__TLA__StrLit_a4 smt__TLA__StrLit_a3b)
    :named |StrLitDistinct a4 a3b|))

;; Axiom: StrLitDistinct a4 cs
(assert
  (! (distinct smt__TLA__StrLit_a4 smt__TLA__StrLit_cs)
    :named |StrLitDistinct a4 cs|))

;; Axiom: StrLitDistinct cs a0
(assert
  (! (distinct smt__TLA__StrLit_cs smt__TLA__StrLit_a0)
    :named |StrLitDistinct cs a0|))

;; Axiom: StrLitDistinct cs a1
(assert
  (! (distinct smt__TLA__StrLit_cs smt__TLA__StrLit_a1)
    :named |StrLitDistinct cs a1|))

;; Axiom: StrLitDistinct cs a2
(assert
  (! (distinct smt__TLA__StrLit_cs smt__TLA__StrLit_a2)
    :named |StrLitDistinct cs a2|))

;; Axiom: StrLitDistinct cs a3a
(assert
  (! (distinct smt__TLA__StrLit_cs smt__TLA__StrLit_a3a)
    :named |StrLitDistinct cs a3a|))

;; Axiom: StrLitDistinct cs a3b
(assert
  (! (distinct smt__TLA__StrLit_cs smt__TLA__StrLit_a3b)
    :named |StrLitDistinct cs a3b|))

;; Axiom: CastInjAlt Bool
(assert
  (!
    (and (= (smt__TLA__Cast_Bool true) smt__TLA__Tt_Idv)
      (distinct (smt__TLA__Cast_Bool false) smt__TLA__Tt_Idv))
    :named |CastInjAlt Bool|))

;; Axiom: CastInjAlt Int
(assert
  (!
    (forall ((smt__x Int))
      (! (= smt__x (smt__TLA__Proj_Int (smt__TLA__Cast_Int smt__x)))
        :pattern ((smt__TLA__Cast_Int smt__x)))) :named |CastInjAlt Int|))

;; Axiom: TypeGuardIntro Bool
(assert
  (!
    (forall ((smt__z Bool))
      (! (smt__TLA__Mem (smt__TLA__Cast_Bool smt__z) smt__TLA__BoolSet)
        :pattern ((smt__TLA__Cast_Bool smt__z))))
    :named |TypeGuardIntro Bool|))

;; Axiom: TypeGuardIntro Int
(assert
  (!
    (forall ((smt__z Int))
      (! (smt__TLA__Mem (smt__TLA__Cast_Int smt__z) smt__TLA__IntSet)
        :pattern ((smt__TLA__Cast_Int smt__z)))) :named |TypeGuardIntro Int|))

;; Axiom: TypeGuardElim Bool
(assert
  (!
    (forall ((smt__x Idv))
      (!
        (=> (smt__TLA__Mem smt__x smt__TLA__BoolSet)
          (or (= smt__x (smt__TLA__Cast_Bool true))
            (= smt__x (smt__TLA__Cast_Bool false))))
        :pattern ((smt__TLA__Mem smt__x smt__TLA__BoolSet))))
    :named |TypeGuardElim Bool|))

;; Axiom: TypeGuardElim Int
(assert
  (!
    (forall ((smt__x Idv))
      (!
        (=> (smt__TLA__Mem smt__x smt__TLA__IntSet)
          (= smt__x (smt__TLA__Cast_Int (smt__TLA__Proj_Int smt__x))))
        :pattern ((smt__TLA__Mem smt__x smt__TLA__IntSet))))
    :named |TypeGuardElim Int|))

;; Axiom: ExtTrigEqDef Idv
(assert
  (!
    (forall ((smt__x Idv) (smt__y Idv))
      (! (= (smt__TLA__TrigEq_Idv smt__x smt__y) (= smt__x smt__y))
        :pattern ((smt__TLA__TrigEq_Idv smt__x smt__y))))
    :named |ExtTrigEqDef Idv|))

; hidden fact

; hidden fact

; omitted declaration of 'CONSTANT_EnabledWrapper_' (second-order)

; omitted declaration of 'CONSTANT_CdotWrapper_' (second-order)

(declare-fun smt__VARIABLE_flag_ () Idv)

(declare-fun smt__VARIABLE_flag__prime () Idv)

(declare-fun smt__VARIABLE_turn_ () Idv)

(declare-fun smt__VARIABLE_turn__prime () Idv)

(declare-fun smt__VARIABLE_pc_ () Idv)

(declare-fun smt__VARIABLE_pc__prime () Idv)

(declare-fun smt__STATE_vars_ () Idv)

(declare-fun smt__STATE_Init_ () Idv)

(declare-fun smt__TEMPORAL_Spec_ () Idv)

(declare-fun smt__STATE_MutualExclusion_ () Idv)

(declare-fun smt__STATE_NeverCS_ () Idv)

(declare-fun smt__STATE_Wait_ (Idv) Idv)

(declare-fun smt__STATE_CS_ (Idv) Idv)

(declare-fun smt__TEMPORAL_Fairness_ () Idv)

(declare-fun smt__TEMPORAL_FairSpec_ () Idv)

(declare-fun smt__TEMPORAL_Liveness1_ () Idv)

(declare-fun smt__TEMPORAL_Liveness_ () Idv)

(declare-fun smt__STATE_I_ () Idv)

(declare-fun smt__TEMPORAL_ISpec_ () Idv)

; hidden fact

; hidden fact

; hidden fact

; hidden fact

; hidden fact

; hidden fact

; hidden fact

; hidden fact

; hidden fact

; hidden fact

(assert
  (and
    (and
      (smt__TLA__Mem smt__VARIABLE_pc_
        (smt__TLA__FunSet
          (smt__TLA__SetEnum_2 (smt__TLA__Cast_Int 0) (smt__TLA__Cast_Int 1))
          (smt__TLA__SetEnum_7 smt__TLA__StrLit_a0 smt__TLA__StrLit_a1
            smt__TLA__StrLit_a2 smt__TLA__StrLit_a3a smt__TLA__StrLit_a3b
            smt__TLA__StrLit_cs smt__TLA__StrLit_a4)))
      (smt__TLA__Mem smt__VARIABLE_turn_
        (smt__TLA__SetEnum_2 (smt__TLA__Cast_Int 0) (smt__TLA__Cast_Int 1)))
      (smt__TLA__Mem smt__VARIABLE_flag_
        (smt__TLA__FunSet
          (smt__TLA__SetEnum_2 (smt__TLA__Cast_Int 0) (smt__TLA__Cast_Int 1))
          smt__TLA__BoolSet))) (= smt__STATE_I_ smt__TLA__Tt_Idv)))

(assert
  (exists ((smt__CONSTANT_self_ Idv))
    (and
      (smt__TLA__Mem smt__CONSTANT_self_
        (smt__TLA__SetEnum_2 (smt__TLA__Cast_Int 0) (smt__TLA__Cast_Int 1)))
      (or
        (or
          (or
            (or
              (or
                (or
                  (and
                    (smt__TLA__TrigEq_Idv
                      (smt__TLA__FunApp smt__VARIABLE_pc_ smt__CONSTANT_self_)
                      smt__TLA__StrLit_a0)
                    (smt__TLA__TrigEq_Idv smt__VARIABLE_pc__prime
                      (smt__TLA__FunExcept smt__VARIABLE_pc_
                        smt__CONSTANT_self_ smt__TLA__StrLit_a1))
                    (and
                      (smt__TLA__TrigEq_Idv smt__VARIABLE_flag__prime
                        smt__VARIABLE_flag_)
                      (smt__TLA__TrigEq_Idv smt__VARIABLE_turn__prime
                        smt__VARIABLE_turn_)))
                  (and
                    (smt__TLA__TrigEq_Idv
                      (smt__TLA__FunApp smt__VARIABLE_pc_ smt__CONSTANT_self_)
                      smt__TLA__StrLit_a1)
                    (smt__TLA__TrigEq_Idv smt__VARIABLE_flag__prime
                      (smt__TLA__FunExcept smt__VARIABLE_flag_
                        smt__CONSTANT_self_ (smt__TLA__Cast_Bool true)))
                    (smt__TLA__TrigEq_Idv smt__VARIABLE_pc__prime
                      (smt__TLA__FunExcept smt__VARIABLE_pc_
                        smt__CONSTANT_self_ smt__TLA__StrLit_a2))
                    (smt__TLA__TrigEq_Idv smt__VARIABLE_turn__prime
                      smt__VARIABLE_turn_)))
                (and
                  (smt__TLA__TrigEq_Idv
                    (smt__TLA__FunApp smt__VARIABLE_pc_ smt__CONSTANT_self_)
                    smt__TLA__StrLit_a2)
                  (smt__TLA__TrigEq_Idv smt__VARIABLE_turn__prime
                    (smt__TLA__Cast_Int
                      (ite
                        (smt__TLA__TrigEq_Idv smt__CONSTANT_self_
                          (smt__TLA__Cast_Int 0)) 1 0)))
                  (smt__TLA__TrigEq_Idv smt__VARIABLE_pc__prime
                    (smt__TLA__FunExcept smt__VARIABLE_pc_
                      smt__CONSTANT_self_ smt__TLA__StrLit_a3a))
                  (smt__TLA__TrigEq_Idv smt__VARIABLE_flag__prime
                    smt__VARIABLE_flag_)))
              (and
                (smt__TLA__TrigEq_Idv
                  (smt__TLA__FunApp smt__VARIABLE_pc_ smt__CONSTANT_self_)
                  smt__TLA__StrLit_a3a)
                (ite
                  (=
                    (smt__TLA__FunApp smt__VARIABLE_flag_
                      (smt__TLA__Cast_Int
                        (ite
                          (smt__TLA__TrigEq_Idv smt__CONSTANT_self_
                            (smt__TLA__Cast_Int 0)) 1 0))) smt__TLA__Tt_Idv)
                  (and
                    (smt__TLA__TrigEq_Idv smt__VARIABLE_pc__prime
                      (smt__TLA__FunExcept smt__VARIABLE_pc_
                        smt__CONSTANT_self_ smt__TLA__StrLit_a3b)))
                  (and
                    (smt__TLA__TrigEq_Idv smt__VARIABLE_pc__prime
                      (smt__TLA__FunExcept smt__VARIABLE_pc_
                        smt__CONSTANT_self_ smt__TLA__StrLit_cs))))
                (and
                  (smt__TLA__TrigEq_Idv smt__VARIABLE_flag__prime
                    smt__VARIABLE_flag_)
                  (smt__TLA__TrigEq_Idv smt__VARIABLE_turn__prime
                    smt__VARIABLE_turn_))))
            (and
              (smt__TLA__TrigEq_Idv
                (smt__TLA__FunApp smt__VARIABLE_pc_ smt__CONSTANT_self_)
                smt__TLA__StrLit_a3b)
              (ite
                (smt__TLA__TrigEq_Idv smt__VARIABLE_turn_
                  (smt__TLA__Cast_Int
                    (ite
                      (smt__TLA__TrigEq_Idv smt__CONSTANT_self_
                        (smt__TLA__Cast_Int 0)) 1 0)))
                (and
                  (smt__TLA__TrigEq_Idv smt__VARIABLE_pc__prime
                    (smt__TLA__FunExcept smt__VARIABLE_pc_
                      smt__CONSTANT_self_ smt__TLA__StrLit_a3a)))
                (and
                  (smt__TLA__TrigEq_Idv smt__VARIABLE_pc__prime
                    (smt__TLA__FunExcept smt__VARIABLE_pc_
                      smt__CONSTANT_self_ smt__TLA__StrLit_cs))))
              (and
                (smt__TLA__TrigEq_Idv smt__VARIABLE_flag__prime
                  smt__VARIABLE_flag_)
                (smt__TLA__TrigEq_Idv smt__VARIABLE_turn__prime
                  smt__VARIABLE_turn_))))
          (and
            (smt__TLA__TrigEq_Idv
              (smt__TLA__FunApp smt__VARIABLE_pc_ smt__CONSTANT_self_)
              smt__TLA__StrLit_cs) true
            (smt__TLA__TrigEq_Idv smt__VARIABLE_pc__prime
              (smt__TLA__FunExcept smt__VARIABLE_pc_ smt__CONSTANT_self_
                smt__TLA__StrLit_a4))
            (and
              (smt__TLA__TrigEq_Idv smt__VARIABLE_flag__prime
                smt__VARIABLE_flag_)
              (smt__TLA__TrigEq_Idv smt__VARIABLE_turn__prime
                smt__VARIABLE_turn_))))
        (and
          (smt__TLA__TrigEq_Idv
            (smt__TLA__FunApp smt__VARIABLE_pc_ smt__CONSTANT_self_)
            smt__TLA__StrLit_a4)
          (smt__TLA__TrigEq_Idv smt__VARIABLE_flag__prime
            (smt__TLA__FunExcept smt__VARIABLE_flag_ smt__CONSTANT_self_
              (smt__TLA__Cast_Bool false)))
          (smt__TLA__TrigEq_Idv smt__VARIABLE_pc__prime
            (smt__TLA__FunExcept smt__VARIABLE_pc_ smt__CONSTANT_self_
              smt__TLA__StrLit_a0))
          (smt__TLA__TrigEq_Idv smt__VARIABLE_turn__prime smt__VARIABLE_turn_))))))

;; Goal
(assert
  (!
    (not
      (and
        (smt__TLA__Mem smt__VARIABLE_pc__prime
          (smt__TLA__FunSet
            (smt__TLA__SetEnum_2 (smt__TLA__Cast_Int 0)
              (smt__TLA__Cast_Int 1))
            (smt__TLA__SetEnum_7 smt__TLA__StrLit_a0 smt__TLA__StrLit_a1
              smt__TLA__StrLit_a2 smt__TLA__StrLit_a3a smt__TLA__StrLit_a3b
              smt__TLA__StrLit_cs smt__TLA__StrLit_a4)))
        (smt__TLA__Mem smt__VARIABLE_turn__prime
          (smt__TLA__SetEnum_2 (smt__TLA__Cast_Int 0) (smt__TLA__Cast_Int 1)))
        (smt__TLA__Mem smt__VARIABLE_flag__prime
          (smt__TLA__FunSet
            (smt__TLA__SetEnum_2 (smt__TLA__Cast_Int 0)
              (smt__TLA__Cast_Int 1)) smt__TLA__BoolSet)))) :named |Goal|))

(check-sat)
(exit)
